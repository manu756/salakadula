from sqlalchemy import create_engine
from sqlalchemy.sql import text
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import smtplib
from datetime import datetime
from datetime import timedelta
from email.mime.application import MIMEApplication
from email import encoders

db = create_engine('postgresql+psycopg2://testing:testing@localhost/babidi')
connection = db.connect()

time_now = datetime.now()
time_now = datetime.strftime(time_now,'%d/%m/%Y')
#hay que poner password
#password = ""
#emailfrom = "info@babidibulibros.com"

#server = smtplib.SMTP('smtp.babidibulibros.com: 587')
#password = ""
#emailfrom = "salakadula.testing@gmail.com"

#server = smtplib.SMTP('smtp.gmail.com: 587')
#server.starttls()
#server.login(emailfrom, password)

contacts=db.execute(text("select e.emailautor, e.idcorreo, e.fecha_envio, e.frecuencia, e.correo_actual, e.fichero, r.enviando from envios_correo e inner join recordatorios r on ( e.emailautor = r.emailautor) where r.enviando = 'SI' and e.fecha_envio = :time_now"), time_now=time_now)

for author in contacts:
    msg = MIMEMultipart()
    msg['To'] = author['emailautor']
    id_correo = author['idcorreo']
    emailautor = author['emailautor']
    fecha_envio = author['fecha_envio']
    correo_actual = author['correo_actual']
    fichero = author['fichero'].encode('UTF-8')
    fecha_envio_check = datetime.strftime(fecha_envio,'%d/%m/%Y')
    frecuencia = author['frecuencia']
    frecuencia = int(frecuencia)
    total = db.execute(text('select count(*) from cadenas_correo where id = :id_correo'), id_correo=id_correo)
    asunto = db.execute(text('select asunto from cadenas_correo where id = :id_correo'), id_correo=id_correo)
    editorial = db.execute(text('select editorial from cadenas_correo where id = :id_correo'), id_correo=id_correo)
    editorial = editorial.fetchone()[0]

    if editorial == 'babidibu':
	    mail_editorial = 'info@babidibulibros.com'
        password = ""
        msg['From'] = "info@babidibulibros.com"
        server = smtplib.SMTP("plesk136.red166.trevenque.es:587")
        server.starttls()
        server.login(msg['From'], password)
    elif editorial == 'mirahadas':
	    mail_editorial = 'info@mirahadas.com'
        password = ""
        msg['From'] = "info@mirahadas.com"
        server = smtplib.SMTP("plesk136.red166.trevenque.es:587")
        server.starttls()
        server.login(msg['From'], password)

    if fichero != 'False' and correo_actual == '1':
        fichero_loc = "/var/opt/{}".format(fichero)
        part = MIMEApplication(open(fichero_loc,"rb").read())
        part.add_header('Content-Disposition','attachment',filename=fichero)
        msg.attach(part)

    if correo_actual == '1':
        texto = db.execute(text('select texto from envios_correo where emailautor = :emailautor'), emailautor=emailautor)
        message = texto.fetchone()[0]
    else:
        texto = db.execute(text('select texto from cadenas_correo where id = :id and orden = :orden'), id=id_correo, orden=correo_actual)
        message = texto.fetchone()[0]
    
    cuerpo = message
    nombre = db.execute(text('select nombre from autores where email = :emailautor'), emailautor=emailautor)
    nombre = nombre.fetchone()[0]
    cuerpo = cuerpo.replace("[Autor/a]",nombre)
    msg.attach(MIMEText(cuerpo, 'plain', 'utf-8'))

    if fecha_envio_check == time_now:
        if correo_actual == total.fetchone()[0]:
            fecha_envio = fecha_envio + timedelta(days=frecuencia)
            server.sendmail(msg['From'], msg['To'], msg.as_string())
            server.sendmail(msg['From'], mail_editorial, msg.as_string())
            server.sendmail(msg['From'], 'm.francosanchez24@gmail.com', msg.as_string())
            db.execute(text('update envios_correo set correo_actual = :correo_actual, fecha_envio = :fecha_envio where emailautor = :emailautor'), correo_actual=correo_actual, fecha_envio=fecha_envio ,emailautor=emailautor)
        else:
	    total = db.execute(text('select count(*) from cadenas_correo where id = :id_correo'), id_correo=id_correo)
	    total_correo = total.fetchone()[0]
	    if int(correo_actual) >= int(total_correo):
		correo_actual = total_correo
	    else:
	    	correo_actual = int(correo_actual) + 1
            correo_actual = str(correo_actual)

        fecha_envio = fecha_envio + timedelta(days=frecuencia)
        msg['Subject'] = asunto.fetchone()[0]
        server.sendmail(msg['From'], msg['To'], msg.as_string())
        server.sendmail(msg['From'], mail_editorial, msg.as_string())
        server.sendmail(msg['From'], 'm.francosanchez24@gmail.com', msg.as_string())
        db.execute(text('update envios_correo set correo_actual = :correo_actual, fecha_envio = :fecha_envio where emailautor = :emailautor'), correo_actual=correo_actual, fecha_envio=fecha_envio ,emailautor=emailautor)
server.quit()

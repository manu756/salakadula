#_*_ encoding: utf-8 _*_
import os
from flask import Flask, g, render_template, flash, url_for, redirect, session, request
from flask_bcrypt import check_password_hash
from werkzeug.utils import secure_filename
import subprocess

import models
import forms

DEBUG = True
PORT = 8080
HOST = '0.0.0.0'
UPLOAD_FOLDER = '/var/opt/'
ALLOWED_EXTENSION = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'docx', 'doc', 'raw', 'gif'])


app = Flask(__name__, static_url_path='/static')
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.secret_key = 'jkldfsjklñsfadjklñdfaskljñdwjklñsdjklsdfjklsdfjsdfdfslj'

def authentication():
    try:
        if session['logged_in']:
            pass
        else:
            flash(u'Logueate por favor', category='Danger')
            return 1
    except KeyError:
        flash(u'Logueate por favor', category='Danger')
        return 1

@app.before_request
def before_request():
    g.db = models.connection
    if g.db.close():
        g.db.connect()

@app.after_request
def after_request(response):
    g.db.close()
    return response

@app.route('/register', methods=('GET','POST'))
def register():
    try:
        if session['logged_in']:
            form = forms.RegisterForm(request.form)
            if request.method == 'POST':
                if form.validate():
                    try:
                        models.User.insert_user(
                                user = form.username.data,
                                password = form.password.data
                                )
                        flash('Usuario registrado', category='success')
                        return redirect(url_for('index'))
                    except:
                        flash('Usuario repetido', category='warning')
                        return redirect(url_for('register'))
            return render_template('register.html', form=form)
        else:
            flash(u'Logueate por favor', category='danger')
            return redirect(url_for('login'))
    except KeyError:
        flash(u'Logueate por favor', category='danger')
        return redirect(url_for('login'))

@app.route('/login', methods=('GET','POST'))
def login():
    form = forms.LoginForm(request.form)
    if request.method == 'POST':
        if form.validate():
            try:
                user = models.User.get_user(form.username.data)
                username = user.fetchone()[0]
                user = models.User.get_user(form.username.data)
                password = user.fetchone()[1]
                if check_password_hash(password, form.password.data):
                    session['logged_in'] = True
                    session['username'] = username
                    return redirect(url_for('index'))
                raise
            except:
                flash(u'Nombre de usuario o contraseña fallido', category='danger')
                return redirect(url_for('login'))
    return render_template('login.html', form=form)

@app.route('/logout')
def logout():
    try:
        if session['logged_in']:
            session['logged_in'] = False
            flash('Has salido de Salakadula', category='warning')
            return redirect(url_for('login'))
        else:
            flash('Necesario Login para Logout', category='warning')
            return redirect(url_for('login'))
    except KeyError:
        return redirect(url_for('login'))

@app.route('/author_register', methods=('GET','POST'))
def author_register():
    try:
        if session['logged_in']:
            form = forms.AuthorForm(request.form)
            if form.validate():
                models.Author.insert_author(
                    nombre = form.nombre.data,
                    apellido = form.apellido.data,
                    email = form.email.data,
                    registrador = session['username']
                    )
                return redirect(url_for('index'))
            return render_template('author_register.html', form=form)
        else:
            flash(u'Logueate por favor', category='danger')
            return redirect(url_for('login'))
    except KeyError:
        return redirect(url_for('login'))

@app.route('/changepass', methods=('GET','POST'))
def changePass():
    try:
        if session['logged_in'] and session['username'] == 'admin':
            form = forms.ResetPassForm(request.form)
            if request.method == 'POST':
                if form.validate():
                    try:
                        user = models.User.get_user(form.username.data)
                        if check_password_hash(user.fetchone()[1], form.oldPassword.data):
                            models.User.change_pass(
                                user = form.username.data,
                                password = form.newPassword.data
                                )
                            flash('Password modificado', category='success')
                            return redirect(url_for('index'))
                        raise
                    except:
                        flash('Nombre de usuario o contraseña fallido', 'danger')
                        return redirect(url_for('changepass'))
            return render_template('changepass.html',form = form)
        else:
            flash('Logueate por favor', 'error')
            return redirect(url_for('login'))
    except KeyError:
        flash(u'Logueate por favor', category='danger')
        return redirect(url_for('login'))
    return render_template('changepass.html',form=form)

@app.route('/autor/<autor>', methods=('GET','POST'))
def autor(autor):
    flag = authentication()
    if flag == 1:
        return redirect(url_for('index'))

    autores = models.Author.get_detail(autor)
    comentarios = models.Author.get_comments(autor)
    libros = models.Author.get_book(autor)
    tipo = models.Author.get_tipo(autor)
    tipo_autor = tipo.fetchone()[0]
    autor = autores.fetchone()[0]
    editoriales = models.Editoriales.get_editoriales()
    autores = models.Author.get_detail(autor)
    if request.method == 'GET':
        return render_template('user.html', autor= autor, autores=autores, comentarios=comentarios, libros=libros, tipo_autor=tipo_autor, editoriales=editoriales, url="autor")
    elif request.method == 'POST':
        form = forms.UpdateAuthorForm(request.form)
        if form.validate():
            for data in autores:
                email = data[0]
                nombre = data[1]
                apellido = data[2]
                numero = data[5]
            if form.nombre.data != nombre:
                nombre = form.nombre.data
            elif form.email.data != email:
                email = form.email.data
            elif form.apellido.data != apellido:
                apellido = form.apellido.data
            elif numero == None:
                models.Author.insert_phone(email, form.numero.data)
            elif form.numero.data != numero and numero != None:
                numero = form.numero.data
                models.Author.update_phone(email, numero)
            models.Author.update_autor(email, nombre, apellido, autor)
            if request.form.get('tipo') != None:
                tipo_autor = request.form.get('tipo')
                models.Author.update_tipo(email, tipo_autor)
            return redirect(url_for('autor', autor=email, tipo_autor=tipo_autor))

@app.route('/add_editorial',methods=('GET','POST'))
def add_editorial():
    flag = authentication()
    if flag == 1:
        return redirect(url_for('index'))

    if request.method == 'GET':
        return render_template('add_editorial.html')
    else:
        form = forms.AddEditorial(request.form)
        if form.validate():
            models.Editoriales.add_editorial(form.nombre.data)
            return redirect(url_for('index'))

@app.route('/comments/<autor>',methods=('GET','POST'))
def comments(autor):
    flag = authentication()
    if flag == 1:
        return redirect(url_for('index'))

    if request.method == 'GET':
        comentarios = models.Author.get_comments(autor)
        return render_template('comments.html', comentarios=comentarios, autor=autor, url="comments")

@app.route('/add_book/<autor>',methods=('GET','POST'))
def add_book(autor):
    flag = authentication()
    if flag == 1:
        return redirect(url_for('index'))

    if request.method == 'POST':
        form = forms.AddBookForm(request.form)
        models.Book.insert_book(form.nombre_libro.data,request.form.get('editorial'),autor,request.form.get('estado_libro'))
        return redirect(url_for('autor', autor=autor))
    else:
        return redirect(url_for('autor', autor=autor))

@app.route('/update_status/<autor>/<nombre>/<nombreeditorial>',methods=('GET','POST'))
def update_status(autor, nombre, nombreeditorial):
    flag = authentication()
    if flag == 1:
        return redirect(url_for('index'))

    if request.method == 'POST':
        estado = request.form.get('actualizado_libro')
        models.Book.update_status(nombre, nombreeditorial, autor, estado)
        return redirect(url_for('autor', autor=autor))
    else:
        return redirect(url_for('autor', autor=autor))

@app.route('/add_comment/<autor>/<url>', methods=('GET','POST'))
def add_comment(autor,url):
    flag = authentication()
    if flag == 1:
        return redirect(url_for('index'))

    if request.method == 'POST':
        form = forms.AddCommentForm(request.form)
        if form.validate():
            editor = session['username']
            models.Comments.add_comment(autor, editor, form.comentario.data)
            return redirect(url_for(url, autor=autor))
    else:
            return redirect(url_for(url, autor=autor))

@app.route('/reminders', methods=('GET','POST'))
def reminders():
    try:
        if session['logged_in']:
            if request.method == 'GET':
                result = models.Author.get_reminders()
                username = session['username']
                return render_template('reminders.html', result=result, username=username)
            else:
                try:
                    os.system('python mail.py')
                    return redirect(request.url)
                except TypeError:
                    flash(u'No hay correos que enviar', category='info')
                    return redirect(request.url)
        else:
            flash(u'Logueate por favor', category='danger')
            return redirect(url_for('login'))
    except KeyError:
        flash(u'Logueate por favor', category='danger')
        return redirect(url_for('login'))

@app.route('/author_send/<email>', methods=('GET','POST'))
def author_send(email):
    flag = authentication()
    if flag == 1:
        return redirect(url_for('index'))

    def allowed_file(filename):
        return '.' in filename and \
                filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSION

    form = forms.AuthorSend(request.form)
    if request.method == 'GET':
        return render_template('author_send.html', autor=email)
    else:
        if form.validate():
            fecha_envio=request.form.get('fecha_envio')
            cadena=request.form.get('cadena')
            periodicidad=request.form.get('periodicidad')
            fichero = 'False'
            if 'file' in request.files:
                file = request.files['file']

                if file.filename == '':
                    flash('Name empty')
                    return redirect(request.url)

                if file and allowed_file(file.filename):
                    filename = secure_filename(file.filename)
                    file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
                    fichero = file.filename.replace(" ","_")

            return redirect(url_for('edit_text', email=email, fecha_envio=fecha_envio, cadena=cadena, periodicidad=periodicidad, fichero=fichero))
        else:
            return render_template('author_send.html', autor=email)

@app.route('/edit_text/<email>/<fecha_envio>/<cadena>/<periodicidad>/<fichero>', methods=('POST','GET'))
def edit_text(email, fecha_envio, cadena, periodicidad, fichero):
    flag = authentication()
    if flag == 1:
        return redirect(url_for('index'))

    if request.method == 'GET':
        texto = models.Mail.get_chain(cadena).fetchone()[0]
        return render_template('edit_text.html', cadena = texto)
    else:
        form = forms.EditText(request.form)
        if form.validate():
            models.Mail.add_mail(email, fecha_envio, form.texto.data, periodicidad, cadena, fichero)
            models.Author.add_reminder(email)
            return redirect(url_for('sending'))

@app.route('/sending',methods=("GET","POST"))
def sending():
    flag = authentication()
    if flag == 1:
        return redirect(url_for('index'))

    os.system('python mail.py')
    return redirect(url_for('reminders'))


@app.route('/stop_mail/<autor>', methods=('POST','GET'))
def stop_mail(autor):
    flag = authentication()
    if flag == 1:
        return redirect(url_for('index'))

    if request.method == 'GET':
            try:
                models.Mail.change_reminder(autor)
            except:
                return redirect(url_for('index'))
            return redirect(url_for('index'))

@app.route('/stop_reminder/<autor>', methods=('GET','POST'))
def stop_reminder(autor):
    flag = authentication()
    if flag == 1:
        return redirect(url_for('index'))

    if request.method == 'GET':
            models.Mail.stop_mail(autor)
            models.Mail.stop_reminder(autor)
            return redirect(url_for('reminders'))

@app.route('/author_delete/<email>', methods=('POST','GET'))
def author_delete(email):
    flag = authentication()
    if flag == 1:
        return redirect(url_for('index'))

    if request.method == 'GET':
        try:
            models.Author.author_delete(email)
            return redirect(url_for('index'))
        except:
            return redirect(url_for('index'))

@app.route('/change_mail/<email>', methods=('POST','GET'))
def change_mail(email):
    flag = authentication()
    if flag == 1:
        return redirect(url_for('index'))

    if request.method == 'GET':
        try:
            cadena = models.Mail.get_chain_autor(email).fetchone()[0]
            periodicidad = models.Mail.get_periodicity(email).fetchone()[0]
            return render_template('mail_update.html', autor=email, cadena=cadena, periodicidad=periodicidad)
        except TypeError:
            flash('Este autor no tiene correos', 'error')
            return redirect(url_for('index'))
    else:
        form = forms.EditMail(request.form)
        if form.validate():
            cadena = request.form.get('cadena')
            periodicidad = request.form.get('periodicidad')
            fecha = request.form.get('fecha')
            return redirect(url_for('edit_mail_text', email=email,fecha=fecha, cadena=cadena, periodicidad=periodicidad))

@app.route('/edit_mail_text/<email>/<fecha>/<cadena>/<periodicidad>', methods=('POST','GET'))
def edit_mail_text(email, fecha,cadena, periodicidad):
    flag = authentication()
    if flag == 1:
        return redirect(url_for('index'))

    if request.method == 'GET':
        texto = models.Mail.get_chain(cadena).fetchone()[0]
        return render_template('edit_mail_text.html', cadena = texto)
    else:
        form = forms.EditMailText(request.form)
        if form.validate():
            texto = request.form.get('texto')
            models.Mail.update_mail(email, fecha, cadena, periodicidad, texto)
            models.Mail.restart_reminder(email)
            return redirect(url_for('sending'))

@app.route('/chains', methods=('POST','GET'))
def chains():
    flag = authentication()
    if flag == 1:
        return redirect(url_for('index'))

    if request.method == 'GET':
        cadenas = models.Mail.get_subjects()
        return render_template('chains.html', user=session['username'],cadenas=cadenas)
    else:
        return redirect(url_for('new_chain'))

@app.route('/subchain/<id_chain>', methods=('GET','POST'))
def subchain(id_chain):
    flag = authentication()
    if flag == 1:
        return redirect(url_for('index'))

    if request.method == 'GET':
        orden_texto = models.Mail.get_orden_texto(id_chain)
        return render_template('edit_chains.html', cadenas=orden_texto)
    
    else:
        form = forms.EditChain(request.form)
        if form.validate():
            lista=[]
            orden = int(models.Mail.get_high_orden(id_chain).fetchone()[0])
            for i in range(1, orden+1):
                lista_aux=[]
                lista_aux.append(request.form.get('texto'+str(i)))
                lista_aux.append(str(i))
                lista_aux.append(id_chain)
                lista.append(lista_aux)

            for cadena in lista:
                models.Mail.update_chains(cadena[0],cadena[1],cadena[2])


            return redirect(url_for('subchain', id_chain=id_chain))

@app.route('/new_chain', methods=('GET','POST'))
def new_chain():
    flag = authentication()
    if flag == 1:
        return redirect(url_for('index'))

    if request.method == 'GET':
       editoriales = models.Editoriales.get_editoriales()
       return render_template('new_chain.html', editoriales=editoriales)
    else:
       form = forms.NewChain(request.form)
       if form.validate():
            asunto = request.form.get('asunto')
            editorial = request.form.get('editorial')
            texto = request.form.get('texto')
            cadenas = models.Mail.get_subjects()
            for cadena in cadenas:
                if asunto == cadena['asunto'] and editorial == cadena['editorial']:
                    id_max = models.Mail.get_id(asunto,editorial).fetchone()[0]
                    orden = models.Mail.get_order_max(asunto).fetchone()[0]

            if 'id_max' and 'orden' not in locals():
                id_max= models.Mail.get_max_id().fetchone()[0]
                orden= '1'

            models.Mail.create_chain(asunto,orden,editorial,texto,id_max)
            return redirect(url_for('chains'))

@app.route('/delete_chain/<id_chain>', methods=('GET','POST'))
def delete_chain(id_chain):
    flag = authentication()
    if flag == 1:
        return redirect(url_for('index'))
    
    if request.method == 'GET':
        cadenas = models.Mail.get_orden_texto(id_chain)
        return render_template('delete_chain.html', cadenas=cadenas)
    else:
        orden = request.form.get('orden')
        models.Mail.delete_chain(id_chain,orden)
        return redirect(url_for('chains'))

@app.route('/')
def index():
    try:
        if session['logged_in']:
            result =  models.Author.get_all()
            username = session['username']
            return render_template('index.html', result=result, username=username)
        else:
            flash(u'Logueate por favor', category='danger')
            return redirect(url_for('login'))
    except KeyError:
        flash(u'Logueate por favor', category='danger')
        return redirect(url_for('login'))

if __name__ == '__main__':
    app.run(debug=DEBUG, host=HOST, port=PORT)

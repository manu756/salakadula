select * from autores where email = :email;
select * from autores where registrador = :editor;
select * from autores where emailAutor in ( select emailAutor from libros where nombreEditorial = :editorial);
select * from autores where emailAutor in ( select emailAutor from libros where registro > current_date - interval ':dias' day);
select * from autores where registrador = :editor and registro > current_date - interval ':dias' day;

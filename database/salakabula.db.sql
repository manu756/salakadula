create table editores
(
     	username        	varchar(25),
	password		varchar(250),
	email			varchar(100),
	constraint pkEditor		primary key (username)
);



create table editoriales
(
	nombre				varchar(25),
	constraint pkEditorial		primary key (nombre)
);


create table editorialDelEditor
(
	nombreEditorial			varchar(25),
	usernameEditor			varchar(25),
	constraint pkEditorialEditor	primary key (nombreEditorial, username),
	constraint fkNombreEditorial	foreign key (nombreEditorial) references editoriales(nombre),
	constraint fkUsernameEditor	foreign key (username) references editores(username)
);


create table autores
(
	email				varchar(100),
	nombre				varchar(25),
	apellido			varchar(25),
	fechaRegistro			timestamp default current_timestamp,
	registrador			varchar(25),
	tipo				varchar(35),
	constraint pkAutor		primary key (email),
	constraint fkUsernameRegistrador	foreign key (registrador) references editores(username),
	constraint tipoValido		check(tipo in ('Escritor','Escritor-Ilustrador','Escritor con Ilustrador'))
);



create table telefonos
(
	numero				varchar(9),
	emailAutor			varchar(25),
	constraint pkTelefono		primary key (numero),
	constraint fkEmailAutor	foreign key(emailAutor) references Autores (email) on update cascade
);

create table comentarios
(
	emailAutor			varchar(25),
	usernameEditor			varchar(25),
	comentario			varchar(500),
	fecha				timestamp default current_timestamp,
	constraint pkComentario		primary key (emailAutor, usernameEditor, fecha),
	constraint fkEmailAutor		foreign key (emailAutor) references autores (email) on update cascade,
	constraint fkUsernameEditor	foreign key (usernameEditor) references editores (username)
);



create table libros
(
	nombre				varchar(30),
	nombreEditorial		varchar(25),
	emailAutor			varchar(25),
	registro			timestamp default current_timestamp,
	estado				varchar(20),
	constraint pkLibro		primary key (nombre, nombreEditorial, emailAutor),
	constraint fkNombreEditorial	foreign key (nombreEditorial) references editoriales (nombre),
	constraint fkEmailAutor		foreign key (emailAutor) references autores (email) on update cascade,
	constraint estadoValido	check(estado in ('Contratado','Desechado','En espera','En ejecución'))
	
);

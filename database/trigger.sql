CREATE PROCEDURAL LANGUAGE plpgsql;

create or replace function ActualizarEnvio() returns trigger as $ActualizarEnvio$
begin
	update recordatorios set enviando = 'NO' where emailautor = old.emailautor;
	return NULL;
end;
$ActualizarEnvio$ language plpgsql;

CREATE TRIGGER ActualizarEnvio
BEFORE DELETE on envios_correo
for each row
execute procedure ActualizarEnvio();

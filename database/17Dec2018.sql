--
-- PostgreSQL database dump
--

-- Dumped from database version 10.6 (Ubuntu 10.6-0ubuntu0.18.04.1)
-- Dumped by pg_dump version 10.6 (Ubuntu 10.6-0ubuntu0.18.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: actualizarenvio(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.actualizarenvio() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
begin
update recordatorios set enviando = 'NO' where emailautor = old.emailautor;
return NULL;
end;
$$;


ALTER FUNCTION public.actualizarenvio() OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: autores; Type: TABLE; Schema: public; Owner: manu
--

CREATE TABLE public.autores (
    email character varying(50) NOT NULL,
    nombre character varying(50),
    apellido character varying(50),
    fecharegistro timestamp without time zone DEFAULT now(),
    registrador character varying(25),
    tipo character varying(50) DEFAULT 'Escritor'::character varying,
    CONSTRAINT tipo_autores CHECK (((tipo)::text = ANY (ARRAY[('Escritor'::character varying)::text, ('Ilustrador'::character varying)::text, ('Ilustrador con escritor'::character varying)::text, ('Escritor-Ilustrador'::character varying)::text, ('Escritor con Ilustrador'::character varying)::text])))
);


ALTER TABLE public.autores OWNER TO manu;

--
-- Name: cadenas_correo; Type: TABLE; Schema: public; Owner: manu
--

CREATE TABLE public.cadenas_correo (
    id character varying(10) NOT NULL,
    orden character varying(10) NOT NULL,
    texto text,
    asunto character varying(200),
    editorial character varying(100)
);


ALTER TABLE public.cadenas_correo OWNER TO manu;

--
-- Name: comentarios; Type: TABLE; Schema: public; Owner: manu
--

CREATE TABLE public.comentarios (
    emailautor character varying(100) NOT NULL,
    usernameeditor character varying(100) NOT NULL,
    comentario character varying(500),
    fecha timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.comentarios OWNER TO manu;

--
-- Name: editores; Type: TABLE; Schema: public; Owner: manu
--

CREATE TABLE public.editores (
    username character varying(25) NOT NULL,
    password character varying(250)
);


ALTER TABLE public.editores OWNER TO manu;

--
-- Name: editorialdeleditor; Type: TABLE; Schema: public; Owner: manu
--

CREATE TABLE public.editorialdeleditor (
    nombreeditorial character varying(25) NOT NULL,
    usernameeditor character varying(25) NOT NULL
);


ALTER TABLE public.editorialdeleditor OWNER TO manu;

--
-- Name: editoriales; Type: TABLE; Schema: public; Owner: manu
--

CREATE TABLE public.editoriales (
    nombre character varying(25) NOT NULL
);


ALTER TABLE public.editoriales OWNER TO manu;

--
-- Name: envios_correo; Type: TABLE; Schema: public; Owner: manu
--

CREATE TABLE public.envios_correo (
    emailautor character varying(250) NOT NULL,
    idcorreo character varying(10) NOT NULL,
    fecha_envio timestamp without time zone,
    frecuencia character varying(10),
    correo_actual character varying(50) DEFAULT '1'::character varying,
    texto character varying(100000),
    fichero character varying(100)
);


ALTER TABLE public.envios_correo OWNER TO manu;

--
-- Name: libros; Type: TABLE; Schema: public; Owner: manu
--

CREATE TABLE public.libros (
    nombre character varying(30) NOT NULL,
    nombreeditorial character varying(25) NOT NULL,
    emailautor character varying(50) NOT NULL,
    registro timestamp without time zone DEFAULT now(),
    estado character varying(20),
    CONSTRAINT estadovalido CHECK (((estado)::text = ANY (ARRAY[('Contratado'::character varying)::text, ('Desechado'::character varying)::text, ('En espera'::character varying)::text, ('Pendiente'::character varying)::text])))
);


ALTER TABLE public.libros OWNER TO manu;

--
-- Name: recordatorios; Type: TABLE; Schema: public; Owner: manu
--

CREATE TABLE public.recordatorios (
    emailautor character varying(250) NOT NULL,
    nombre character varying(60),
    apellido character varying(60),
    enviando character varying(10) DEFAULT 'SI'::character varying
);


ALTER TABLE public.recordatorios OWNER TO manu;

--
-- Name: telefonos; Type: TABLE; Schema: public; Owner: manu
--

CREATE TABLE public.telefonos (
    numero character varying(20) NOT NULL,
    emailautor character varying(50)
);


ALTER TABLE public.telefonos OWNER TO manu;

--
-- Data for Name: autores; Type: TABLE DATA; Schema: public; Owner: manu
--

COPY public.autores (email, nombre, apellido, fecharegistro, registrador, tipo) FROM stdin;
vnbandera@hotmail.es	Veronica N	Bandera 	2016-03-09 10:38:00	Maria	Escritor
arcixformacion@gmail.com	Olga	Casanova 	2016-04-05 10:19:00	Maria	Escritor
juanlucas98@gmail.com	Juan	Lucas Onieva	2016-04-05 15:43:00	Maria	Escritor
victoriainglesrodrigo@gmail.com	Victoria	Ingles Rodrigo	2016-04-05 15:48:00	Maria	Escritor
apaquiva@hotmail.com	Francisca	García Malla	2016-05-13 14:05:00	Maria	Escritor
yasminarodriguez5@gmail.com	Yasmina	Rodríguez Gimeno	2016-05-24 14:11:00	Maria	Escritor
AlbaSanJuan1989@hotmail.com	Alba	San Juan 	2016-05-27 11:51:00	Maria	Escritor
aya1980_@hotmail.com	Africa Mª	Sánchez Sánchez	2016-06-01 16:28:00	Maria	Escritor
aslima22@hotmail.com	ISABEL Mª	ASENSI LIDÓN	2016-06-03 18:20:00	Maria	Escritor
design@lamartinique.es	Marta	Marqués Bataller	2016-06-08 11:00:00	Maria	Escritor
anaescobargp@gmail.com	Ana	Escobar 	2016-06-14 18:38:00	Maria	Escritor
info@fernando-artwork.com	Fernando	García 	2016-06-27 16:58:00	Maria	Escritor
palomaguerreroa@gmail.com	Paloma	Guerrero Arantave	2016-07-05 12:52:00	Maria	Escritor
mvictoria.becerra.castro@gmail.com	Victoria	Becerra Castro	2016-07-13 10:49:00	Maria	Escritor
gmulero@xtec.cat	Ginés	Mulero Caparrós	2016-07-22 12:48:00	Maria	Escritor
estimahh@hotmail.com	Miguel	Pérez 	2016-08-01 18:14:00	Maria	Escritor
escarlatasb@hotmail.com	Maribel	Bravo Zamora	2016-08-11 18:57:00	Maria	Escritor
pazlopez21@gmail.com	Paz	Lopez 	2016-08-22 19:00:00	Maria	Escritor
valeriamederer@hotmail.com	Valeria	Mederer 	2016-08-31 14:11:00	Maria	Escritor
sergioayensa@gmail.com	Sergio	Ayensa 	2016-09-05 18:04:00	Maria	Escritor
gloriia.gg@gmail.com	Gloria	Garcia Hernandez	2016-09-12 13:39:00	Maria	Escritor
gonzalezygarcia@gmail.com	Alberto	Gonzalez Garcia	2016-09-21 13:28:00	Maria	Escritor
anirmanx@yahoo.es	Marina	Arias 	2016-10-14 10:14:00	Maria	Escritor
miquel@accio.es	Miquel	Pujol Lozano	2016-10-25 17:07:00	Maria	Escritor
m_o_r_i_@hotmail.com	Helena	Morillas Rodriguez	2016-10-25 18:52:00	Maria	Escritor
cnlafay@gmail.com	Carme	Lafay 	2016-11-04 18:49:00	Maria	Escritor
estersevillalabora@gmail.com	Ester	Sevilla 	2016-11-08 16:32:00	Maria	Escritor
carmensubiza9@gmail.com	Carmen	Subiza 	2016-12-01 12:29:00	Maria	Escritor
bredakimkaty@hotmail.com	Katy	Castellon 	2016-12-09 16:44:00	Maria	Escritor
adrippi@adrippi.com	AGURTZANE	BENITO GARRIDO	2016-12-09 18:45:00	Maria	Escritor
fromthetree@yahoo.es	Luis	Ruiz Del Árbol	2016-12-12 10:16:00	Maria	Escritor
maykaponce@hotmail.com	Mayka	Perez Ponce	2016-12-14 11:56:00	Maria	Escritor
laclinicadellenguaje@gmail.com	Elena	Mesonero Gomez	2016-12-14 12:00:00	Maria	Escritor
marijosesorgina@gmail.com	María José	Gulin 	2016-12-21 18:46:00	Maria	Escritor
ale_moncherie@hotmail.com	Alejandra	.. 	2016-12-22 12:50:00	Maria	Escritor
vivianahabad@gmail.com	Viviana	Huaman 	2017-01-26 14:01:00	Maria	Escritor
mamaynaiaraleemos@gmail.com	Raquel 	Raquel Castaño Gonzalez Raquel Castaño Gonzalez	2017-01-30 16:37:00	Maria	Escritor
dvr90dvr90@gmail.com	Dario	Vargas Rodriguez	2017-02-02 11:18:00	Maria	Escritor
almuqf@gmail.com	Almudena	Quintanilla Fernández	2017-02-06 14:18:00	Maria	Escritor
javiroger86@gmail.com	JAVIER	SARDINERO GÓMEZ	2017-02-13 14:13:00	Maria	Escritor
saravilcer@gmail.com	SARA	VILLANUEVA CERCENADO	2017-03-02 16:30:00	Maria	Escritor
manuelacampo@hotmail.com	Manuela	Campo Vázquez	2017-03-21 18:15:00	Maria	Escritor
cristimoly@hotmail.com	Cristina	Molinero Pérez	2017-05-24 18:44:00	Maria	Escritor
silviaferama@gmail.com	Silvia	Fernández Amaya	2017-06-07 13:48:00	Maria	Escritor
inforaffologia@gmail.com	Rafael	Lucas 	2017-06-13 14:31:00	Maria	Escritor
mcsaheli@aol.com	M Carolina	Saheli 	2017-06-22 13:25:00	Maria	Escritor
estelamail2@gmail.com	Estela 	Franco 	2017-10-09 14:22:00	Maria	Escritor
hola@unapizcadeeducacion.com	Esti Y Patri	.. 	2017-10-18 16:49:00	Maria	Escritor
beatriz_winter@yahoo.es	Beatriz 	Winter  Verdú	2017-11-09 13:23:00	Maria	Escritor
mlm5013@hotmail.com	Mercedes 	Losada  Muriel	2017-12-19 17:19:00	Maria	Escritor
nuriamarfy@gmail.com	Marfy 	Ayrun 	2018-01-25 14:35:00	Maria	Escritor
mclopez81@hotmail.es	Mari CArmen	Lopez  Rosa	2018-02-01 12:30:00	Maria	Escritor
neus.ncc@gmail.com	Neus	Caamaño 	2016-02-11 18:46:00	marta	Escritor
beachaves@hotmail.com	BEATRIZ	BEATRIZ CUÑADO	2016-03-01 17:20:00	Maria	Escritor
tikituku13@gmail.com	Alberto	Mendoza 	2016-03-01 17:48:00	Maria	Escritor
gema9falda@gmail.com	Gema	Palomo 	2016-03-02 10:15:00	Maria	Escritor
marina.forato@gmail.com	Marina	Ammar Forato	2016-03-02 11:34:00	Maria	Escritor
info@manueltristante.com	Manuel	Tristante 	2016-03-02 13:13:00	Maria	Escritor
mvelagarcia@gmail.com	María José	Vela García	2016-03-03 10:20:00	Maria	Escritor
angelicoprieto@gmail.com	Angel	Prieto Prieto	2016-03-04 12:19:00	Maria	Escritor
agnese.ermacora@gmail.com	Agnese	Ermacora 	2016-03-09 11:26:00	Maria	Escritor
cancelado@msn.com	Juan Manuel	Moreno 	2016-03-09 11:35:00	Maria	Escritor
BeatrizAyalaCoach@hotmail.com	Beatriz	Ayala 	2016-03-09 11:42:00	Maria	Escritor
mayfloresmanzano@gmail.com	May	Flores Manzano	2016-03-11 14:09:00	Maria	Escritor
franpulido1@gmail.com	Francisco	Pulido Rios	2016-03-14 18:12:00	Maria	Escritor
leramalaga@hotmail.com	Valeria	Kiselova 	2016-03-18 09:47:00	Maria	Escritor
ortegatorresbego@gmail.com	Begoña Ortega	Ortega 	2016-03-18 13:42:00	Maria	Escritor
olga.portero@gmail.com	Olga	Sanchez 	2016-03-22 12:20:00	Maria	Escritor
djboyes@hotmail.com	Dave	Boyes 	2016-03-22 12:22:00	Maria	Escritor
estherpinus@gmail.com	Esther	Pino 	2016-03-22 13:34:00	Maria	Escritor
maria.mayor@gmail.com	María	Mayor 	2016-03-30 12:09:00	Maria	Escritor
ayj170498@gmail.com	Ana Esmeralda	Piña Recuenco	2016-03-30 13:19:00	Maria	Escritor
artistgala@hotmail.com	Gala	Peláez San José	2016-03-30 13:24:00	Maria	Escritor
mj.llorach@gmail.com	María José	Llorach Masip	2016-03-31 12:16:00	Maria	Escritor
pepe.maria.prada@gmail.com	Jose Maria	Prada 	2016-04-04 16:36:00	Maria	Escritor
emiliagarciaserna1@gmail.com	Emilia	García Serna	2016-04-04 18:51:00	Maria	Escritor
pepamayo1@hotmail.com	Pepa	Mayo 	2016-04-05 10:56:00	Maria	Escritor
norigairam@gmail.com	María	Girón 	2016-04-05 13:37:00	marta	Escritor
davidmulla@hotmail.com	David	Muñoz 	2016-04-06 12:23:00	Maria	Escritor
teregr2003@yahoo.com	Teresa	Galiano 	2016-04-06 13:49:00	Maria	Escritor
pauforaster@gmail.com	Paulina	Foraster Aspe	2016-04-11 10:55:00	Maria	Escritor
golden.orlando@gmail.com	Orlando	Rivas 	2016-04-11 11:23:00	Maria	Escritor
manuel.goberman@gmail.com	Manuel	Bermudez 	2016-04-11 11:29:00	Maria	Escritor
Misuperclase@hotmail.com	María	Aa 	2016-04-12 11:36:00	Maria	Escritor
Nereabeni@hotmail.com	Nerea	Aa 	2016-04-12 11:40:00	Maria	Escritor
yrudysacosufusil@hotmail.com	Ana Isabel	Tomás Ramírez	2016-04-12 11:56:00	Maria	Escritor
raul.not1988@gmail.com	Raúl	Canales 	2016-04-12 12:00:00	Maria	Escritor
julianmontesinos@gmail.com	Julian	Montesinos Ruiz	2016-04-12 12:13:00	Maria	Escritor
Paolaisabelsantos@gmail.com	Paola	I Santos 	2016-04-12 12:24:00	Maria	Escritor
karingalvis@gmail.com	Karin	Galvis 	2016-04-12 12:29:00	Maria	Escritor
umeirakasle@gmail.com	Raúl	De La Cruz Mate	2016-04-12 12:32:00	Maria	Escritor
dlanzabarba@gmail.com	Daniel	Lanza 	2016-04-12 12:40:00	Maria	Escritor
rafael.mora.espejo@juntadeandalucia.es	Rafael	Mora Espero	2016-04-12 13:19:00	Maria	Escritor
isabelbelmonte@outlook.es	Isabel	Belmonte 	2016-04-13 11:32:00	Maria	Escritor
mercehomar@hotmail.com	Mercè	Homar Mas	2016-04-13 11:34:00	Maria	Escritor
fran@eraestelar2012.com	Francisco José	Ortega Estrella	2016-04-15 13:28:00	Maria	Escritor
yucudani2@hotmail.com	CARLOS VICENTE	RAMIREZ GARCIA	2016-04-15 13:30:00	Maria	Escritor
letraenye@yahoo.es	Noemi	Martínez Guirles	2016-04-15 13:32:00	Maria	Escritor
catc78@yahoo.com	Carlos Andrés	Corral Corral	2016-04-18 10:20:00	Maria	Escritor
ernesto-mtz94@hotmail.com	Ernesto	Martínez Jiménez	2016-04-18 11:06:00	Maria	Escritor
jeydilibros@gmail.com	Jesús Y Daniela	González Yumar	2016-04-19 13:50:00	Maria	Escritor
srnumero2@gmail.com	Antonio	Díaz 	2016-04-20 09:57:00	Maria	Escritor
elrincondeloli@gmail.com	Mariló	Fallereta 	2016-04-20 10:02:00	Maria	Escritor
silviapereznebot@yahoo.es	Silvia	Perez Nebot	2016-04-20 18:47:00	marta	Escritor
milaruizpastor@gmail.com	Mila	Aa 	2016-04-21 11:26:00	Maria	Escritor
sara_22-2-1996@hotmail.com	Sara	Silvar Gómez	2016-04-21 11:30:00	Maria	Escritor
jsefisg27@gmail.com	Chum	Aa 	2016-04-21 11:49:00	Maria	Escritor
romannSM@hotmail.com	Roman	Sanz Mouta	2016-04-21 12:39:00	Maria	Escritor
patri.musica@hotmail.es	Patricia	García Sánchez	2016-04-22 10:13:00	Maria	Escritor
bosquedefantasias@gmail.com	Bosque Fantasia	Aa 	2016-04-22 12:39:00	Maria	Escritor
Jakedegures@gmail.com	Joaquín	Fiúza 	2016-04-26 11:16:00	Maria	Escritor
thelittleshell@gmail.com	Concxita	Tunica 	2016-04-26 12:08:00	Maria	Escritor
marialopez110514@gmail.com	María	López 	2016-05-04 13:44:00	Maria	Escritor
mjsalejop@hotmail.com	MANUEL	JIMENEZ SANZ	2016-05-05 11:56:00	Maria	Escritor
gerenciaprr@gmail.com	Patricio	Rubel Rubinstein	2016-05-05 14:17:00	Maria	Escritor
sara_cienfu@hotmail.com	Sara	Cienfuegos 	2016-05-06 14:18:00	Maria	Escritor
carolina.i.quesada@gmail.com	Carolina	Iñesta 	2016-05-06 14:22:00	Maria	Escritor
martina93@hotmail.com	Martina	Gallo Galitiello	2016-05-09 13:21:00	Maria	Escritor
saqueslopez@yahoo.es	Xose Luis	Saqués 	2016-05-10 11:17:00	Maria	Escritor
sndra.m.v@gmail.com	Sandra	Martínez Vidal	2016-05-10 13:26:00	Maria	Escritor
crespocarrillosonia@gmail.com	Sonia	Crespo Carrillo	2016-05-11 11:00:00	Maria	Escritor
tlinaresm@gmail.com	Antonia	Linares Morales	2016-05-13 13:53:00	Maria	Escritor
laurapp27@outlook.es	Lidia Y Laura	Aa 	2016-05-13 14:15:00	Maria	Escritor
severine.dhooge@sfr.fr	Severine	DHOOGE 	2016-05-16 16:38:00	Maria	Escritor
miguelcopyfree@gmail.com	Miguel Angel	Prados 	2016-05-16 18:41:00	Maria	Escritor
gaur@gmx.es	Esteban	Etxebarria 	2016-05-16 18:45:00	Maria	Escritor
inigomonreal97@gmail.com	Iñigo	Monreal Legarda	2016-05-17 14:08:00	Maria	Escritor
antonio.martinez82@hotmail.com	Antonio	Martinez 	2016-05-17 14:18:00	Maria	Escritor
alexfalconillustrator@gmail.com	Alex	Falcon 	2016-05-23 13:27:00	Maria	Escritor
jguasp@gmail.com	Jorge	Guasp 	2016-05-23 13:29:00	Maria	Escritor
tomasbases@gmail.com	Tomas	Bases Hernandez	2016-05-23 14:04:00	Maria	Escritor
pazoshermida@gmail.com	Silvia	Pazos Hermida	2016-05-23 14:19:00	Maria	Escritor
torrente_lugones@hotmail.com	Gema	Murciano Martin	2016-05-25 11:31:00	Maria	Escritor
nilotrovo@hotmail.com	Nilo	Trovo 	2016-05-25 12:45:00	Maria	Escritor
caca.franca@gmail.com	Clarissa	França 	2016-05-26 10:07:00	Maria	Escritor
lmartini1@bbox.fr	Laetitia	Martini 	2016-05-27 12:01:00	Maria	Escritor
mariandresmarti@gmail.com	Maria	Andres Marti	2016-05-30 13:29:00	Maria	Escritor
soles6507@gmail.com	Soledad	García 	2016-06-01 13:32:00	Maria	Escritor
maneroriverola@gmail.com	María	Manero Riverola	2016-06-02 11:29:00	Maria	Escritor
evaruiz7@gmail.com	Eva	Ruiz 	2016-06-02 12:47:00	Maria	Escritor
khaleesi.khaleesi@aol.com	Daeneris	Khaleesi 	2016-06-06 14:39:00	Maria	Escritor
rebecablanco50@magisteriosc.es	Rebeca	Blanco Aranda	2016-06-07 12:47:00	Maria	Escritor
lumbricita@gmail.com	Viviana	Palma 	2016-06-07 13:22:00	Maria	Escritor
inmall93@hotmail.com	Inma	López 	2016-06-07 14:29:00	Maria	Escritor
aimurakaji@gmail.com	Efraín	Villamor 	2016-06-09 11:40:00	Maria	Escritor
daaanix@yahoo.es	Daniel	Garcia 	2016-06-09 11:44:00	Maria	Escritor
adriansanchezplaza@gmail.com	Adrian	Sanchez Plaza	2016-06-09 12:00:00	Maria	Escritor
djg.forense@gmail.com	Daniel	Jiménez Gómez	2016-06-13 14:25:00	Maria	Escritor
pilar62tc@gmail.com	Pilar	Torres 	2016-06-15 11:17:00	Maria	Escritor
patnieto2011@gmail.com	Patricia	Nieto Jiménez	2016-06-15 12:08:00	Maria	Escritor
carolina.valtuille@gmail.com	Carolina	Valtuille 	2016-06-15 12:43:00	Maria	Escritor
montana.campon@terra.com	Montaña	Campon 	2016-06-17 17:42:00	Maria	Escritor
lucia.rios@outlook.com	Lucía	Ríos 	2016-06-20 12:05:00	Maria	Escritor
anamaria.navalon@gmail.com	Ana Maria	Navalon Valera	2016-06-20 12:10:00	Maria	Escritor
rebecablanco50@gmail.com	Rebeca	Blanco 	2016-06-20 12:44:00	Maria	Escritor
jopelo109@hotmail.com	Jorge	Perez 	2016-06-20 16:53:00	Maria	Escritor
leno@diegolenoart.com	DIEGO	LENO TRESPALACIOS	2016-06-24 17:29:00	Maria	Escritor
hijodesious@hotmail.com	Rubén	Pacheco Rondero	2016-06-28 11:58:00	Maria	Escritor
bigworldernight@gmail.com	Ruth	Barat 	2016-06-30 13:35:00	Maria	Escritor
lopezyasmin@hotmail.es	Yasmín	Lópeo 	2016-06-30 18:18:00	Maria	Escritor
leiremayendia@gmail.com	Leire	Mayendia 	2016-07-04 11:25:00	Maria	Escritor
canalconstantina@hotmail.com	Montse	Heraiz 	2016-07-04 12:08:00	Maria	Escritor
helena18mg@hotmail.com	Elena	Martínez 	2016-07-04 16:47:00	Maria	Escritor
erika.catalan@gmail.com	Erika	Catalán Moreno	2016-07-06 10:49:00	Maria	Escritor
kyla_jessy-k@hotmail.com	Jessica	Aa 	2016-07-06 14:37:00	Maria	Escritor
mariajezfez@yahoo.es	María	Jiménez 	2016-07-11 13:42:00	Maria	Escritor
maypepon@gmail.com	Mayka	Pérez 	2016-07-11 13:51:00	Maria	Escritor
adrianaproduccionesgraficas@gmail.com	Adriana	Italiano 	2016-07-11 14:01:00	Maria	Escritor
nbpamies@gmail.com	Natalia	Berlanga Pamies	2016-07-11 14:07:00	Maria	Escritor
carmecolomer@hotmail.com	Carme	Colomer 	2016-07-11 14:25:00	Maria	Escritor
charomm@us.es	María Del Rosario	Martín Muñoz	2016-07-13 11:09:00	Maria	Escritor
ifortunyp@gmail.com	Ignasi	Fortuny 	2016-07-13 13:18:00	Maria	Escritor
ainogatap23@hotmail.com	Alicia	López De Heredia	2016-07-13 14:01:00	Maria	Escritor
mpascualnavas@gmail.com	Maria	Pascual Navas	2016-07-18 13:06:00	Maria	Escritor
anhermart@gmail.com	Andrés	Hernández Martínez	2016-07-18 13:21:00	Maria	Escritor
patryyy21@hotmail.com	Patry	YY 	2016-07-18 13:47:00	Maria	Escritor
miranda_naranjo_romina@hotmail.com	Romina	Miranda Naranjo	2016-07-18 13:55:00	Maria	Escritor
rojasho@ldschurch.org	Homero	Rojas 	2016-07-18 16:22:00	Maria	Escritor
camilla.cecilia.storie01@gmail.com	Camilla	Cecilia 	2016-07-19 13:24:00	Maria	Escritor
marianae82@gmail.com	Mariana	Morlio 	2016-07-19 14:27:00	Maria	Escritor
angelalonsovelasco@gmail.com	Ángela	Alonso 	2016-07-19 14:36:00	Maria	Escritor
arnmararan@gmail.com	Aran	Arnau 	2016-07-19 18:44:00	Maria	Escritor
Aninagarcia64@gmail.com 	Ana Maria	Garcia Nuñez	2016-07-22 16:49:00	Maria	Escritor
mortizechazu@gmail.com	María Virginia	Ortiz Echazú	2016-07-27 18:35:00	Maria	Escritor
j.monzon72@hotmail.com	Juan	Monzón Barrado	2016-08-01 12:19:00	Maria	Escritor
bnogueiragonzalez@gmail.com	Blanca	Nogueira 	2016-08-01 14:14:00	Maria	Escritor
starofmoonalexa@gmail.com	Alexandra	Galindo 	2016-08-02 18:33:00	Maria	Escritor
aralua@hotmail.com	Lucía	Cirujano Mata	2016-08-03 12:11:00	Maria	Escritor
cuenterojhonardila@gmail.com	Jhon	Ardila 	2016-08-03 13:41:00	Maria	Escritor
teatro_vigo@hotmail.com	Nerea	Garrido Noya	2016-08-03 17:46:00	Maria	Escritor
pinki_74@hotmail.com	Tania	Martinez Rodriguez	2016-08-05 16:05:00	Maria	Escritor
annarosacar@hotmail.com	Anna Rosa	Car 	2016-08-05 16:40:00	Maria	Escritor
gongru88@hotmail.com	Matías Gastón	Gonzalez Gruman	2016-08-05 16:51:00	Maria	Escritor
iglesiasmartinez@yahoo.es	María Del Mar	Iglesias Martínez	2016-08-08 18:15:00	Maria	Escritor
gab.elena.a@gmail.com	Gabriela	Alessandroni 	2016-08-08 18:40:00	Maria	Escritor
sklonvo@gmail.com	Sergio	Colombo Ruiz	2016-08-09 17:10:00	Maria	Escritor
yolandaloroy@gmail.com	Yolanda	Lopez Rodriguez	2016-08-09 18:37:00	Maria	Escritor
eguerra@xtec.cat	Elisenda	Guerra Juliá	2016-08-10 18:27:00	Maria	Escritor
infantilescuentos@hotmail.es	M Begoña	Lisón Nuez	2016-08-16 13:22:00	Maria	Escritor
rosalc20039@hotmail.com	ROSA	LEDO CALVO	2016-08-16 13:31:00	Maria	Escritor
Sandra.Bongjoh67@gmail.com	Sandra	Bongjoh 	2016-08-22 13:45:00	Maria	Escritor
vicki.rawa@gmail.com	Victoria	Rawa 	2016-08-26 17:11:00	Maria	Escritor
ebasualdo18@gmail.com	Juan Eduardo	Basualdo 	2016-08-30 17:41:00	Maria	Escritor
fatimanoel2@gmail.com	FÃ¡tima	Lema Caamaño	2016-09-02 18:03:00	Maria	Escritor
antonioarilla@hotmail.com	ANTONIO	Arilla Begueria	2016-09-08 13:05:00	Maria	Escritor
nadiacarreiranunez@gmail.com	Nadia	Carreira Nuñez	2016-09-12 18:50:00	Maria	Escritor
rosarioochoa87@gmail.com	Rosario	Ochoa 	2016-09-14 18:22:00	Maria	Escritor
andonigone4@gmail.com	Andoni	Godoy 	2016-09-20 12:04:00	Maria	Escritor
luisfercerezo@gmail.com	NAN	Cerezo 	2016-09-23 14:23:00	Maria	Escritor
marian@muzak.com.mx	Marián	Valdés 	2016-09-28 18:12:00	Maria	Escritor
cleofer_22@hotmail.com	Digna	Cleofer Diaz	2016-09-28 18:49:00	Maria	Escritor
pepigonzalezplanelles@gmail.com	Josefa	González Planelles	2016-10-03 12:08:00	Maria	Escritor
mandujar@xtec.cat	Carmen	Andújar Zorrilla	2016-10-03 14:35:00	Maria	Escritor
ica73@live.com	Mercedes	Arana Vargas	2016-10-03 16:30:00	Maria	Escritor
raquel_8_1996@hotmail.com	Raquel	De La Rocha 	2016-10-03 16:51:00	Maria	Escritor
sergio.cirer@hotmail.es	Sergio	Cirer Buendía	2016-10-04 18:25:00	Maria	Escritor
1977abad@gmail.com	M Del Mar	Abad 	2016-10-04 18:44:00	Maria	Escritor
teresacollsanmartinilustradora@gmail.com	Teresa	Coll Sanmartín	2016-10-05 12:55:00	Maria	Escritor
yolfelice@yahoo.es	Yolanda	Felice 	2016-10-05 13:55:00	Maria	Escritor
blas.rueda@gmail.com	Blas	Rueda García	2016-10-05 14:37:00	Maria	Escritor
carsamo9@gmail.com	Carolina	Sanchez Molero	2016-10-05 16:49:00	Maria	Escritor
joselunada@hotmail.com	María José	Escalante 	2016-10-05 17:05:00	Maria	Escritor
bmedranogutierrez@hotmail.com	Blanca	Medrano 	2016-10-06 14:08:00	Maria	Escritor
jorgeperezlozano@gmail.com	Jorge	Jiménez Crespo	2016-10-06 17:58:00	Maria	Escritor
milarondoo@gmail.com	Mila	Rondo 	2016-10-07 12:40:00	Maria	Escritor
fgca1vivazapata@gmail.com	Fernando	García Carrasco	2016-10-11 17:07:00	Maria	Escritor
eulaliagarciacayuela2013@gmail.com	EULALIA	.. 	2016-10-13 14:14:00	Maria	Escritor
rosita832110@gmail.com	Rosa Maria	Hernandez Sosa	2016-10-13 18:12:00	Maria	Escritor
elrincondepardo@gmail.com	ALBERTO	PARDO IBÁÑEZ	2016-10-13 18:22:00	Maria	Escritor
polyros03@hotmail.com	Paola	Pastorino 	2016-10-14 11:35:00	Maria	Escritor
jsintes2@gmail.com	José	Sintes 	2016-10-14 17:52:00	Maria	Escritor
paugraci@tinet.cat	Gracia	Moreno 	2016-10-17 14:36:00	Maria	Escritor
alejaguapa@gmail.com	Alejandra	Guapacha 	2016-10-17 14:39:00	Maria	Escritor
rosadevientos12@gmail.com	Christina	.. 	2016-10-17 18:38:00	Maria	Escritor
magdafire@gmail.com	Magdalena	Castillo 	2016-10-18 10:44:00	Maria	Escritor
benjulopez@gmail.com	Miguel Ángel	Benjumea 	2016-10-18 17:55:00	Maria	Escritor
currocorre@yahoo.es	Mamen	Hernández 	2016-10-19 17:59:00	Maria	Escritor
amapolaolaclown@gmail.com	Gema	.. 	2016-10-20 17:11:00	Maria	Escritor
francmontesinos@hotmail.com	Francisco Jóse	.. 	2016-10-20 17:47:00	Maria	Escritor
miannaselka@gmail.com	Mianna	Vilalta Raspau	2016-10-21 19:06:00	Maria	Escritor
md@mdimage.es	Margarita	.. 	2016-10-24 18:37:00	Maria	Escritor
macopeca19@gmail.com	Marta	Cornello Perez-Calderon	2016-10-25 17:09:00	Maria	Escritor
craggraul@hotmail.com	Raúl	Cragg 	2016-10-25 17:34:00	Maria	Escritor
josepfrailegarcia@hotmail.com	Anna	Garcia 	2016-10-27 10:06:00	Maria	Escritor
beuka1234@gmail.com	Beatriz	Pascual Del Prado	2016-10-27 10:40:00	Maria	Escritor
rhidalgofernandez@gmail.com	Rosario	.. 	2016-10-28 13:58:00	Maria	Escritor
cecibumcjs@gmail.com	Cecilia	Jiménez Serna	2016-10-28 17:27:00	Maria	Escritor
niviolv@gmail.com	Nivio	López Vigil	2016-10-28 17:40:00	Maria	Escritor
jsferez@hotmail.com	JOSE	SALVADOR FÉREZ	2016-10-31 13:31:00	Maria	Escritor
rosigranada@hotmail.com	Rosy	Martinez Tello	2016-10-31 13:35:00	Maria	Escritor
puntitochimpun@gmail.com	Inma	Muñoz 	2016-10-31 14:18:00	Maria	Escritor
virginia.vera@expericuentos.es	Virginia	Vera 	2016-10-31 14:22:00	Maria	Escritor
tuchicalibelula@gmail.com	Natalia	Martinez Perez	2016-11-02 17:30:00	Maria	Escritor
mari_marcos@hotmail.com	Mariola	Marcos 	2016-11-02 17:33:00	Maria	Escritor
rodmati@hotmail.com	Matilde	.. 	2016-11-02 17:43:00	Maria	Escritor
1974gll.cienife@gmail.com	German	Lopez Germán	2016-11-02 17:48:00	Maria	Escritor
info@nanaribeiro.com	Nana	Ribeiro Larravide	2016-11-03 14:08:00	Maria	Escritor
m.vinardell@hotmail.com	Mónica	Vinardell Gross	2016-11-03 14:14:00	Maria	Escritor
alfredo.becker.c@gmail.com	Alfredo	Becker 	2016-11-04 16:38:00	Maria	Escritor
hugosailing@icloud.com	Hugo	Ramón 	2016-11-08 14:31:00	Maria	Escritor
david.ladero.illustration@gmail.com	David	Ladero Ilustracion	2016-11-14 12:31:00	Maria	Escritor
poetapedrojavier@gmail.com	Pedro	Javier Martínez	2016-11-14 14:38:00	Maria	Escritor
fio_perdomo@hotmail.com	Fiorella	Perdomo 	2016-11-14 16:43:00	Maria	Escritor
Liamanthony39@gmail.com	Liam	Anthony 	2016-11-14 16:45:00	Maria	Escritor
lilalayers@gmail.com	Lila	Layers 	2016-11-14 18:38:00	Maria	Escritor
lile312000@yahoo.it	Ilenia	Rosati 	2016-11-15 10:55:00	Maria	Escritor
lcrespillo@gmail.com	Laura	Crespillo López	2016-11-18 11:35:00	Maria	Escritor
cataplinaencuentada@gmail.com	Catalina	Martinez-moro 	2016-11-18 12:26:00	Maria	Escritor
lauracervera_84@hotmail.com	Laura	Cervera 	2016-11-18 12:38:00	Maria	Escritor
lourdesdiazl73@gmail.com	Lourdes	Díaz 	2016-11-18 13:35:00	Maria	Escritor
vasile370@yahoo.es	Ionela	Lazar 	2016-11-18 17:42:00	Maria	Escritor
asiersanznieto@gmail.com	Asier	Sanz 	2016-11-18 18:03:00	Maria	Escritor
gazzigiovanna@gmail.com	GIOVANNA	GAZZI 	2016-11-21 18:47:00	Maria	Escritor
verdejade77@gmail.com	Mamen	.. 	2016-11-22 12:10:00	Maria	Escritor
joseypatri2009@gmail.com	Patricia	Sanchez Rodilla	2016-11-22 12:41:00	Maria	Escritor
alonsanchez_23@hotmail.com	Alone	Sánchez 	2016-11-22 17:33:00	Maria	Escritor
duendurky@hotmail.com	Pilar	Urquia 	2016-11-24 12:06:00	Maria	Escritor
animontalvo@gmail.com	Ani	Montalvo 	2016-11-29 11:21:00	Maria	Escritor
ema.comanicu@gmail.com	Ema	Coman 	2016-11-30 12:36:00	Maria	Escritor
liamanthony87@icloud.com	Liam	Anthony 	2016-12-09 10:12:00	Maria	Escritor
marta.mena86@gmail.com	Marta	Mena 	2016-12-09 18:36:00	Maria	Escritor
ticontilatiti@gmail.com	Toni	Cif 	2016-12-09 18:54:00	Maria	Escritor
palopher@gmail.com	Paloma	Lopez Hernandez	2016-12-13 12:25:00	Maria	Escritor
guyunuso@gmail.com	Juan Manuel	Toledo Amorìn	2016-12-13 18:03:00	Maria	Escritor
danilogonzalez21@hotmail.com	Danilo	Gonzalez 	2016-12-14 12:52:00	Maria	Escritor
gallego_195@hotmail.com	Oscar	Gallego 	2016-12-14 13:51:00	Maria	Escritor
stgonper@gmail.com	Samuel	González Pérez	2016-12-14 19:03:00	Maria	Escritor
sorayabartolome@gmail.com	Soraya	Bartolome 	2016-12-15 11:37:00	Maria	Escritor
andresochandres@gmail.com	Mercedes	Arguello 	2016-12-15 12:07:00	Maria	Escritor
peke85sds@hotmail.com	Silvia	Salvado 	2016-12-16 11:55:00	Maria	Escritor
karloff.garcia@gmail.com	Karloff	García 	2016-12-19 10:48:00	Maria	Escritor
avferreiro@gmail.com	Alberto	Varela 	2016-12-19 12:44:00	Maria	Escritor
poly.erika.campoverde@hotmail.com	Erika	Campoverde 	2016-12-19 14:23:00	Maria	Escritor
lourdesatlb@yahoo.es	Lourdes	.. 	2016-12-21 18:11:00	Maria	Escritor
idrojgim@hotmail.com	Jorge	.. 	2016-12-21 18:14:00	Maria	Escritor
aliciabtm@hotmail.com	Alicia Belén	Torres Muñoz	2016-12-21 18:31:00	Maria	Escritor
tommyllorens@yahoo.es	Tommy	Llorens 	2016-12-27 12:00:00	Maria	Escritor
CarlosLopezPerez300@hotmail.com	Carlos	Lopez 	2016-12-27 18:50:00	Maria	Escritor
Ivanchi1123@hotmail.com	Ivan	Chica 	2016-12-29 10:17:00	Maria	Escritor
angmarov13@gmail.com	Ángela	Martínez Oviedo	2016-12-29 10:31:00	Maria	Escritor
elisagargo@gmail.com	Elisa	.. 	2016-12-29 10:41:00	Maria	Escritor
diegovaya@hotmail.com	Diego	Vaya 	2016-12-29 12:39:00	Maria	Escritor
carmen.nino@madrid.org	CARMEN	NIÑO GUTIERREZ	2016-12-30 13:42:00	Maria	Escritor
lacasitoz@outlook.com	Lacasitoz	Lacasitoz 	2016-12-30 16:35:00	Maria	Escritor
naiara.vidal@hotmail.com	Naiara	Vidal 	2016-12-30 18:20:00	Maria	Escritor
jldiazmarcos@gmail.com	José Luis	Díaz Marcos	2017-01-03 12:30:00	Maria	Escritor
gpuigoriol@icloud.com	Gerard	Puigoriol 	2017-01-04 17:12:00	Maria	Escritor
guyunuso@hotmail.com	Manuel	Toledo 	2017-01-04 17:27:00	Maria	Escritor
antonioreyes_1975@hotmail.com	ANTONIO	REYES NUÑO	2017-01-04 18:07:00	Maria	Escritor
parnaus@gmail.com	Pilar	Arnaus 	2017-01-04 18:11:00	Maria	Escritor
estebanbargas@gmail.com	Esteban	Bargas 	2017-01-05 11:41:00	Maria	Escritor
salmaia@yahoo.com	Ana	Galiana Jaime	2017-01-05 12:10:00	Maria	Escritor
j.guiralse@yahoo.es	Jesus	Guiral 	2017-01-10 17:48:00	Maria	Escritor
namoroulgu@yahoo.es	Nacho	Moreno 	2017-01-11 11:16:00	Maria	Escritor
prudencioterceronieto@gmail.com	Pruden	Tercero Nieto	2017-01-11 19:02:00	Maria	Escritor
edermoises141@gmail.com	Eder	Uzcategui 	2017-01-13 12:41:00	Maria	Escritor
meli.moreira@yahoo.es	Melina	Fernandez 	2017-01-13 18:27:00	Maria	Escritor
evita_pg@hotmail.com	Eva	Pérez 	2017-01-16 17:57:00	Maria	Escritor
alfredobalbascampo@gmail.com	Alfredo	Balbás Campo	2017-01-18 12:49:00	Maria	Escritor
cristina.anta@icloud.com	Cristina	Anta 	2017-01-18 12:53:00	Maria	Escritor
gabarri77@gmail.com	Nuria	Gabarron 	2017-01-19 17:35:00	Maria	Escritor
sara8@hotmail.com	Sara	.. 	2017-01-20 18:17:00	Maria	Escritor
andreagailustra@gmail.com	Andrea	Galecio 	2017-01-20 18:24:00	Maria	Escritor
mariabeltranfernandez@gmail.com	Maria	BF 	2017-01-20 18:42:00	Maria	Escritor
rutherojo@hotmail.com	Ruth	Rojo 	2017-01-26 18:32:00	Maria	Escritor
guirnaldo@hotmail.com	Guirnaldo	Fernández Guzman	2017-01-30 11:20:00	Maria	Escritor
rainkproject@gmail.com	Javier	Conde 	2017-01-30 17:35:00	Maria	Escritor
ivan.fedechko@gmail.com	Ivan	Fedechko 	2017-01-30 17:52:00	Maria	Escritor
quino.losan@gmail.com	Joaquin	.. 	2017-01-30 18:16:00	Maria	Escritor
edgraphandbass@gmail.com	Edgar	Flores 	2017-02-01 18:16:00	Maria	Escritor
egrauhoms@gmail.com	Elisabeth	Grau 	2017-02-03 11:39:00	Maria	Escritor
guidoroiz@gmail.com	Guillermo	Gonzalez 	2017-06-07 13:30:00	Maria	Escritor
elaventurerodepapel@gmail.com	Ana	Castro Manzanares	2017-02-06 13:52:00	Maria	Escritor
decospg@gmail.com	Patricia	De Cos 	2017-02-07 11:54:00	Maria	Escritor
originaldakotan@yahoo.com	Rod	Jensen 	2017-02-08 10:58:00	Maria	Escritor
claratudela@outlook.es	Clara	Tudela Valencia	2017-02-08 18:23:00	Maria	Escritor
lienamarlita@gmail.com	Marta	Liena 	2017-02-09 16:26:00	Maria	Escritor
Nheabad@hotmail.com	Nohemi	Abad Jiménez	2017-02-13 13:43:00	Maria	Escritor
jasbleidy.lopez.a@gmail.com	Jasbleidy	Lopez 	2017-02-13 16:27:00	Maria	Escritor
lunattiko@hotmail.com	Javi	Lyky 	2017-02-13 17:11:00	Maria	Escritor
nardinaiz@gmail.com	Nardi	Gomez 	2017-02-14 18:03:00	Maria	Escritor
boscola66@gmail.com	Carles	Paez 	2017-02-15 11:22:00	Maria	Escritor
carloselx73@gmail.com	Carloselx	.. 	2017-02-15 11:52:00	Maria	Escritor
mahytemorales16@gmail.com	Mahyte	Morales 	2017-02-20 17:14:00	Maria	Escritor
contabilidad.arostegui@gmail.com	Marco Antonio	Aróstegui Alfaro	2017-02-20 17:23:00	Maria	Escritor
cristinapenaandres@gmail.com	Cristina	Peña 	2017-02-22 18:36:00	Maria	Escritor
MARIAN_MAESTRA@hotmail.com	MARIA ÁNGELES	.. 	2017-03-01 17:27:00	Maria	Escritor
a.espadas@ono.com	Antonia	Espadas Pérez	2017-03-01 17:39:00	Maria	Escritor
deboravega93@gmail.com	Débora	Vega 	2017-03-02 16:20:00	Maria	Escritor
xohox2013@gmail.com	Gabriel	Lerman 	2017-03-08 09:56:00	Maria	Escritor
olgablazquez@gmail.com	Olga	Blázquez Sánchez	2017-03-10 14:59:00	Maria	Escritor
akay1971@gmail.com	Juan Carlos	Pérez Rodriguez	2017-03-13 13:41:00	Maria	Escritor
gregdriverdeveloper@gmail.com	Gregorio José	Sánchez Montes	2017-03-13 13:44:00	Maria	Escritor
ireenee.rv@gmaill.com	Irene	Romero Vilas	2017-03-13 13:47:00	Maria	Escritor
afmdiwi@gmail.com	Amanda	Flores 	2017-03-13 13:50:00	Maria	Escritor
sotojorgefelix@gmail.com	Jorge Felix	Soto González	2017-03-13 14:02:00	Maria	Escritor
Rosasol23@hotmail.com	Mileidys	Villarreal 	2017-03-13 14:06:00	Maria	Escritor
MASTER@rosaspage.com	Xavier	Arumi Salavedra	2017-03-13 14:15:00	Maria	Escritor
francescnogales@gmail.com	Francesc	Nogales 	2017-03-15 18:45:00	Maria	Escritor
elena.mcubillos@gmail.com	Elena	Martín 	2017-03-21 18:10:00	Maria	Escritor
silvialopezchillaron@gmail.com	Silvia	López Chillarón	2017-03-21 18:13:00	Maria	Escritor
isabelle.aragon@hotmail.be	Isabelle	.. 	2017-03-22 11:41:00	Maria	Escritor
jevillena22@hotmail.com	JC	Gonzalez Villena	2017-03-22 17:18:00	Maria	Escritor
noeliamarquezcalamaro@yahoo.es	Noelia	Márquez Fernández	2017-03-23 09:59:00	Maria	Escritor
ireenee.rv@gmail.com	Irene	.. 	2017-03-28 13:57:00	Maria	Escritor
iresega@gmail.com	Irene	.. 	2017-03-29 12:42:00	Maria	Escritor
manpaba@hotmail.com	Mansur	Paez Barroso	2017-03-29 18:58:00	Maria	Escritor
pablo@fi-films.com	Pablo	De La Rocha 	2017-04-03 17:59:00	Maria	Escritor
mayrebastardobatchvaroff@gmail.com	Mayré	Bastardo Batchvaroff	2017-04-04 18:18:00	Maria	Escritor
cajita.musicalis@gmail.com	María Sol	Savoretti 	2017-04-04 18:27:00	Maria	Escritor
mesoneslaura@gmail.com	Laura	Mesones 	2017-04-05 14:46:00	Maria	Escritor
irene_vp3@hotmail.com	Irene	Vazquez Perez	2017-04-06 11:26:00	Maria	Escritor
vero@comando-g.com	Vero	Gorri 	2017-04-06 18:35:00	Maria	Escritor
juliafern64@hotmail.com	Julia	Fernández Esteve	2017-04-07 11:51:00	Maria	Escritor
taniarodriguez-arias@hotmail.com	Tania	Rodríguez-Arias 	2017-04-10 17:45:00	Maria	Escritor
glanzuela@hotmail.com	Gerardo	Lanzuela 	2017-04-10 18:56:00	Maria	Escritor
eloymorera@hotmail.es	Eloy	Morera 	2017-04-11 09:22:00	Maria	Escritor
esther_shue@icloud.com	Esther	González Cañeque	2017-04-12 11:57:00	Maria	Escritor
jruperez@outlook.com	Jara	Rupérez 	2017-04-19 14:36:00	Maria	Escritor
mariaiglesiasreal@gmail.com	María	Iglesias 	2017-04-24 16:24:00	Maria	Escritor
miamigafantasma@gmail.com	Rocio	Rakel 	2017-04-24 17:58:00	Maria	Escritor
carmensomavilla@gmail.com	Carmen	Somavilla 	2017-04-25 13:04:00	Maria	Escritor
profandreavluna@gmail.com	Andrea V	Luna 	2017-04-25 13:42:00	Maria	Escritor
andromacalennox@gmail.com	Andrómaca	Lennox 	2017-04-26 09:30:00	Maria	Escritor
alexiaviola@hotmail.com	Alejandra	MARTIN HERNANDEZ	2017-04-26 16:12:00	Maria	Escritor
v.petkova@optimum-bg.com	Petkova	.. 	2017-04-27 18:15:00	Maria	Escritor
alvarovalderas2@gmail.com	Álvaro	Valderas 	2017-05-02 16:16:00	Maria	Escritor
mmir@totalfreightw.com	Manuel	Mir 	2017-05-03 12:47:00	Maria	Escritor
jdgogeta2013@gmail.com	José David	Gómez Urrego	2017-05-03 17:19:00	Maria	Escritor
hola@mariajosebarragan.com	Mariajosé	Barragán Salado	2017-05-03 17:27:00	Maria	Escritor
desiree__@hotmail.com	Desirée	Mora 	2017-05-03 17:44:00	Maria	Escritor
jgumaestro@gmail.com	Javier	Gutiérrez Maestro	2017-05-03 17:50:00	Maria	Escritor
martamartinezgavidia9@gmail.com	Marta	Martinez Gavidia	2017-05-08 16:50:00	Maria	Escritor
aliciatamon@hotmail.com	Alicia	Gonzalez 	2017-05-09 13:05:00	Maria	Escritor
valle1984@gmail.com	Valle	Pérez Pastor	2017-05-11 13:57:00	Maria	Escritor
eider.gonzalez@hotmail.com	Eider	.. 	2017-05-11 18:19:00	Maria	Escritor
gastonsciolli@gmail.com	Gaston	Sciolli 	2017-05-12 14:47:00	Maria	Escritor
fflorosoler@gmail.com	Francisco	Floro Soler	2017-05-12 14:53:00	Maria	Escritor
mocaponce@gmail.com	Monica	Ponce Martinez	2017-05-16 13:34:00	Maria	Escritor
chiara.boffi@gmail.com	Chiara	Boffi 	2017-05-22 13:29:00	Maria	Escritor
helianacrespo@hotmail.com	Heliana	Crespo 	2017-05-23 12:48:00	Maria	Escritor
carlosglz@mundo-r.com	Carlos	González 	2017-05-23 13:57:00	Maria	Escritor
nalonso@uva.es	Mª Natividad	Alonso Elvira	2017-05-23 16:33:00	Maria	Escritor
fernandocacho86@gmail.com	Fernando	.. 	2017-05-24 14:09:00	Maria	Escritor
jlj_naja@yahoo.es	Juan Luis	Jiménez 	2017-05-24 18:11:00	Maria	Escritor
marianoguera97@hotmail.com	María	Noguera Velasco	2017-05-24 18:41:00	Maria	Escritor
emiyart@yahoo.com	Emilia	García 	2017-05-24 18:59:00	Maria	Escritor
palenciamarbella@hotmail.es	Miguel Ángel	Palencia Marbella	2017-05-26 12:00:00	Maria	Escritor
mpilar.saez@gmail.com	Pilar	Sáez Rodríguez	2017-05-26 12:28:00	Maria	Escritor
ceaysantos@hotmail.com	Maria Elena	Santos Martin	2017-05-26 12:53:00	Maria	Escritor
kdeandrade65@gmail.com	Kamilla	De Andrade 	2017-05-29 13:31:00	Maria	Escritor
mfslgr@hotmail.com	Manuel	Fdez 	2017-05-29 13:42:00	Maria	Escritor
sandratabasco@gmail.com	Sandra	Tabasco 	2017-05-29 13:48:00	Maria	Escritor
Mariacuellarpaz@gmail.com	Maria	Cuellar Paz	2017-05-29 14:06:00	Maria	Escritor
emilioa.salas@gmail.com	Emilio A	Salas Leiton	2017-06-09 14:25:00	Maria	Escritor
virvacon@gmail.com	Virginia	Valero 	2017-06-12 09:41:00	Maria	Escritor
feliza.marro@gmail.com	Feliza	Marro 	2017-06-12 11:17:00	Maria	Escritor
juanfrancisconc@hotmail.com	Juan Francisco	.. 	2017-06-12 12:55:00	Maria	Escritor
saradieg@gmail.com	Sara	Diego 	2017-06-12 18:23:00	Maria	Escritor
rocio@iescabodelahuerta.com	Rocio	.. 	2017-06-16 10:57:00	Maria	Escritor
rossafb@hotmail.com	Rosa Maria	Fernandez Bermejo	2017-06-16 13:45:00	Maria	Escritor
swamipereira@hotmail.com	Swami	Pereira 	2017-06-16 14:03:00	Maria	Escritor
marcfreixasbanon@gemail.com	Marc	Freixas 	2017-06-21 12:39:00	Maria	Escritor
zst@um.es	Zaida	Sánchez Terrer	2017-06-22 14:38:00	Maria	Escritor
mercedesg.bueno@gmail.com	Mercedes	Gutierrez Bueno	2017-06-26 12:19:00	Maria	Escritor
clamva@yahoo.com.ar	Claudia	Valenti 	2017-06-27 17:05:00	Maria	Escritor
albert_ventura@yahoo.es	Albert	Ventura 	2017-06-27 17:10:00	Maria	Escritor
vanillabase@gmail.com	Enrique	Corrales Rubio	2017-06-28 17:23:00	Maria	Escritor
m_l_c_19@hotmail.com	Maria	Largos Canales	2017-06-29 13:10:00	Maria	Escritor
afrofeminas@gmail.com	Antoinette	Torres Soler	2017-07-03 10:28:00	Maria	Escritor
gguuicho@gmail.com	Güicho	.. 	2017-07-03 17:28:00	Maria	Escritor
gatamaja@gmail.com	Mariamar	Navarro 	2017-07-03 18:42:00	Maria	Escritor
Desigonzalezfernan.254@gmail.com	Desirée	.. 	2017-07-04 09:44:00	Maria	Escritor
0013creacions@gmail.com	Guillem	Gonzalo Vila	2017-07-04 17:01:00	Maria	Escritor
gpereyra.saez@gmail.com	Gonzalo	Pereyra Saez	2017-07-06 17:46:00	Maria	Escritor
vale_200384@hotmail.com	Valeria	Nuñez 	2017-07-07 14:32:00	Maria	Escritor
anitahjar@hotmail.com	Ana	Batres Torres	2017-07-11 10:03:00	Maria	Escritor
jecarruiz@gmail.com	Jesús	Cárdenas Ruiz	2017-07-11 10:49:00	Maria	Escritor
zeiram@hotmail.com	Carlos	Fernández 	2017-07-11 18:49:00	Maria	Escritor
ceferronpa@hotmail.com	Celia	Ferrón Paramio	2017-07-12 16:52:00	Maria	Escritor
diego_sandro@yahoo.com.ar	Diego	Sandro 	2017-07-18 11:01:00	Maria	Escritor
adrinoemolina@hotmail.com	Adriana	Molina Sarach	2017-07-24 18:07:00	Maria	Escritor
sergio@sbimbo.es	Sergio	Bimbo 	2017-07-26 17:20:00	Maria	Escritor
ainhoabelda198@hotmail.com	AINHOA	BELDA BERRAONDO	2017-07-27 09:22:00	Maria	Escritor
m.solcg@hotmail.com	Marisol	.. 	2017-07-27 10:10:00	Maria	Escritor
hadalimonada@gmail.com	Asun	Martinez Torres	2017-08-01 10:18:00	Maria	Escritor
yolandamartinezsantiago@gmail.com	Yolanda	Martínez Santiago	2017-08-01 18:10:00	Maria	Escritor
nfo@lunasanjuan.com	Luna	San Juan 	2017-08-01 18:13:00	Maria	Escritor
gburunat@gmail.com	Gisela	Burunat 	2017-08-07 11:33:00	Maria	Escritor
beacuentacuentos@gmail.com	Beatriz	Baquero Sillas	2017-08-07 17:27:00	Maria	Escritor
ricoirapuato@hotmail.com	MARTHA EUGENIA	HERRERA SILLER	2017-08-16 10:56:00	Maria	Escritor
manuelroblejo82@gmail.com	Manuel	Roblejo Proenza	2017-08-23 16:16:00	Maria	Escritor
inigolombardia@hotmail.com	Íñigo	L 	2017-08-25 09:21:00	Maria	Escritor
hola@bookishstudios.com	Ana	Sainz 	2017-08-25 14:18:00	Maria	Escritor
i.mazarod@gmail.com	Irene	Maza 	2017-09-12 12:47:00	Maria	Escritor
eerised819@hotmail.com	Desiree 	Regalado 	2017-09-12 13:17:00	Maria	Escritor
enmabovary@gmail.com	Virginia 	Melendo 	2017-09-12 13:54:00	Maria	Escritor
arquitectoverde@gmail.com	José Mario 	Calero  Vizcaíno	2017-09-14 19:49:00	Maria	Escritor
rociogalanrios@gmail.com	Rocío 	Galán  Ríos	2017-09-14 19:51:00	Maria	Escritor
naititi@msn.com	NATI PÉREZ 	CASELLES 	2017-09-15 15:01:00	Maria	Escritor
marc.best.7@gmail.com	Marc Gabriel Rafael	Jara  Condori	2017-09-18 09:44:00	Maria	Escritor
javi.santos@hotmail.es	Javier 	Santos  Blanco	2017-09-18 14:15:00	Maria	Escritor
romosif@gmail.com	Francisco 	Romo  Simón	2017-09-18 18:06:00	Maria	Escritor
maldonadoantonio@hotmail.com	Antonio 	Maldonado 	2017-09-22 13:24:00	Maria	Escritor
jacynt2002@yahoo.es	Jacinto 	Vidal  	2017-09-26 16:13:00	Maria	Escritor
laclasedevirginia@gmail.com	Virginia 	.. 	2017-09-28 09:52:00	Maria	Escritor
aonaa31@otmail.com	Gabriela 	.. 	2017-09-29 13:11:00	Maria	Escritor
maralcast@gmail.com	Mar 	Alvarez Castañé	2017-09-29 14:02:00	Maria	Escritor
vituilustradora@gmail.com	Maria Victoria 	Caruso 	2017-10-03 13:46:00	Maria	Escritor
jguillaumin@outlook.es	Jaime 	Romeo  Alary	2017-10-04 17:21:00	Maria	Escritor
kravenknight@hotmail.com	David 	Moreno  	2017-10-11 17:12:00	Maria	Escritor
mcuetas@gmail.com	Maria 	Cuetos 	2017-10-16 17:07:00	Maria	Escritor
kongema@gmail.com	Gema	.. 	2017-10-16 17:28:00	Maria	Escritor
josemanuelblancoserrano@gmail.com	José Manuel 	Blanco  Serrano	2017-10-16 17:31:00	Maria	Escritor
metge.juliette@gmail.com	Juliette	Arthaud 	2017-10-23 16:17:00	Maria	Escritor
m4rga.fb@gmail.com	Margarita	.. 	2017-10-25 18:35:00	Maria	Escritor
picodelospalotes@gmail.com	Margaritti	Aretussa  	2017-10-30 18:14:00	Maria	Escritor
mcappelli@gmail.com	Juan	Martín  Cappelli	2017-10-31 12:41:00	Maria	Escritor
mbruttirp@gmail.com	Maximiliano 	Brutti 	2017-10-31 14:20:00	Maria	Escritor
luplame@yahoo.es	Lucía 	Platas 	2017-11-03 14:20:00	Maria	Escritor
anaaguilera.periodista@gmail.com	Ana	.. 	2017-11-06 11:42:00	Maria	Escritor
rosadr38@gmail.com	Rosa 	Durán 	2017-11-07 19:20:00	Maria	Escritor
pila_tierra@yahoo.es	Pilar	Aranda 	2017-11-09 13:13:00	Maria	Escritor
ljastudilloc@gmail.com	Lidia	Astudillo Corona	2017-11-09 19:04:00	Maria	Escritor
nosoymegryan@gmail.com	María Jesús 	.. 	2017-11-15 11:49:00	Maria	Escritor
anabenitezgar@gmail.com	Ana	Benitez 	2017-11-16 19:23:00	Maria	Escritor
ezequielcisneromail@yahoo.es	Ezequiel 	Cisnero  	2017-11-20 19:10:00	Maria	Escritor
raquelparrondo@lasallevp.es	Raquel 	Parrondo 	2017-11-23 10:44:00	Maria	Escritor
oherrerog@gmail.com	Oscar 	Herrero  Galán	2017-12-04 19:05:00	Maria	Escritor
mari29es@hotmail.com	Carmen 	Trujillo  Romero	2017-12-05 19:12:00	Maria	Escritor
info@riccardoemargherita.com	Riccardo 	Francaviglia 	2017-12-11 14:10:00	Maria	Escritor
pacoperez21@hotmail.com	Paco	Perez  	2017-12-13 11:39:00	Maria	Escritor
lolapeinture27@gmail.com	Marie 	Bouchelloug  	2017-12-18 19:35:00	Maria	Escritor
auralez.contact@gmail.com	Laura 	Suárez 	2017-12-18 19:42:00	Maria	Escritor
bneiran@gmail.com	Bernardita 	Neira 	2017-12-21 19:55:00	Maria	Escritor
marthalorto@yahoo.com	Marta	.. 	2017-12-22 15:28:00	Maria	Escritor
debbieyafe@hotmail.com	Debbie 	Yafe  	2017-12-27 14:18:00	Maria	Escritor
elialavi89@hotmail.com	Elia Lavinia 	Hernández  Santos	2017-12-29 15:26:00	Maria	Escritor
ferldiazdreams@gmail.com	Jennifer 	Díaz 	2017-12-29 15:40:00	Maria	Escritor
meditaccion@meditaccion.es	Diana	P 	2018-01-05 14:46:00	Maria	Escritor
mlopezmvl1967@gmail.com	Mónica	López 	2018-01-08 15:32:00	Maria	Escritor
borgesaguilar@gmail.com	AÍDA MARÍA 	BORGES  AGUILAR	2018-01-08 18:38:00	Maria	Escritor
yasmani.osoria@nauta.cu	Yasmani	Osoria 	2018-01-10 19:51:00	Maria	Escritor
albada1956@gmail.com	Isabel 	Jimenez 	2018-01-15 19:18:00	Maria	Escritor
evapalomargomez@gmail.com	Eva 	Palomar  Gómez	2018-01-15 19:21:00	Maria	Escritor
mariosat@telefonica.net	Mario 	Satz  	2018-01-17 19:45:00	Maria	Escritor
nuriadelolmol@gmail.com	Nuria 	Del Olmo  López	2018-01-18 13:55:00	Maria	Escritor
Layospeinado@hotmail.com	MANUEL	.. 	2018-01-22 15:11:00	Maria	Escritor
javierhgarrido@gmail.com	Javier	Garrido 	2018-01-24 18:17:00	Maria	Escritor
ariel--lopez@hotmail.com	Ariel	López 	2018-02-05 19:42:00	Maria	Escritor
xanlopezdominguez@gmail.com	Xan 	López  Domínguez	2018-02-06 14:10:00	Maria	Escritor
info@astridsaalmann.es	Astrid 	Saalmann 	2018-02-07 12:07:00	Maria	Escritor
aenaefilius@gmail.com	Isabel 	Bustamante 	2018-02-14 14:22:00	Maria	Escritor
oscarescalera@hotmail.com	Oscar	Escalera 	2018-02-19 18:49:00	Maria	Escritor
mireiapratsv@gmail.com	Mireia	Prats Vilamalla	2016-01-25 17:39:00	marta	Escritor
mdetelleria@gmail.com	María José	Detelleria 	2016-02-01 19:44:00	marta	Escritor
furifuridoris@hotmail.es	Julia	Picossi 	2016-02-10 18:21:00	marta	Escritor
anne.m.braune@gmail.com	Anne	Braune 	2016-02-10 19:29:00	marta	Escritor
diegogena@hotmail.com	Diego	Genatiempo 	2016-02-22 12:07:00	marta	Escritor
takeuchi.chihiro@nifty.com	Chihiro	Takeuchi 	2016-02-22 17:55:00	marta	Escritor
manica@manicamusil.com	Manica	Musil 	2016-02-23 19:32:00	marta	Escritor
martinapeluso@gmail.com	Martina	Peluso 	2016-03-01 12:31:00	marta	Escritor
cristina_trapanese@yahoo.it	Cristina	Trapanese 	2016-03-01 13:17:00	marta	Escritor
cristina.martin.artajo@gmail.com	Cristina	Martin-artajo Dicente De Vera	2016-03-01 17:22:00	Maria	Escritor
cyberevabcn@hotmail.com	Eva	Jimenez Garcia	2016-03-01 18:04:00	Maria	Escritor
shivy366@gmail.com	Silvia	Llanto 	2016-03-02 10:45:00	Maria	Escritor
lasalasandra@hotmail.com	Sandra	Carballar Diaz	2016-03-04 10:37:00	Maria	Escritor
martadetorre@hotmail.com	Marta	De Torre 	2016-03-15 11:48:00	Maria	Escritor
dannyserra2587@hotmail.com	Danny	Serra 	2016-03-16 12:16:00	Maria	Escritor
carolina.corvillo@gmail.com	Carolina	Corvillo 	2016-03-16 12:22:00	Maria	Escritor
frodriguez4600@gmail.com	Facundo Luis	Rodriguez 	2016-03-16 12:28:00	Maria	Escritor
dileo.mary@libero.it	Di Leo	Maria 	2016-03-16 12:57:00	Maria	Escritor
lunasalinassevcik@gmail.com	Luna	Salinas Sevcik	2016-03-18 09:49:00	Maria	Escritor
blandelatorref@yahoo.es	MÂª Blanca	De La Torre 	2016-03-18 09:50:00	Maria	Escritor
cuentoslibres@live.com	Miguel Ángel	Roco 	2016-03-21 10:47:00	Maria	Escritor
enriquecarlosmartin@gmail.com	Enrique Carlos	Martín 	2016-03-22 12:26:00	Maria	Escritor
bogota8487@hotmail.com	Francisca	Alcaraz Marín	2016-03-30 12:24:00	Maria	Escritor
yaelahavaangel@gmail.com	Yael	Ahava Angel	2016-03-30 12:27:00	Maria	Escritor
patricia.gayan@gmail.com	Patricia	Gayan 	2016-03-30 13:17:00	Maria	Escritor
tracybinnaz@gmail.com	Tracy	Binnaz 	2016-03-30 13:26:00	Maria	Escritor
lauraleonv@gmail.com	Laura León	León 	2016-03-30 13:28:00	Maria	Escritor
caballerolopezelizabeth@gmail.com	Elizabeth	Lopez Caballero	2016-03-31 12:49:00	Maria	Escritor
agnes@dennyefjorden.no	Agnes	Capella Sala	2016-04-04 16:52:00	marta	Escritor
abraham.hernandez2003@gmail.com	Abrahan Hernandez	Hernandez 	2016-04-04 18:33:00	Maria	Escritor
marisalopezsoria@gmail.com	Marisa	LOPEZ SORIA	2016-04-04 19:00:00	Maria	Escritor
pinerocecilia@yahoo.es	Carmen Cecilia	Piñero Gil	2016-04-05 09:29:00	Maria	Escritor
polomaltes@hotmail.com	MÂª Teresa	Polo Maltes	2016-04-05 10:39:00	Maria	Escritor
hernanguillermoparedes@gmail.com	Hernán Guillermo	Paredes 	2016-04-05 12:04:00	Maria	Escritor
nuriacantos@hotmail.com	Nuria	Cantos Encina	2016-04-05 13:05:00	Maria	Escritor
arcixformacion@arcix.net	Olga	Casanova 	2016-04-08 11:01:00	Maria	Escritor
lealcoach@gmail.com	Luis Enrique	Antiñolo Lucas	2016-04-13 10:58:00	Maria	Escritor
becolu@hotmail.com	Begoña	Aa 	2016-04-13 12:04:00	Maria	Escritor
her04xep@yahoo.es	Hector Francisco	Xep 	2016-04-13 12:08:00	Maria	Escritor
enidrebeca@gmail.com	Rebeca	Aragón Santiago	2016-04-18 11:00:00	Maria	Escritor
alixares@hotmail.com	Jose Pablo	Aa 	2016-04-19 13:54:00	Maria	Escritor
michelnamid@hotmail.com	Miguel Ángel	Jiménez 	2016-04-20 09:47:00	Maria	Escritor
aisoler@hotmail.com	Ana	Soler Cantalejo	2016-04-20 10:04:00	Maria	Escritor
annasoldevilab@gmail.com	Anna	Soldevila 	2016-04-20 13:09:00	Maria	Escritor
cucarancha@gmail.com	Arancha	Álvarez 	2016-04-25 11:18:00	Maria	Escritor
blancatartu@gmail.com	Blanca	Rubiales Medina	2016-04-26 11:11:00	Maria	Escritor
dariko2013@hotmail.com	W. Darío	Amaral 	2016-04-26 11:38:00	Maria	Escritor
rcorujo@uniss.edu.cu	Riselda María	Corujo Quesada	2016-04-26 16:44:00	Maria	Escritor
elepizarronogues@hotmail.com	Elena	Pizarro 	2016-04-28 10:26:00	Maria	Escritor
joselogopedia@gmail.com	José Fernando Taverner	Taverner 	2016-05-04 14:08:00	Maria	Escritor
baloola34@gmail.com	Amaya	Rodríguez 	2016-05-04 14:24:00	Maria	Escritor
trilogiakayla@gmail.com	María	Cuesta Martín	2016-05-05 13:03:00	Maria	Escritor
gloriaalmendariz@gmail.com	GLORIA	ALMENDÁRIZ PÉREZ	2016-05-05 14:27:00	Maria	Escritor
monica.iglesias74@gmail.com	Mónica	Iglesias 	2016-05-06 10:59:00	Maria	Escritor
lorena_lima_millares@hotmail.com	Lorena	Lima Millares	2016-05-10 13:18:00	Maria	Escritor
laiaalabau@gmail.com	Laia	Alabau Bagué	2016-05-10 13:35:00	Maria	Escritor
bellinibenassi2016@gmail.com	Bellini &Benassi	Aa 	2016-05-11 10:38:00	Maria	Escritor
montserrat@garbuix-agency.com	Montserrat	Terrones 	2016-05-11 11:14:00	Maria	Escritor
claravedillo@gmail.com	Clara	Saetiz 	2016-05-12 10:18:00	Maria	Escritor
edurne.asecas@hotmail.com	Edurne	Cacho 	2016-05-13 14:02:00	Maria	Escritor
rosaluz_92@hotmail.com	Rosana	GIL DE LA GUARDIA	2016-05-16 18:20:00	Maria	Escritor
terrybilmente.lapis@libero.it	Maria Teresa	Conte 	2016-05-17 14:26:00	Maria	Escritor
chincheta15@hotmail.com	Itziar	Aa 	2016-05-17 14:29:00	Maria	Escritor
lucholaxe@hotmail.com	Lucho	Aa 	2016-05-25 16:11:00	Maria	Escritor
sirenaaguamar@gmail.com	Ana	Libia Espinosa	2016-05-25 18:23:00	Maria	Escritor
lmmartinal@hotmail.com	Luisa Maria	Martin Alonso	2016-05-27 13:47:00	Maria	Escritor
dbrarua@hotmail.com	David	Bravo Ruano	2016-05-27 16:41:00	Maria	Escritor
catalinapark@hotmail.com	Carlota	Bermudez 	2016-05-27 18:00:00	Maria	Escritor
albertoluissierra@hotmail.com	Alberto	Sierra 	2016-05-30 11:53:00	Maria	Escritor
davidberlui@gmail.com	David	Berlui 	2016-05-30 13:48:00	Maria	Escritor
danober09@gmail.com	Graciela	Bedayes Alvarez	2016-05-31 09:59:00	Maria	Escritor
SYLVIA.VIVANCO@GMAIL.COM	Sylvia	Vivanco 	2016-06-01 12:42:00	Maria	Escritor
bellesartsvic@gmail.com	Montse	Fletas 	2016-06-02 17:50:00	Maria	Escritor
manuelarduinopavn@yahoo.com.ar	Manuel	Arduino Pavon	2016-06-03 12:35:00	Maria	Escritor
vicky131110@hotmail.com	Vicky	Gui 	2016-06-03 13:21:00	Maria	Escritor
fercerrudo@yahoo.com.ar	Fernando	Cerrudo 	2016-06-09 10:58:00	Maria	Escritor
pamela.carrington.m@gmail.com	Pamela	Carrington 	2016-06-14 12:42:00	Maria	Escritor
jmontesaggi@gmail.com	Jose Luis	Montes Aggi	2016-06-16 12:01:00	Maria	Escritor
luisa_temon@hotmail.com	Luisa	Marin 	2016-06-20 13:55:00	Maria	Escritor
carmenbxyon@gmail.com	Carmen	Bayon 	2016-06-21 16:37:00	Maria	Escritor
enriqueguion@gmail.com	Enrique	Dueñas 	2016-06-22 12:16:00	Maria	Escritor
pilarsanchezdiaz@gmail.com	Pilar	Sánchez Díaz	2016-06-22 13:09:00	Maria	Escritor
carmen.rendueles@gmail.com	Carmen	Rendueles 	2016-06-22 13:20:00	Maria	Escritor
giulia1991co@libero.it	Giulia	Costa 	2016-06-27 16:52:00	Maria	Escritor
leyreramoscastro@gmail.com	Leyre	Ramos Castro	2016-06-27 17:15:00	Maria	Escritor
soldebla@gmail.com	David	De Gil Gómez	2016-06-28 12:21:00	Maria	Escritor
jjmesbailer@hotmail.com	Juan José	Mesbailer Arco	2016-06-28 14:24:00	Maria	Escritor
saragodoy17@hotmail.com	Sara	Godoy Gil	2016-06-30 13:14:00	Maria	Escritor
williamv.3@hotmail.com	William	Velasco 	2016-06-30 14:01:00	Maria	Escritor
javierponsoda@gmail.com	Javier	Ponsoda Navarro	2016-07-04 11:33:00	Maria	Escritor
mar.workcontact@gmail.com	Mar	Amorena 	2016-07-06 10:20:00	Maria	Escritor
stephanieleidinger@gmail.com	Stephanie	Leidinger 	2016-07-06 11:48:00	Maria	Escritor
olgarodriguez62@hotmail.com	Olga Herminia	Rodríguez Brito	2016-07-06 14:40:00	Maria	Escritor
portalgaviota@yahoo.com.mx	Gabriela	Loayza 	2016-07-06 16:31:00	Maria	Escritor
silcor_19@hotmail.com	Silvia	Corbo 	2016-07-06 16:54:00	Maria	Escritor
eearwen@hotmail.com	Marta	Aa 	2016-07-08 13:39:00	Maria	Escritor
pizcolga2@hotmail.com	Olga	Pérez Barbero	2016-07-08 13:51:00	Maria	Escritor
trucoseosem@gmail.com	Javier	Gacimartin Gonzalez	2016-07-08 13:54:00	Maria	Escritor
pauvolk@gmail.com	Paula	Martínez 	2016-07-11 13:30:00	Maria	Escritor
vanesagp92@hotmail.com	Vanesa	G 	2016-07-12 19:03:00	Maria	Escritor
beatriz-jim-larrey@hotmail.com	Beatriz	Jiménez Larrey	2016-07-13 10:47:00	Maria	Escritor
arquitectura@zazurca.com	Andrea	Zazurca 	2016-07-13 13:34:00	Maria	Escritor
raquelru@gmail.com	Raquel	Ruiz 	2016-07-18 18:44:00	Maria	Escritor
laguilsays@gmail.com	Laura	Aguilera 	2016-07-19 13:41:00	Maria	Escritor
isabel.mfgt@gmail.com	Isabel	MF 	2016-07-21 09:55:00	Maria	Escritor
urkkelina@gmail.com	Begoña	Sastre 	2016-07-21 11:04:00	Maria	Escritor
belhicca@gmx.com	Belhicca	Aa 	2016-07-21 18:25:00	Maria	Escritor
rsotoned@gmail.com	Raquel	Soto Boned	2016-07-26 16:45:00	Maria	Escritor
paquimerinos@gmail.com	Francisca	Martinez Marinos	2016-07-26 18:13:00	Maria	Escritor
a.perezhdz@gmail.com	Antonio	Pérez Hernández	2016-07-27 14:05:00	Maria	Escritor
kiariscosentino@gmail.com	Kiaris	Cosentino 	2016-07-28 17:15:00	Maria	Escritor
albertcebrian@hotmail.com	Albert	Cebrían 	2016-08-01 13:42:00	Maria	Escritor
mirkofilippi66@gmail.com	MIRKO	FILIPPI 	2016-08-02 14:05:00	Maria	Escritor
raimundo.montava@gmail.com	Raimundo	Montava 	2016-08-04 11:58:00	Maria	Escritor
jziuxar@hotmail.com	JORGE	RODRIGUEZ ALVAREZ	2016-08-05 14:17:00	Maria	Escritor
md.marina@outlook.com	Marina	Martín Díaz	2016-08-08 18:50:00	Maria	Escritor
cris_ss88@hotmail.es	Cristina	Seijas Sanfiz	2016-08-09 12:56:00	Maria	Escritor
sandra_ss22@hotmail.com	Sandra	Sánchez Sillero	2016-08-09 17:02:00	Maria	Escritor
andreareyesru@gmail.com	Andrea	Reyes 	2016-08-10 12:46:00	Maria	Escritor
ada_6015@hotmail.com	Inmaculada	Linares Sillero	2016-08-16 17:41:00	Maria	Escritor
arantxagalb@hotmail.com	Arancha	García 	2016-08-16 18:25:00	Maria	Escritor
elenamoralesgar@gmail.com	Elena	Morales García	2016-08-16 18:30:00	Maria	Escritor
ammmesas2016@gmail.com	Maria Del Mar	. 	2016-08-17 13:47:00	Maria	Escritor
lolyn.1010@hotmail.com	Lola	Nóvoa 	2016-08-17 18:01:00	Maria	Escritor
izaskunaurrekoetxea30@gmail.com	Izaskun	Aurrekoetxea 	2016-08-22 13:50:00	Maria	Escritor
zahira_job@hotmail.com	ZAHIRA	ECHEGARAY 	2016-08-22 13:59:00	Maria	Escritor
erick.cuevas@gmail.com	Erick	Cuevas Gutierrez	2016-08-23 10:08:00	Maria	Escritor
ignaciolongobardo@gmail.com	Ignacio	Longobardo 	2016-08-23 10:16:00	Maria	Escritor
pelguetam@gmail.com	Patricio	Elgueta 	2016-08-23 12:41:00	Maria	Escritor
srivera90@hotmail.com	Silvana	Rivera 	2016-08-24 17:38:00	Maria	Escritor
jmsilvapuerta@hotmail.com	José Manuel	Silva Puerta	2016-08-24 18:48:00	Maria	Escritor
raulcis11@hotmail.com	RAÚL	CISNEROS LÓPEZ	2016-08-26 17:41:00	Maria	Escritor
paulacortes1963@gmail.com	Paula	Cortés Granado	2016-08-30 14:17:00	Maria	Escritor
dmfebrero@hotmail.com	Doly	Morales 	2016-08-30 14:27:00	Maria	Escritor
anagutrubio@gmail.com	Ana	Gutiérrz 	2016-08-30 16:33:00	Maria	Escritor
rosanacuervo@hotmail.com	Rosana	Cuervo Alvarez	2016-08-31 12:35:00	Maria	Escritor
csilva.velasco@gmail.com	Felisa	Martinez Rojas	2016-08-31 12:38:00	Maria	Escritor
fatimabachiller.novaforma@gmail.com	Fátima	Bachiller Burgos	2016-09-01 12:17:00	Maria	Escritor
damarisagudoblasco@gmail.com	Dámaris	.. 	2016-09-01 14:15:00	Maria	Escritor
francisco.garcia.landa@gmail.com	Francisco Javier	García Landa	2016-09-01 14:46:00	Maria	Escritor
sandra.avegil@gmail.com	Sandra	Avecilla 	2016-09-01 17:49:00	Maria	Escritor
colmenares.vicky@gmail.com	Victoria	Colmenares 	2016-09-02 13:40:00	Maria	Escritor
izarrakmargotuz@gmail.com	Nuria	Vilalta 	2016-09-05 17:57:00	Maria	Escritor
didaccampano@gmail.com	Dídac	Campano 	2016-09-06 11:44:00	Maria	Escritor
patriciagarcia356@gmail.com	Patricia	Garcia 	2016-09-06 13:06:00	Maria	Escritor
duplicacionmecp2@gmail.com	Ana	Mourelo 	2016-09-06 13:27:00	Maria	Escritor
auradevoradorademundos@hotmail.com	Ariadna	.. 	2016-09-06 16:14:00	Maria	Escritor
isabelsoriairisarri@gmail.com	ISABEL	SORIA 	2016-09-07 11:40:00	Maria	Escritor
isabelalamar@ono.com	Isabel	Alamar 	2016-09-07 12:23:00	Maria	Escritor
tebanisaza@hotmail.com	Esteban	Isaza Montoya	2016-09-07 12:48:00	Maria	Escritor
a.garciamolina@yahoo.es	Antonio	Garcia Molina	2016-09-08 11:03:00	Maria	Escritor
sheivigo@hotmail.com	Seila	Viqueira Poveda	2016-09-08 11:07:00	Maria	Escritor
numaranna@gmail.com	Anna	Agulló Prieto	2016-09-08 14:18:00	Maria	Escritor
aurorarosillo00@gmail.com	Aurora	Medina 	2016-09-09 13:00:00	Maria	Escritor
dolormarieg@gmail.com	Maria Dolores	.. 	2016-09-09 16:49:00	Maria	Escritor
ana@anazurita.com	Ana	Zurita 	2016-09-09 17:36:00	Maria	Escritor
conmilibretadecorazones@gmail.com	Nona	Mercel 	2016-09-09 18:44:00	Maria	Escritor
jmbeghor@hotmail.com	JOSÉ MANUEL	BEGINES HORMIGO	2016-09-12 12:54:00	Maria	Escritor
eraseunavezunarbol@gmail.com	Maria Y Paula	VG 	2016-09-12 14:01:00	Maria	Escritor
summerby33@hotmail.com	Virginia	Ortega Rodriguez	2016-09-12 14:29:00	Maria	Escritor
albinanap@gmail.com	Pol	Albiñana 	2016-09-12 14:31:00	Maria	Escritor
ariacuale@gmail.com	Lorena	Viar Nevado	2016-09-14 10:45:00	Maria	Escritor
paginaswebmataro@gmail.com	Luis	Barquero 	2016-09-14 12:52:00	Maria	Escritor
evpatoria7@yahoo.es	Vika	.. 	2016-09-14 13:53:00	Maria	Escritor
pgranaval@hotmail.com	Patricia	Granados Ávalos	2016-09-14 14:28:00	Maria	Escritor
alicianunez10492@gmail.com	Alicia	Nuñez Isaac	2016-09-14 16:54:00	Maria	Escritor
mjcabral2046@gmail.com	María José	Cabral 	2016-09-15 11:02:00	Maria	Escritor
emmayuste@gmail.com	Emma	Yuste 	2016-09-15 14:33:00	Maria	Escritor
sie2004@gmail.com	Iván	Sánchez Roig	2016-09-15 15:54:00	Maria	Escritor
gcamara@fundacionsafa.es	Gloria	Cámara 	2016-09-20 11:11:00	Maria	Escritor
pilarserranoescritora@gmail.com	Pilar	Serrano 	2016-09-20 12:18:00	Maria	Escritor
anabe.manzano@gmail.com	Anabé	Manzano 	2016-09-20 14:32:00	Maria	Escritor
spdelpino@gmail.com	Santiago	Pérez 	2016-09-20 18:28:00	Maria	Escritor
matildetoimil@hotmail.com	Vanessa	Toimil 	2016-09-20 18:54:00	Maria	Escritor
mpfgfa@gmail.com	Marta	Gamboa 	2016-09-21 10:09:00	Maria	Escritor
walkbrave@gmail.com	Raquel	Luque 	2016-09-21 18:55:00	Maria	Escritor
alpispa@gmail.com	Dolores	Espinosa Márquez	2016-09-22 14:08:00	Maria	Escritor
lexea71@hotmail.com	Alexander	Fábrega Cogley	2016-09-22 18:10:00	Maria	Escritor
annelazpita@gmail.com	Anne	Lazpita Bárcena	2016-09-22 18:59:00	Maria	Escritor
apartillustration@gmail.com	Alvaro	Perez Mendez	2016-09-23 12:22:00	Maria	Escritor
zaidamarinillustrator@gmail.com	Zaida	Peña Marín	2016-09-23 13:10:00	Maria	Escritor
nuribolanos@gmail.com	Nuria	Bolaños 	2016-09-23 13:34:00	Maria	Escritor
xavier.frias.conde@gmail.com	Xavier	Frias Conde	2016-09-23 14:27:00	Maria	Escritor
Ialvarezfernandez6@gmail.com	Juan Antonio	.. 	2016-09-23 14:32:00	Maria	Escritor
vgomez227@gmail.com	Vicky	Gómez 	2016-09-23 17:51:00	Maria	Escritor
mrosamcasas@gmail.com	Rosa	Muñoz Casas	2016-09-26 11:05:00	Maria	Escritor
capomiquel@hotmail.com	Miquel	Capó Dolz	2016-09-26 14:21:00	Maria	Escritor
molina0783@gmail.com	Tabatha	Molina Valbuena	2016-09-26 14:26:00	Maria	Escritor
trocitochocolate@gmail.com	Mónica	Rodríguez Estévez	2016-09-26 16:48:00	Maria	Escritor
pedro.cano.navarro@hotmail.es	Pedro	Cano 	2016-09-26 18:39:00	Maria	Escritor
trini_noa@yahoo.es	Trinidad	Sanchez Rodriguez	2016-09-27 18:21:00	Maria	Escritor
juiagema63@hotmail.com	Julia Gema	Fernández Pla	2016-09-28 14:27:00	Maria	Escritor
lucialarcon97@gmail.com	Lucía	Alarcón Reyes	2016-09-28 14:30:00	Maria	Escritor
sarita_dlfd@hotmail.es	Sara	De La Fuente Dominguez	2016-09-28 18:00:00	Maria	Escritor
saradelasnieves@gmail.com	Sara	De Las Nieves 	2016-09-29 11:04:00	Maria	Escritor
redaccionesalejandra@hotmail.com	Sandra	Martin 	2016-09-30 13:53:00	Maria	Escritor
zzalmaguzman@gmail.com	ZALMA	GUZMÁN 	2016-09-30 17:38:00	Maria	Escritor
elclubdelaslectorasypoetas@gmail.com	Paula	Cabrera Schwartz	2016-09-30 17:41:00	Maria	Escritor
Aggmadrid@hotmail.com	Alberto	Gayoso García	2016-10-07 17:44:00	Maria	Escritor
olga.lukinskaya@gmail.com	Olga	Lukinskaya 	2016-10-10 13:17:00	Maria	Escritor
novelarcoiris@gmail.com	Franco  Y Emilia	Silveira Y García 	2016-10-10 18:17:00	Maria	Escritor
gaby.razazi@hotmail.com	Gabriela	Razazi 	2016-10-13 18:20:00	Maria	Escritor
jsintes1@telefonica.net	JOSE	SINTES ARNAIZ	2016-10-14 10:21:00	Maria	Escritor
graci.moreno.peces@gmail.com	Gracia	Moreno 	2016-10-14 11:45:00	Maria	Escritor
maryjrp80@gmail.com	Trentaonze	Mary 	2016-10-17 17:47:00	Maria	Escritor
pau.guldris@gmail.com	Pau	.. 	2016-10-18 18:40:00	Maria	Escritor
mgonzvaz74@gmail.com	Maria	Gonzalez Vazquez	2016-10-18 18:45:00	Maria	Escritor
miriam.haas@gmx.de	Miriam 	Miriam Haas 	2016-10-19 10:08:00	Maria	Escritor
rebepati@hotmail.com	Rebeca	Arias Tobar	2016-10-19 12:52:00	Maria	Escritor
zapatosdesirena@gmail.com	Elena	Blanco 	2016-10-20 16:47:00	Maria	Escritor
lperezdelar@hotmail.com	Laura	Pérez De Larraya	2016-10-21 18:41:00	Maria	Escritor
magdarayuso@gmail.com	Magdalena	Rodriguez 	2016-10-25 13:29:00	Maria	Escritor
marivihc@gmail.com	Mariví	De La Hoz Calderón	2016-10-25 16:24:00	Maria	Escritor
raulca8@hotmail.com	Raul	Jiménez Sastre	2016-10-25 16:27:00	Maria	Escritor
noelia.rguez.glez@gmail.com	Noelia	Rodríguez González	2016-10-25 18:26:00	Maria	Escritor
pequeari82@hotmail.com	Ariana	Pellitero 	2016-10-26 18:10:00	Maria	Escritor
xoanbabarro@hotmail.com	Xoán	Babarro 	2016-10-27 12:07:00	Maria	Escritor
maria_indie@hotmail.com	María	Lara Morales	2016-10-28 13:28:00	Maria	Escritor
deseada.acevedo@gmail.com	Desirée	Acevedo 	2016-10-28 17:58:00	Maria	Escritor
sandraura@hotmail.com	Sandra	Urrutikoetxea 	2016-10-28 18:23:00	Maria	Escritor
deledda75@hotmail.com	Rosa Maria	Deledda Gutierrez	2016-10-31 12:33:00	Maria	Escritor
tamaraaguilar11@hotmail.com	Tamara	.. 	2016-10-31 18:59:00	Maria	Escritor
irene.meurs@gmail.com	Irene	.. 	2016-11-03 18:38:00	Maria	Escritor
Yolandagg63@hotmail.com	Yolanda	.. 	2016-11-04 12:04:00	Maria	Escritor
l.gerpegarcia@gmail.com	Luisa	Gerpe García	2016-11-04 17:55:00	Maria	Escritor
pablitom88@hotmail.com	Pablo	Martín 	2016-11-04 18:28:00	Maria	Escritor
catigdr@hotmail.com	Cati	Garcia Del Rio	2016-11-04 18:31:00	Maria	Escritor
natividad@bailenasesores.com	Natividad	Fernández Mellinas	2016-11-04 18:40:00	Maria	Escritor
cristina.ayuso.guti@gmail.com	Cristina	Ayuso Gutiérrez	2016-11-04 18:51:00	Maria	Escritor
cristinapadin@gmail.com	Cristina	.. 	2016-11-04 18:53:00	Maria	Escritor
orpau39@gmail.com	OLGA	OLARIA SENDRA	2016-11-07 18:41:00	Maria	Escritor
carolglez@wanadoo.es	Carolina	González De Figueras	2016-11-07 18:46:00	Maria	Escritor
federico-correa@hotmail.com	Federico	Correa 	2016-11-07 18:53:00	Maria	Escritor
gloriaaliacar@gmail.com	Gloria	Aliacar 	2016-11-07 18:59:00	Maria	Escritor
cristinamateolopez84@gmail.com	Cristina	Mateo López	2016-11-08 10:44:00	Maria	Escritor
jettconnie@gmail.com	Connie	Jett 	2016-11-08 11:35:00	Maria	Escritor
ntejper@gmail.com	NEREA	.. 	2016-11-08 11:42:00	Maria	Escritor
lucasescary@hotmail.com	Lucas	Ezequiel 	2016-11-08 12:09:00	Maria	Escritor
tamaraestrellamurillo@gmail.com	Tamara	Estrella Murillo	2016-11-08 13:45:00	Maria	Escritor
jfilipone@yahoo.com	Julia	Filipone Erez	2016-11-08 14:40:00	Maria	Escritor
ricardocel@hotmail.com	Ric	Ibáñez 	2016-11-08 16:13:00	Maria	Escritor
martagonzalezdelcastillo@wanadoo.es	Marta	González 	2016-11-08 16:46:00	Maria	Escritor
ecqsalp@gmail.com	Alejandro	.. 	2016-11-08 17:37:00	Maria	Escritor
carinalimardo@gmail.com	Carina	.. 	2016-11-09 16:32:00	Maria	Escritor
info@imagosg.com	Ángel	Cañibano 	2016-11-10 14:31:00	Maria	Escritor
avelinlin@gmail.com	Ávelin	.. 	2016-11-10 16:46:00	Maria	Escritor
juliadiezlij@gmail.com	JÚLIA	DÍEZ VASCO	2016-11-10 17:30:00	Maria	Escritor
salascacabelos@gmail.com	Rocio	Rodríguez González	2016-11-10 18:15:00	Maria	Escritor
nasarrisa@hotmail.com	Isabel	Pena 	2016-11-10 18:31:00	Maria	Escritor
mtitos.gonzalez@gmail.com	Macarena	Titos González	2016-11-14 12:29:00	Maria	Escritor
lasrtapottery@gmail.com	Paula	Molina 	2016-11-14 12:38:00	Maria	Escritor
revistacincoarcos@hotmail.com	Teresa	Moncayo Lopez	2016-11-14 12:45:00	Maria	Escritor
sol.ilustra@gmail.com	Sol	Ilustra 	2016-11-14 13:04:00	Maria	Escritor
e.kahane@aiic.net	Eduardo	Kahane 	2016-11-14 13:16:00	Maria	Escritor
mcsorial@gmail.com	Concha  Y Marisol	.. 	2016-11-14 14:08:00	Maria	Escritor
cruz@danicruz.com	Dani	Cruz 	2016-11-14 18:32:00	Maria	Escritor
hola@ehtoons.com	Diego	Baigorri 	2016-11-15 13:04:00	Maria	Escritor
laurarobado@gmail.com	Laura	Robado 	2016-11-15 13:08:00	Maria	Escritor
fini_arenal@hotmail.es	Fini	Rodriguez 	2016-11-15 13:42:00	Maria	Escritor
swe@hotmail.es	Sergio	Raga 	2016-11-15 14:31:00	Maria	Escritor
soniafan2@gmail.com	Sonia	Fandos Sanz	2016-11-17 17:12:00	Maria	Escritor
vicky_20486@hotmail.com	Vicky	Lopez 	2016-11-18 10:21:00	Maria	Escritor
luiscr744@ymail.com	Luis	Campoy Ramos	2016-11-18 10:37:00	Maria	Escritor
constantinomenendez@gmail.com	Constantino	Menendez 	2016-11-18 11:00:00	Maria	Escritor
adriana.oliveros.a@gmail.com	Adriana	Oliveros 	2016-11-18 11:03:00	Maria	Escritor
rmjulia94@gmail.com	Julia	Rodríguez Morales	2016-11-18 12:03:00	Maria	Escritor
aifos30@gmail.com	Sofia	Amores Luzon	2016-11-21 11:18:00	Maria	Escritor
luzestrellasb@yahoo.es	Luz	Estrella Sánchez	2016-11-21 11:21:00	Maria	Escritor
ocne@ocaso.es	Graciela	Carro Asorey	2016-11-21 11:55:00	Maria	Escritor
mcastilloserrano@loveandchild.com	María	Castillo 	2016-11-21 11:58:00	Maria	Escritor
landa@ivanlanda.com	Ivan	Landa 	2016-11-21 12:22:00	Maria	Escritor
es.anagil@gmail.com	Ana Isabel	Gil 	2016-11-21 14:18:00	Maria	Escritor
luciarlorenzo@gmail.com	Lucia	.. 	2016-11-21 14:25:00	Maria	Escritor
nitziapty@gmail.com	Nitzia	.. 	2016-11-21 14:27:00	Maria	Escritor
esterverd@hotmail.com	ESTER	VERDUCH ZANON	2016-11-21 18:29:00	Maria	Escritor
numancya@hotmail.com	MARÍA	SANZ 	2016-11-21 18:41:00	Maria	Escritor
tinchoc00@gmail.com	Martin	Lucas Cejas	2016-11-22 12:12:00	Maria	Escritor
teresaf417@gmail.com	Teresa	.. 	2016-11-22 12:54:00	Maria	Escritor
dori.espejo@gmail.com	Dori	Espejo 	2016-11-22 13:10:00	Maria	Escritor
prue_embrujada@hotmail.com	Miriam	.. 	2016-11-23 18:04:00	Maria	Escritor
ramonmarinbustos@hotmail.com	Ramons	Marin Bustos	2016-11-24 11:56:00	Maria	Escritor
cappolavoro@hotmail.com	Tito	Del Muro 	2016-11-24 12:54:00	Maria	Escritor
guillermobarreto88@gmail.com	Guillermo	Barreto 	2016-11-24 17:50:00	Maria	Escritor
davidsureda@hotmail.es	David	Sureda Gabaldón	2016-11-25 09:59:00	Maria	Escritor
alma7477@gmail.com	Vanessa	Martínez 	2016-11-25 16:58:00	Maria	Escritor
yulianpopov@gmail.com	Julian	Popov 	2016-11-28 17:37:00	Maria	Escritor
jsuazo67@gmail.com	Javier Enrique	Suazo Mejía	2016-11-28 18:49:00	Maria	Escritor
yayorivasmorales@hotmail.com	Rosario	Rivas Morales	2016-11-30 18:05:00	Maria	Escritor
martaljnb@gmail.com	Marta	.. 	2016-11-30 19:04:00	Maria	Escritor
kerkis@hotmail.com	Angela	Serrano 	2016-12-01 11:39:00	Maria	Escritor
angierossi2010@gmail.com	Angie	Rossi 	2016-12-01 13:31:00	Maria	Escritor
merinocorleone@gmail.com	Jesus	Merino 	2016-12-01 13:51:00	Maria	Escritor
Evai1@hotmail.com	Eva	.. 	2016-12-01 17:45:00	Maria	Escritor
inmiscao@hotmail.com	Inmaculada	Cañete Olivenza	2016-12-02 17:12:00	Maria	Escritor
ilustracionesyudi@gmail.com	Yudi	Vargas 	2016-12-09 13:18:00	Maria	Escritor
duvi.abril@gmail.com	Duvi	Abril 	2016-12-09 16:50:00	Maria	Escritor
lau_vg_92@hotmail.com	Laura	Villar Gomez	2016-12-09 17:11:00	Maria	Escritor
kiaopo@hotmail.com	Mari Carmen	Gárate Mejías	2016-12-09 17:52:00	Maria	Escritor
beatrizroma.artistaplastica@gmail.com	Beatriz	Rodriguez Martin	2016-12-09 18:19:00	Maria	Escritor
karloff-85@hotmail.com	Karlos	García 	2016-12-09 19:04:00	Maria	Escritor
lookandflywithme@gmail.com	Eva María	.. 	2016-12-12 10:18:00	Maria	Escritor
covarrubiasedo@gmail.com	Eduardo	Covarrubias Saenz	2016-12-12 13:29:00	Maria	Escritor
info@artopiastudio.com	Pablo	Solis 	2016-12-13 18:05:00	Maria	Escritor
chernandezgarri@gmail.com	Cristina	.. 	2016-12-13 18:11:00	Maria	Escritor
yazminlor@yahoo.es	Yazmin	Lorenzo 	2016-12-14 11:02:00	Maria	Escritor
raiberdisseny@gmail.com	Sergi	Solivelles Castillo	2016-12-14 18:25:00	Maria	Escritor
javism28@yahoo.es	Javier	Segovia Madrigal	2016-12-19 13:17:00	Maria	Escritor
maria.santossanchez.81@gmail.com	María	Santos Sánchez	2016-12-19 18:25:00	Maria	Escritor
albertoguaitatello@gmail.com	Alberto	Guaita Tello	2016-12-20 14:32:00	Maria	Escritor
minorinmakes@gmail.com	Paloma	López 	2016-12-22 17:28:00	Maria	Escritor
alecab18@gmail.com	Alejandro	Cabrera 	2016-12-22 18:41:00	Maria	Escritor
marionaalonso@hotmail.com	Mariona	.. 	2016-12-27 12:35:00	Maria	Escritor
carmen.delarosa@goya.eu	Carmen	De La Rosa 	2017-01-04 09:55:00	Maria	Escritor
plblan86@gmail.com	Patricia	Lázaro 	2017-01-04 10:59:00	Maria	Escritor
aitorgallegos@gmail.com	AITOR	GALLEGO 	2017-01-04 13:20:00	Maria	Escritor
dugoisa@gmail.com	Isabel	Dugo 	2017-01-05 12:20:00	Maria	Escritor
teruel_silzor@gva.es	Silvia	Teruel 	2017-01-10 17:31:00	Maria	Escritor
marta.estringana@gmail.com	Marta	López Estríngana	2017-01-12 13:50:00	Maria	Escritor
omarsotom@hotmail.com	Omar	Sotomayor Noel	2017-01-12 16:25:00	Maria	Escritor
sferre43@gmail.com	Silvia	Ferré 	2017-01-12 16:38:00	Maria	Escritor
nasglot@gmail.com	Francisco	.. 	2017-01-13 10:38:00	Maria	Escritor
edwinsandmor@gmail.com	Edwin	Sandoval 	2017-01-13 10:51:00	Maria	Escritor
Francis_jaen@hotmail.es	Francisco	Jiménez 	2017-01-13 11:12:00	Maria	Escritor
babubitos@gmail.com	MANY	.. 	2017-01-13 17:20:00	Maria	Escritor
octaviopintos@gmail.com	Octavio	Pintos 	2017-01-16 10:06:00	Maria	Escritor
fran_3392@hotmail.com	Francisco	Martin Rodriguez	2017-01-16 10:29:00	Maria	Escritor
luisajung@outlook.com	Luisa	Jung 	2017-01-16 11:00:00	Maria	Escritor
mariananoelnakama@yahoo.com.ar	Mariana	Nakama 	2017-01-16 17:54:00	Maria	Escritor
danillabres@yahoo.es	Dani	Llabrés 	2017-01-18 12:56:00	Maria	Escritor
maarc155@hotmail.com	Milena	Marcela Calderón	2017-01-18 14:07:00	Maria	Escritor
mariateruelreguera@gmail.com	Maria	Teruel 	2017-01-19 17:19:00	Maria	Escritor
irenemontano.imago@gmail.com	Irene	Montano 	2017-01-19 18:20:00	Maria	Escritor
desm91@gmail.com	Desiree	.. 	2017-01-20 17:14:00	Maria	Escritor
ghoulmez@gmail.com	Alejandro	Gomez 	2017-01-23 10:22:00	Maria	Escritor
sobrarte@gmail.com	SUSANA	SOBRÁ 	2017-01-23 13:38:00	Maria	Escritor
yury.fedorchenko@gmail.com	Yura	Fedorchenko 	2017-01-23 13:45:00	Maria	Escritor
kikeitf@hotmail.com	ENRIQUE	VAQUER CARDONA	2017-01-24 12:28:00	Maria	Escritor
piky_nr@hotmail.com	Pilar	Nuñez Robles	2017-01-24 13:32:00	Maria	Escritor
mariola1995@hotmail.com	Dolores Fuensanta	Buendía Vivo	2017-01-24 16:30:00	Maria	Escritor
annamorato@yahoo.com	Anna	Morato 	2017-01-24 18:19:00	Maria	Escritor
jdelgado.granada@gmail.com	José	Delgado Ruiz	2017-01-24 18:51:00	Maria	Escritor
glorialilia@gmx.de	Gloria	Lilia Burgens	2017-01-26 14:04:00	Maria	Escritor
silviagarciaart@gmail.com	Silvia	Garcia 	2017-01-26 18:27:00	Maria	Escritor
lobuslotus2@gmail.com	Óscar	Pérez 	2017-01-30 11:38:00	Maria	Escritor
sergio@sergioceron.es	Sergio	Cerón 	2017-01-30 11:45:00	Maria	Escritor
oscarfdz@hotmail.com	Óscar	Fernández 	2017-01-30 13:25:00	Maria	Escritor
lrgm2017@gmail.com	Lisandro	Reholón 	2017-01-30 13:30:00	Maria	Escritor
andresnava99@gmail.com	Andrés	Fernández Gutiérrez	2017-01-30 13:46:00	Maria	Escritor
mjose1984@hotmail.com	María Jose	.. 	2017-01-30 18:15:00	Maria	Escritor
ogalla@dgraficos.com	Virginia	Ogalla 	2017-01-30 18:21:00	Maria	Escritor
mariahrcr@gmail.com	María	Hernández 	2017-02-03 11:43:00	Maria	Escritor
mperezrodriguez@hotmail.com	Maria	Pérez Rodríguez	2017-02-03 17:19:00	Maria	Escritor
carlosbahos@gmail.com	Carlos	Bahos 	2017-02-03 18:44:00	Maria	Escritor
loboverdugo@hotmail.com	FRANCISCO JAVIER	VERDUGO GONZALEZ	2017-02-06 18:01:00	Maria	Escritor
estirubi@hotmail.es	Esther	.. 	2017-02-06 18:04:00	Maria	Escritor
espacionuz@gmail.com	Juan Carlos	Nuñez 	2017-02-08 18:06:00	Maria	Escritor
infotucuentopersonal@gmail.com	Abi	Wall 	2017-02-08 18:59:00	Maria	Escritor
zibidobosz@gmail.com	Zibi	Dobosz 	2017-02-08 19:03:00	Maria	Escritor
trsa_lc@msn.com	Teresa	Pinedo 	2017-02-08 19:06:00	Maria	Escritor
califut@gmail.com	Carlos Andres	Perez Boada	2017-02-09 11:08:00	Maria	Escritor
davidg_ortas@hotmail.com	David	.. 	2017-02-09 11:32:00	Maria	Escritor
cuentosdeltropico@gmail.com	Herminia	Peña Aznar	2017-02-09 11:38:00	Maria	Escritor
hello.lauragomez@gmail.com	Laura	Gomez 	2017-02-09 11:43:00	Maria	Escritor
eugeniaariaslopez@hotmail.com	Eugenia	Arias 	2017-02-09 18:36:00	Maria	Escritor
silvia@silviacabello.com	SILVIA	CABELLO 	2017-02-10 12:57:00	Maria	Escritor
belenmorenonunez@gmail.com	Belen	Moreno 	2017-02-13 11:30:00	Maria	Escritor
cripeboi@gmail.com	Cristina	Pérez 	2017-02-13 13:40:00	Maria	Escritor
anxelesferrer@hotmail.com	Anxeles	Rodriguez Ferrer	2017-02-13 14:04:00	Maria	Escritor
milamoon@gmail.com	Renata	Srpcanska 	2017-02-13 14:19:00	Maria	Escritor
ddgarciagg@gmail.com	David	Garcia Gomez	2017-02-14 14:29:00	Maria	Escritor
thais_plg@hotmail.com	Thais	Granero De Alberdi	2017-02-14 16:33:00	Maria	Escritor
hola@saludosalpollo.com	Agurtzane	Abajo 	2017-02-15 13:11:00	Maria	Escritor
ermacoravictoria@gmail.com	Victoria	Ermacora 	2017-02-17 13:45:00	Maria	Escritor
sochovo@gmail.com	Sagrario	Ochovo Puebla	2017-02-17 14:20:00	Maria	Escritor
zenasantana.editorial@gmail.com	Zenaida	Santana 	2017-02-17 14:25:00	Maria	Escritor
infotirandodelrizo@gmail.com	Maria Jose	Floriano 	2017-02-20 13:55:00	Maria	Escritor
carmencioranu@gmail.com	Carmen	Cioranu 	2017-02-20 14:04:00	Maria	Escritor
mariaelviragonzalez@yahoo.com	Maria Elvira	Gonzalez 	2017-02-20 17:16:00	Maria	Escritor
daviddegilgomez@gmail.com	David	De Gil Gómez	2017-02-20 17:19:00	Maria	Escritor
javiesejota@yahoo.es	Javier	González Fernández	2017-02-21 11:51:00	Maria	Escritor
soniasanm@gmail.com	Sonia	Sánchez 	2017-02-21 14:24:00	Maria	Escritor
jamg.officialbox@gmail.com	Jamg	Freelineartist 	2017-02-22 17:45:00	Maria	Escritor
sofi.escude@gmail.com	Sofi	Escude 	2017-02-22 17:55:00	Maria	Escritor
cesarverduguez@gmail.com	Cesar	Verduguez 	2017-02-23 12:35:00	Maria	Escritor
anadelina@yahoo.es	Ani	Perea 	2017-02-23 13:29:00	Maria	Escritor
iriajq@hotmail.com	Iria	Juaneda Quintanilla	2017-02-23 18:13:00	Maria	Escritor
etanner519@gmail.com	Eileen	Tanner 	2017-03-03 11:02:00	Maria	Escritor
ergarevalillo@gmail.com	Eduardo	Rivera Gómez-Arevalillo	2017-03-06 18:19:00	Maria	Escritor
artemanolo@hotmail.com	Manuel	De La Torre Peña	2017-03-06 18:54:00	Maria	Escritor
poetavadu@outlook.com	Uvel	Vazquez 	2017-03-07 17:27:00	Maria	Escritor
mariancreacionliteraria@gmail.com	Marian	Garcia 	2017-03-07 18:30:00	Maria	Escritor
isimon3@hotmail.com	Iñaki	.. 	2017-03-08 10:19:00	Maria	Escritor
susanacela82@hotmail.com	SUSANA	CELA LOPEZ	2017-03-08 12:11:00	Maria	Escritor
acuapicolina@gmail.com	María José	Romero Toscano	2017-03-08 13:09:00	Maria	Escritor
yolandaas@gmail.com	Yolanda	Alonso Sanz	2017-03-10 11:46:00	Maria	Escritor
licsoledadzapata@gmail.com	SOLEDAD	ZAPATA 	2017-03-13 12:54:00	Maria	Escritor
lilianabuigues@gmail.com	Liliana	Gago Buigues	2017-03-13 13:04:00	Maria	Escritor
mjosepulifer@gmail.com	María José	Pulido Fernández	2017-03-13 13:31:00	Maria	Escritor
loreley783@gmail.com	Neylar	Hernández 	2017-03-13 13:38:00	Maria	Escritor
marcobalvin@yahoo.es	Marco	Balvín Ortega	2017-03-13 13:52:00	Maria	Escritor
sergiomirajordan@gmail.com	Sergio	Mira Jordán	2017-03-13 13:58:00	Maria	Escritor
info@estudiognomo.com	Dani	Viéitez 	2017-03-13 14:04:00	Maria	Escritor
felipe_martin_76@hotmail.com	Felipe	Martin Moreno	2017-03-13 14:09:00	Maria	Escritor
yessgui@hotmail.com	JESSICA	.. 	2017-03-13 14:12:00	Maria	Escritor
danhellez@gmail.com	Dani	Hellez 	2017-03-13 14:34:00	Maria	Escritor
artromyb@gmail.com	Romy	Berntsen 	2017-03-13 14:39:00	Maria	Escritor
marta.revuelta@yahoo.es	Marta	Revuelta 	2017-03-13 16:21:00	Maria	Escritor
Soniasanzescudero@gmail.com	Sonia	Sanz Escudero	2017-03-13 16:24:00	Maria	Escritor
estefani.bravo@gmail.com	Estefani Con E	.. 	2017-03-13 16:32:00	Maria	Escritor
monimg_1982@hotmail.com	Monica	Mínguez AlcázaR	2017-03-13 16:41:00	Maria	Escritor
anukicreationsinfo@gmail.com	Ana	Lopez 	2017-03-13 16:44:00	Maria	Escritor
ramaragacuentos@gmail.com	Marisa Y Rafa	.. 	2017-03-15 18:51:00	Maria	Escritor
gabriella.gasp@gmail.com	Maria Gabriella	Gasparri 	2017-03-16 14:16:00	Maria	Escritor
venuseros10@gmail.com	Francisco Javier	Díaz Rengel	2017-03-16 14:27:00	Maria	Escritor
susigg2@yahoo.es	Susi	Gonzalez Garrido	2017-03-16 14:33:00	Maria	Escritor
juanignacioa92@gmail.com	Juan Ignacio	Amayo 	2017-03-20 13:30:00	Maria	Escritor
angelatornerofernandez@yahoo.es	Ángela	Tornero Fernández	2017-03-21 18:19:00	Maria	Escritor
contacto@rafaestudio.com	Rafa	Garcia 	2017-03-23 11:24:00	Maria	Escritor
davidpedrera@yahoo.es	David	Pedrera Macías	2017-03-23 17:32:00	Maria	Escritor
ibagalan@gmail.com	Alfredo	Ibáñez García	2017-03-27 18:33:00	Maria	Escritor
fanyusta@hotmail.com	Estefania	Yusta 	2017-03-28 13:50:00	Maria	Escritor
andser@live.com	Francisco	Fernandez Santana	2017-03-28 17:02:00	Maria	Escritor
ruizpachecopedro@gmail.com	Pedro	Ruíz Pacheco	2017-03-28 17:35:00	Maria	Escritor
donapozomarina@gmail.com	Marina	Doña Pozo	2017-03-29 14:25:00	Maria	Escritor
info@gerardo-perez.com.ar	Gerardo	Perez 	2017-03-30 13:30:00	Maria	Escritor
evabramo@gmail.com	Eva	Braojos 	2017-04-03 14:39:00	Maria	Escritor
comandojaza@gmail.com	Lucas	Baró 	2017-04-03 18:28:00	Maria	Escritor
marianacurr@gmail.com	Mari Curros	.. 	2017-04-03 18:40:00	Maria	Escritor
diazhurtado@gmail.com	Rodrigo	Diaz Hurtado	2017-04-04 18:48:00	Maria	Escritor
infoartviu@gmail.com	David	Peñaranda 	2017-04-04 18:51:00	Maria	Escritor
recargolat2014@gmail.com	Jose Luis	Aragon 	2017-04-05 10:27:00	Maria	Escritor
cgar@telefonica.net	Cristina	Garmendia Frutos	2017-04-05 10:50:00	Maria	Escritor
carmenmujerdeluz@gmail.com	Carmen	Vila Barberá	2017-04-05 14:43:00	Maria	Escritor
lucianapnigro@hotmail.com	Luciana	Nigro 	2017-04-05 17:03:00	Maria	Escritor
lauravorobiof@hotmail.com	Laura	Vorobiof 	2017-04-05 18:54:00	Maria	Escritor
anasanchezmarin@hotmail.com	Ana	Sánchez Marín	2017-04-07 09:52:00	Maria	Escritor
javieranisa1977@hotmail.com	Javier	Anisa 	2017-04-07 09:54:00	Maria	Escritor
admin@islacuentacuentos.es	Isabel	.. 	2017-04-07 11:59:00	Maria	Escritor
yolisantos28@gmail.com	Yoli	Santos 	2017-04-10 09:52:00	Maria	Escritor
saraproubi@hotmail.com	Sara	Proubasta 	2017-04-10 10:09:00	Maria	Escritor
lorenagoar3@gmail.com	Lorena	.. 	2017-04-10 11:43:00	Maria	Escritor
agustingarriga@gmail.com	Agustín	Garriga 	2017-04-10 17:39:00	Maria	Escritor
irene.ferrer88@gmail.com	Irene	Ferrer 	2017-04-12 12:33:00	Maria	Escritor
AVA@once.es	Antonio	Valles Arandiga	2017-04-12 14:24:00	Maria	Escritor
albapalacioest@gmail.com	Alba	Palacio 	2017-04-18 17:08:00	Maria	Escritor
samuelsanzmontesinos@gmail.com	Samuel	Sanz Montesinos	2017-04-18 17:23:00	Maria	Escritor
hola@joaquincamp.com.ar	Joaquin	Camp 	2017-04-19 18:08:00	Maria	Escritor
Barbora.Augustinova@seznam.cz	Barbora	Augustinova 	2017-04-25 18:18:00	Maria	Escritor
amvediciones@amvediciones.com	AMV	EDICIONES 	2017-04-26 09:52:00	Maria	Escritor
ibanrocafont@gmail.com	Ibán	Roca 	2017-04-26 17:08:00	Maria	Escritor
patricia.vilardebo@gmail.com	Patricia	Vilardebó 	2017-04-26 18:24:00	Maria	Escritor
topanga11@gmail.com	Inma	Alonso 	2017-04-27 18:24:00	Maria	Escritor
marcelomayerestilomm@yahoo.com.ar	Marcelo	Mayer 	2017-04-28 11:56:00	Maria	Escritor
Isabelconesa@hotmail.com	Isabel	Conesa 	2017-04-28 11:59:00	Maria	Escritor
emiliochec@gmail.com	Emilio	Checa 	2017-04-28 12:38:00	Maria	Escritor
marinailustracion@gmail.com	Marina	Rodriguez 	2017-04-28 14:46:00	Maria	Escritor
mdelrgg@gmail.com	Rosario	Garci Gil	2017-05-02 14:20:00	Maria	Escritor
zna123@freenet.de	Akiles	Sanz 	2017-05-02 18:08:00	Maria	Escritor
rakelcrea@gmail.com	Raquel	Bonilla 	2017-05-03 10:16:00	Maria	Escritor
emiliadj.dcv@gmail.com	Emilia	.. 	2017-05-03 10:54:00	Maria	Escritor
nicolaspuente@yahoo.de	Nicolas	Puente 	2017-05-03 12:37:00	Maria	Escritor
intersarani@hotmail.com	Roberto	Moreno 	2017-05-03 17:06:00	Maria	Escritor
milena.lister@gmail.com	Milena	.. 	2017-05-03 17:25:00	Maria	Escritor
artany2@hotmail.com	Zahira	Atalaya Guijarro	2017-05-03 17:31:00	Maria	Escritor
marialbornos@hotmail.com	Mari Carmen	Albornos Pintos	2017-05-03 17:38:00	Maria	Escritor
david31guerr@gmail.com	Javier David	Menéndez Guerra	2017-05-03 17:40:00	Maria	Escritor
hanna.skorpen@gmail.com	Hanna Skorpen	Claeson Moreno	2017-05-03 17:42:00	Maria	Escritor
cecilia.lapuente@gmail.com	Cecilia	Lapuente 	2017-05-03 17:46:00	Maria	Escritor
mariagoneda@gmail.com	María	González Pineda	2017-05-03 17:48:00	Maria	Escritor
descorcia71@hotmail.com	DAGOBERTO	ESCORCIA SAMACA	2017-05-03 17:52:00	Maria	Escritor
floriapons@hotmail.com	Floria	Pons Coll	2017-05-03 17:54:00	Maria	Escritor
jeremiasaguilar76@gmail.com	Jeremias	Aguilar Lopez	2017-05-04 11:50:00	Maria	Escritor
akiles@dianasanz.de	Michael	Schmidt Salomon	2017-05-04 12:49:00	Maria	Escritor
jackie.nogales07@gmail.com	Jackie	Nogales 	2017-05-04 13:08:00	Maria	Escritor
ladyranita@gmail.com	AURORA	MENDEZ MAZANO	2017-05-04 13:19:00	Maria	Escritor
gemmaalonsolope@hotmail.com	Gemma	.. 	2017-05-05 11:25:00	Maria	Escritor
ensastories@gmail.com	SANDRA	LOBATO 	2017-05-08 13:08:00	Maria	Escritor
marga.estevez.martin@gmail.com	Marga	Estévez Martín	2017-05-09 10:23:00	Maria	Escritor
lag2786@gmail.com	Lisandra	Alvarez 	2017-05-09 10:32:00	Maria	Escritor
i.navarro.juan@gmail.com	Isabel	Navarro 	2017-05-09 11:23:00	Maria	Escritor
luchinguer_@hotmail.com	Lucia	Hernandez 	2017-05-16 16:56:00	Maria	Escritor
fantasioso140@hotmail.com	Fernando	Lee Gonzales	2017-05-16 17:13:00	Maria	Escritor
corre_privado@yahoo.com	Aarón	Andres 	2017-05-16 17:20:00	Maria	Escritor
merisaldana@gmail.com	Meritxell	Saldaña Reaño	2017-05-18 10:52:00	Maria	Escritor
carolinasolano@mgtcomunicacion.com	Carolina	Solano 	2017-05-19 11:08:00	Maria	Escritor
anabelen.santosmartin@gmail.com	Ana	.. 	2017-05-22 11:40:00	Maria	Escritor
martaherrero412@gmail.com	Marta	.. 	2017-05-22 17:30:00	Maria	Escritor
josemariagil.design@gmail.com	Josma	Gil 	2017-05-22 18:50:00	Maria	Escritor
carolinasolano@icloud.com	Carolina	Solano 	2017-05-23 14:18:00	Maria	Escritor
vihard2006@gmail.com	Vicenç	.. 	2017-05-24 12:35:00	Maria	Escritor
itobar@iies.es	Ignacio	Tobaruela Delgado Ignacio Tobaruela Delgado	2017-05-24 12:38:00	Maria	Escritor
felipe_rodrigues_@live.com	Felipe	Rodrigues 	2017-05-24 13:24:00	Maria	Escritor
erretres10@yahoo.es	Raquel	Romero Redondo	2017-05-24 18:08:00	Maria	Escritor
amayaquiroga@gmail.com	Amaya	Quiroga 	2017-05-24 18:15:00	Maria	Escritor
contacto@cudicuga.es	Lourdes	Fariñas Puig	2017-05-24 18:19:00	Maria	Escritor
margotromano@hotmail.com	Margot	.. 	2017-05-24 18:22:00	Maria	Escritor
espetello@gmail.com	Esperanza	López-Tello 	2017-05-24 18:38:00	Maria	Escritor
hector_i.r._1993@hotmail.com	Héctor	Infante Rodríguez	2017-05-24 18:45:00	Maria	Escritor
joseehsma@hotmail.com	José Antonio	Vílchez Triguero	2017-05-24 18:52:00	Maria	Escritor
carolenrech@yahoo.es	Carolina	Enrech Pérez	2017-05-24 19:00:00	Maria	Escritor
cruzberar@gmail.com	Mari Cruz	Berruga Archilla	2017-05-26 12:59:00	Maria	Escritor
r_david_r@hotmail.com	David	Rodriguez 	2017-05-29 10:49:00	Maria	Escritor
e_nanum@hotmail.com	Encarni	Jimenez Montes	2017-05-29 18:13:00	Maria	Escritor
teisaf@hotmail.com	Teisa	Figueira Platas	2017-05-31 13:18:00	Maria	Escritor
todosloscuentosdelmundo@gmail.com	Pilar	Leiva 	2017-06-01 14:14:00	Maria	Escritor
garavagliajuan@gmail.com	Juan	.. 	2017-06-01 17:15:00	Maria	Escritor
olga_pardo_escudero@hotmail.com	Olga	Pardo Escudero	2017-06-01 18:30:00	Maria	Escritor
davidgs84@hotmail.com	David	Gallardo Salvador	2017-06-02 10:15:00	Maria	Escritor
carreras_3aac@hotmail.com	Luzmary	Coromoto Tovar	2017-06-08 14:41:00	Maria	Escritor
sugey.robles@international.gc.ca	Sugey	Robles 	2017-06-12 11:37:00	Maria	Escritor
c2e-@hotmail.com	Carolina	Cebrian 	2017-06-12 16:45:00	Maria	Escritor
asociacioncinetika@yahoo.es	Matilde	Cinetica 	2017-06-14 16:45:00	Maria	Escritor
lola.urbano@hotmail.com	LOLA	URBANO 	2017-06-16 10:06:00	Maria	Escritor
elpetitcaracol@gmail.com	Gemma	Font 	2017-06-20 10:01:00	Maria	Escritor
evatricer@yahoo.es	Eva	Trigo Cervera	2017-06-22 10:26:00	Maria	Escritor
josialvaradovalero@gmail.com	JOSI	ALVARADO VALERO	2017-06-22 10:56:00	Maria	Escritor
mep1621@hotmail.com	Elena	Pirrone 	2017-06-22 13:19:00	Maria	Escritor
floryquito@hotmail.com	Florencia	Casado Tellier	2017-06-22 14:23:00	Maria	Escritor
Brios@sc.cm.rimed.cu	Brios	.. 	2017-06-22 14:28:00	Maria	Escritor
solrac143@gmail.com	Carlos	Fernandez 	2017-06-23 13:29:00	Maria	Escritor
RGBL1152.PROYECTO@gmail.com	Rafael	Guillermo 	2017-06-27 17:02:00	Maria	Escritor
trybalblz@gmail.com	Txema	Pinedo 	2017-06-27 18:03:00	Maria	Escritor
belen.galiotti@gmail.com	Belén	Galiotti 	2017-06-27 18:37:00	Maria	Escritor
candelariagar3999@gmail.com	Rafael	Moreno Garcia	2017-06-28 17:05:00	Maria	Escritor
cabalgando_juntos@hotmail.com	Judith	Espin Tribes	2017-06-30 13:19:00	Maria	Escritor
fran_tnt@hotmail.com	Francisco	.. 	2017-06-30 14:53:00	Maria	Escritor
npasillas80@gmail.com	Noemi	Pasillas 	2017-07-03 11:07:00	Maria	Escritor
teresapascuals@gmail.com	Teresa	Pascual Soler	2017-07-03 11:17:00	Maria	Escritor
irmagarcia_40@hotmail.com	Irma Clara	Garcia Galindo	2017-07-03 11:23:00	Maria	Escritor
embajadaeventos@um.dk	Diego	Mette 	2017-07-03 18:57:00	Maria	Escritor
javi_5891@hotmail.com	Javier	.. 	2017-07-03 18:59:00	Maria	Escritor
creadoresbooz@gmail.com	Jeffer	David 	2017-07-03 19:01:00	Maria	Escritor
palomacamilojosecela@gmail.com	PALOMA	HOYOS 	2017-07-04 09:47:00	Maria	Escritor
solgaldamesdiaz@gmail.com	M Soledad	Galdames Díaz	2017-07-05 11:57:00	Maria	Escritor
covisn@gmail.com	Covi	Sánchez 	2017-07-05 13:12:00	Maria	Escritor
antonio.ibarra.santiago@gmail.com	Antonio	Ibarra 	2017-07-05 18:54:00	Maria	Escritor
jorgitocelulito@hotmail.com	JORGE	ROMERA GUERRERO	2017-07-06 17:20:00	Maria	Escritor
antonia.pinas@gmail.com	Antonia	Piñas 	2017-07-07 14:57:00	Maria	Escritor
jicamo223@gmail.com	Jidibeth	Caicedo 	2017-07-11 11:07:00	Maria	Escritor
jose.urbano56@hotmail.com	Jose	Urbano Rodriguez	2017-07-11 15:18:00	Maria	Escritor
jorcardenas0@gmail.com	Jorge	Cárdenas Sánchez	2017-07-11 17:05:00	Maria	Escritor
amandineben@outlook.com	Amandine	Ben 	2017-07-12 16:27:00	Maria	Escritor
alvaro_keto@hotmail.com	Alvaro	Pascual Martínez	2017-07-13 10:57:00	Maria	Escritor
cgjaso@hotmail.com	Cristina	Gonzalvo 	2017-07-17 13:16:00	Maria	Escritor
cbescansaj@gmail.com	Carmen	.. 	2017-07-17 13:33:00	Maria	Escritor
rfmonse@gmail.com	Monse	Rodriguez 	2017-07-17 14:14:00	Maria	Escritor
martaraminico@gmail.com	Marta	Ramírez Nicolás	2017-07-18 12:56:00	Maria	Escritor
maria@babidibulibros.com	Maria	Calero 	2017-07-18 13:03:00	Maria	Escritor
mjsabio@hotmail.com	María Jesús	Garrido Sabio	2017-07-19 17:56:00	Maria	Escritor
rvdelvalle@hotmail.com	ROCIO DEL VALLE	.. 	2017-07-20 16:36:00	Maria	Escritor
noemicita78@gmail.com	Noemí	Valiente 	2017-07-20 16:40:00	Maria	Escritor
tallonalvarez@hotmail.com	Iago	Tallon Álvarez	2017-07-21 09:40:00	Maria	Escritor
lifosede@gmail.com	Liana	Fornier De Serres	2017-07-21 11:03:00	Maria	Escritor
braccodog@gmail.com	Eduardo	Capuano 	2017-07-24 11:47:00	Maria	Escritor
pabloibarsdemuller@gmail.com	Pablo	Ibars De Muller	2017-07-24 18:09:00	Maria	Escritor
lauraurcelayescritora@gmail.com	Laura	Ruiz Benito	2017-07-24 18:38:00	Maria	Escritor
cristian.andrade27@gmail.com	Cristian	De Andrade Correia	2017-07-25 10:12:00	Maria	Escritor
felipe.felipeponce.urrutia@gmail.com	Felipe	Urrutia 	2017-07-25 10:16:00	Maria	Escritor
elisa.elisenda@gmail.com	Elisa	Saez Vidal	2017-07-25 13:52:00	Maria	Escritor
itomusicaukulele@gmail.com	Alvaro	Rubio 	2017-07-26 10:17:00	Maria	Escritor
cereng@gmail.com	Ceren	Gergeroglu Akgul	2017-07-26 18:14:00	Maria	Escritor
lic.virginia.gutierrez@gmail.com	Virginia	Gutierrez Bolcatto	2017-07-31 12:15:00	Maria	Escritor
info@anjabrinkmann.com	Anja Brinkmann Y Marta Castillo	.. 	2017-07-31 14:19:00	Maria	Escritor
ingbburgos@gmail.com	Brallan	Burgos 	2017-07-31 17:44:00	Maria	Escritor
egleegonz@hotmail.com	Eglee	Gonzalez 	2017-07-31 18:41:00	Maria	Escritor
francorodriguez677@gmail.com	Franco	.. 	2017-08-01 09:40:00	Maria	Escritor
roisro@hotmail.com	Rocio	.. 	2017-08-01 12:22:00	Maria	Escritor
yolrc2015@gmail.com	YOLANDA	ROYO CALVO	2017-08-01 12:36:00	Maria	Escritor
alycia.alba@hotmail.com	Alycia	Alba 	2017-08-01 13:04:00	Maria	Escritor
alichesacasa@gmail.com	Alicia	Chesa Casado	2017-08-02 17:17:00	Maria	Escritor
cuentosparadragoncitos@gmail.com	Juani	Velilla Montes	2017-08-02 17:50:00	Maria	Escritor
carolina_ulens@hotmail.com	Carolina	Ulens 	2017-08-03 09:29:00	Maria	Escritor
castells.cruzado.raul@repsol.com	RAUL	CASTELLS CRUZADO	2017-08-04 12:11:00	Maria	Escritor
velazfreixas@gmail.com	Veroushka	.. 	2017-08-07 10:52:00	Maria	Escritor
adrianaleiss@hotmail.com	Adriana	.. 	2017-08-07 11:00:00	Maria	Escritor
masandau@yahoo.com	MARIA	SANCHEZ DAUDER	2017-08-07 11:02:00	Maria	Escritor
danielaposmagiu@hotmail.com	Daniel	Cuñarro Bango	2017-08-07 11:04:00	Maria	Escritor
ismael.ugarte@gmail.com	Ismael	Ugarte 	2017-08-08 09:55:00	Maria	Escritor
nuria_san_and@yahoo.es	Nuria Sanchez	Guerra Martin	2017-08-08 14:18:00	Maria	Escritor
ampi302@gmail.com	Amparo	García 	2017-08-16 09:44:00	Maria	Escritor
tommyvillarreal@gmail.com	Tomas	Villarreal 	2017-08-16 12:34:00	Maria	Escritor
paomotta02@hotmail.com	Erika Paola	Motta 	2017-08-17 10:26:00	Maria	Escritor
joemyblanco@gmail.com	Joemy	Blanco 	2017-08-21 12:09:00	Maria	Escritor
angeldelaserna@grow.es	Angel	De La Serna 	2017-08-24 12:14:00	Maria	Escritor
saro.dia@hotmail.com	Saro	Díaz 	2017-08-25 14:15:00	Maria	Escritor
adrianaperezsantana@outlook.com	Adriana	Pérez 	2017-09-12 12:52:00	Maria	Escritor
recortarpalavras@gmail.com	Recortar 	Palavras 	2017-09-12 12:55:00	Maria	Escritor
historiadorericlara@gmail.com	Eric 	Lara 	2017-09-12 12:56:00	Maria	Escritor
tarmarm@gmail.com	Tomás 	Armas 	2017-09-12 12:58:00	Maria	Escritor
arubendl@yahoo.es	Rubén	Regalado 	2017-09-12 13:33:00	Maria	Escritor
veronicabernardinogonzalez@gmail.com	Veronica Covadonga	Bernardino  Gonzalez	2017-09-12 13:45:00	Maria	Escritor
brauliobarquero@gmail.com	Braulio 	Barquero 	2017-09-12 13:58:00	Maria	Escritor
recursosmx@yahoo.com	Francisco Javier 	Segura  Mojica	2017-09-12 14:01:00	Maria	Escritor
estef.ros@hotmail.com	Estefania 	Ros  Galvez	2017-09-13 13:10:00	Maria	Escritor
oflop@hotmail.com	Ofelia 	Lopez 	2017-09-14 19:40:00	Maria	Escritor
danilopezgbarrera@hotmail.com	Daniel 	López  	2017-09-14 19:43:00	Maria	Escritor
ggillustrations@yahoo.es	Gabriela 	Graham 	2017-09-18 10:27:00	Maria	Escritor
tannysandragonzales@gmail.com	Sandra Gissella 	Gonzales  Flores	2017-09-18 13:12:00	Maria	Escritor
breinerfortichmeza@gmail.com	Breiner 	Fortich 	2017-09-18 16:45:00	Maria	Escritor
karladigar@gmail.com	KARLA 	DÍAZ 	2017-09-22 14:19:00	Maria	Escritor
loredu101@hotmail.com	Eduardo 	Lorenzo 	2017-09-22 14:32:00	Maria	Escritor
jtroush2001@yahoo.com	JT 	Torres  	2017-09-22 14:39:00	Maria	Escritor
fany198@hotmail.com	Estefania	Ortega  Garcia	2017-09-25 17:20:00	Maria	Escritor
andrea.acbno@gmail.com	Andrea 	Acedo 	2017-09-25 18:11:00	Maria	Escritor
estitxujimenezcrespo@gmail.com	Estitxu 	Jimenez 	2017-09-25 18:49:00	Maria	Escritor
heidi.iuorno@gmail.com	Zoraida 	Luorno 	2017-09-26 13:45:00	Maria	Escritor
jerselporcupine@gmail.com	Porcupine	.. 	2017-09-28 18:37:00	Maria	Escritor
kohn.michel17@gmail.com	Michel	Kohn  	2017-09-28 18:45:00	Maria	Escritor
kelsy20@hotmail.com	Kelsy 	Quiroz 	2017-09-28 18:50:00	Maria	Escritor
augustopanessi@hotmail.com	Augusto 	Panessi 	2017-09-29 11:25:00	Maria	Escritor
betty@conhoff.de	Betty 	Conhoff  	2017-09-29 12:54:00	Maria	Escritor
maria.anguloaguado@gmail.com	Maria	Angulo 	2017-09-29 13:57:00	Maria	Escritor
ppulgaralves@gmail.com	Paula 	Pulgar  Alves	2017-10-02 17:04:00	Maria	Escritor
david@davidcasals-roma.com	David 	Casals-Roma 	2017-10-03 12:28:00	Maria	Escritor
sergio@inhispania.com	Sergio 	Palacios Inhispania	2017-10-03 12:44:00	Maria	Escritor
b.romeroclinica@gmail.com	Bárbara 	Romero 	2017-10-03 13:31:00	Maria	Escritor
lolimarsan@gmail.com	Lolimar 	Araujo  Morales	2017-10-04 17:17:00	Maria	Escritor
noelia_pasandodel@hotmail.com	Noelia 	Sanchez  Casero	2017-10-04 17:25:00	Maria	Escritor
mianmar26@hotmail.com	Miguel Angel 	Martinez  Cantillo	2017-10-05 11:16:00	Maria	Escritor
armando.92@nauta.cu	Armando	.. 	2017-10-05 11:23:00	Maria	Escritor
avivancoss@hotmail.com	Alberto 	Vivancos  Sanz	2017-10-06 13:20:00	Maria	Escritor
universe_nj@yahoo.com	Nooshin 	Jafari 	2017-10-09 12:28:00	Maria	Escritor
ainhoa.santosgoiko@gmail.com	Ainhoa 	Santos-Goikoetxea 	2017-10-09 13:50:00	Maria	Escritor
monica.mmm391@gmail.com	Monica	Martin  Martin	2017-10-09 17:20:00	Maria	Escritor
carlitoscopito@hotmail.es	Carlos	.. 	2017-10-09 17:57:00	Maria	Escritor
rdmanteca@gmail.com	Ricardo 	Díez 	2017-10-09 18:25:00	Maria	Escritor
pepevillenagonzalez@gmail.com	José 	Villena  González	2017-10-11 12:42:00	Maria	Escritor
dalilehpb@gmail.com	Dalileh	.. 	2017-10-16 10:36:00	Maria	Escritor
aulas.conscientes@gmail.com	Clara	.. 	2017-10-16 17:09:00	Maria	Escritor
patriciazoe7@gmail.com	Patricia	Mariela 	2017-10-16 17:15:00	Maria	Escritor
francismc76@hotmail.com	FRANCISCO JOSE 	MAYOR  CANO	2017-10-16 17:19:00	Maria	Escritor
lapulseradebronce@gmail.com	Becca 	Fernández 	2017-10-18 14:36:00	Maria	Escritor
mpilarpsico@gmail.com	PILAR 	GIMENEZ 	2017-10-18 17:45:00	Maria	Escritor
contacto@gioart.com.ar	GIO 	Fornieles 	2017-10-20 10:05:00	Maria	Escritor
clv_etra@hotmail.com	Cris L	Vargas 	2017-10-20 12:12:00	Maria	Escritor
capitansolo82@hotmail.com	Jose 	Carvajal  Hidalgo	2017-10-23 10:16:00	Maria	Escritor
en.carni92@hotmail.es	Encarni 	Garcia  Ruiz	2017-10-23 10:23:00	Maria	Escritor
samuletiluna@gmail.com	LETICIA 	CEBRIÁN  RUIZ 	2017-10-23 11:16:00	Maria	Escritor
josecarlosatienza@hotmail.com	Jose Carlos 	Atienza 	2017-10-23 13:35:00	Maria	Escritor
sarini_10@hotmail.com	Sara	.. 	2017-10-23 14:12:00	Maria	Escritor
spiderjuanita@ono.com	Juanita	.. 	2017-10-23 17:18:00	Maria	Escritor
coach@elinarees.com	Elina 	Rees 	2017-10-24 10:48:00	Maria	Escritor
mariaveg@gmail.com	María 	Vega  	2017-10-25 19:00:00	Maria	Escritor
paulaminanahurtado@gmail.com	Paula 	Miñana  Hurtado	2017-10-25 19:03:00	Maria	Escritor
n2hernando@gmail.com	Noemí 	Hernando 	2017-10-27 11:32:00	Maria	Escritor
lauranavarrogarces@gmail.com	Laura 	Navarro 	2017-10-31 13:53:00	Maria	Escritor
dbvizcaino@yahoo.es	David	Beltrán 	2017-11-03 11:28:00	Maria	Escritor
leticiavazquezll@hotmail.com	Leticia 	Vazquez  	2017-11-03 11:40:00	Maria	Escritor
luzmarinabaltasar@gmail.com	LUZ MARINA	BALTASAR 	2017-11-03 14:39:00	Maria	Escritor
ferran.marti.alcaire@gmail.com	Ferran 	Martí Alcaire	2017-11-03 14:48:00	Maria	Escritor
marion.bouldoire@orange.fr	Marion 	Bouldoire 	2017-11-06 10:20:00	Maria	Escritor
ilvanfilho@yahoo.com	ILVAN 	FILHO 	2017-11-06 10:43:00	Maria	Escritor
ac10de10@gmail.com	Jonathan 	Martin  Fernandez 	2017-11-06 13:16:00	Maria	Escritor
mireiaroal@hotmail.com	Mireia 	Roch  Alamo	2017-11-06 19:31:00	Maria	Escritor
maeva.iaorana@gmail.com	Sara 	Carrascosa  Fuentes	2017-11-06 19:39:00	Maria	Escritor
miprimerlibrodebomberos@gmail.com	Simón	Fuentes Navarro	2017-11-06 19:50:00	Maria	Escritor
rominadeluca1988@gmail.com	Romina	.. 	2017-11-06 19:55:00	Maria	Escritor
beleniky@hotmail.com	Belén 	Aznar  Espinosa	2017-11-06 19:58:00	Maria	Escritor
Auxicam13@hotmail.com	Auxi	Camacho 	2017-11-07 14:02:00	Maria	Escritor
Geral.rosamistica@gmail.com	Carla Maria 	Da Silva  Monteiro	2017-11-07 17:54:00	Maria	Escritor
susana.chantal.dominguez@gmail.com	Susana 	Chantal  Domínguez	2017-11-07 18:22:00	Maria	Escritor
nfo@creaciongrafica.es	Patricia 	López De San Román 	2017-11-07 19:16:00	Maria	Escritor
juasma21@gmail.com	Juan Manuel 	Sanchez 	2017-11-07 20:02:00	Maria	Escritor
silviaalvarezmerino@gmail.com	Silvia 	Álvarez 	2017-11-08 14:29:00	Maria	Escritor
sangumiel@gmail.com	ANA MARIA	SAN TEODORO  GUMIEL	2017-11-08 17:51:00	Maria	Escritor
inter.byl@gmail.com	Toñi 	López  Segura	2017-11-09 13:03:00	Maria	Escritor
iademasllo@hotmail.com	Consuelo	Araque 	2017-11-09 13:17:00	Maria	Escritor
dianis_effect@hotmail.com	Diana	.. 	2017-11-09 13:20:00	Maria	Escritor
csartit@gmail.com	CRISTINA 	SARTI  TERCERO 	2017-11-09 13:43:00	Maria	Escritor
hugocarrara4@gmail.com	Hugo 	Carrara 	2017-11-09 13:47:00	Maria	Escritor
laura.sanchez.maiz@gmail.com	Laura 	Sánchez Maíz	2017-11-09 18:17:00	Maria	Escritor
mariherrero58@gmail.com	Mari 	Herrero 	2017-11-13 18:45:00	Maria	Escritor
sophie_rcf@hotmail.com	Romina	.. 	2017-11-13 19:40:00	Maria	Escritor
clara.granara@gmail.com	Clara 	Granara 	2017-11-14 11:38:00	Maria	Escritor
AAD2016@outlook.es	JORDI	.. 	2017-11-14 18:24:00	Maria	Escritor
lorecurbelo78@icloud.com	Lorena 	Curbelo  Robayna	2017-11-15 10:47:00	Maria	Escritor
oscaruco1977@hotmail.com	Oscar 	Garcia 	2017-11-16 14:06:00	Maria	Escritor
guillermorugama@gmail.com	Guillem 	Martínez  Rugama	2017-11-16 19:30:00	Maria	Escritor
galtton@hotmail.com	Ana Bell	.. 	2017-11-16 19:40:00	Maria	Escritor
masbath@yahoo.es	Javier 	Jiménez  	2017-11-17 14:55:00	Maria	Escritor
elenamiguelcampos@yahoo.es	Elena 	 Miguel 	2017-11-17 15:06:00	Maria	Escritor
mmssrodriguez@gmail.com	Mayra	.. 	2017-11-17 15:13:00	Maria	Escritor
daniella.emiliani@gmail.com	Daniella 	Emiliani  Sanz 	2017-11-17 15:38:00	Maria	Escritor
martajuanotero@icloud.com	Marta 	Juan  Otero	2017-11-20 13:09:00	Maria	Escritor
carmen@gundemaro.com	Carmen 	Guzmán 	2017-11-20 13:24:00	Maria	Escritor
emiliaaliaga@hotmail.es	 EMILIA 	ALIAGA MARTINEZ	2017-11-20 19:55:00	Maria	Escritor
rlanseros@gmail.com	Raquel 	Lanseros 	2017-11-20 20:02:00	Maria	Escritor
ainasantamaria@gmail.com	Aina 	Santamaria 	2017-11-21 18:53:00	Maria	Escritor
martajuanotero@gmail.com	Marta 	JUAN  OTERO	2017-11-21 19:33:00	Maria	Escritor
ddt@ddtsfx.com	MONTSE 	RIBÉ 	2017-11-22 17:59:00	Maria	Escritor
cam1998garcia@gmail.com	Abigail 	.. 	2017-11-24 15:54:00	Maria	Escritor
veronicaica@yahoo.com.ar	Verónica 	Rivas  López	2017-11-27 11:49:00	Maria	Escritor
dhvillamor@yahoo.es	David	.. 	2017-11-28 10:40:00	Maria	Escritor
sofidavies@hotmail.com	Sofi 	Davies 	2017-11-28 19:19:00	Maria	Escritor
barbara.frohmann@gmail.com	Bárbara 	Frohmann 	2017-11-30 15:11:00	Maria	Escritor
JVILLARINOESPINO@gmail.com	Julio Eulogio 	Villarino  Espino	2017-11-30 18:59:00	Maria	Escritor
pilarponcelara@gmail.com	Pilar 	Ponce Lara	2017-12-01 15:54:00	Maria	Escritor
abellaperal@hotmail.com	Abel 	Laperal  	2017-12-04 12:36:00	Maria	Escritor
davidherranzgomez@hotmail.com	David 	Herranz  Gómez	2017-12-04 18:47:00	Maria	Escritor
orozcogonzaleznoelia@gmail.com	Noelia 	Orozco  Gonzalez	2017-12-04 19:23:00	Maria	Escritor
Mercuriopro@yahoo.com	Jorge 	Sotolongo 	2017-12-05 18:27:00	Maria	Escritor
francescasantana@gmail.com	Francesca	Santana 	2017-12-05 18:44:00	Maria	Escritor
elenviadodelrockandrollestaakidenuevo@hotmail.com	Manuel	.. 	2017-12-05 19:18:00	Maria	Escritor
eri.mavraki@gmail.com	Eri	Mavraki 	2017-12-05 19:24:00	Maria	Escritor
martazafrilla@gmail.com	Marta 	Zafrilla 	2017-12-11 15:28:00	Maria	Escritor
lepota.cosmo@hotmail.com	Lepota Lazar 	Luba  Cosmo	2017-12-11 18:08:00	Maria	Escritor
infodarimunoz@gmail.com	Dari 	Muñoz  Maurici	2017-12-11 18:28:00	Maria	Escritor
jfb.profe@gmail.com	Juan 	Fernandez  Bravo	2017-12-11 18:41:00	Maria	Escritor
gisela.laurenti@gmail.com	Gisela 	Carolina  Laurenti	2017-12-11 18:45:00	Maria	Escritor
mariatrigos@hotmail.com	Maria 	Trigos 	2017-12-11 19:31:00	Maria	Escritor
usoamendikute@hotmail.com	USOA 	MENDIKUTE 	2017-12-12 19:10:00	Maria	Escritor
lidia310lucia@gmail.com	Lidia 	Vicente 	2017-12-12 19:18:00	Maria	Escritor
iztarju@hotmail.com	Julia	Izquierdo  Tardón	2017-12-12 19:53:00	Maria	Escritor
juana_torrijos@hotmail.com	Juana 	Torrijos 	2017-12-13 10:56:00	Maria	Escritor
rosaosuna@gmail.com	Rosa 	Osuna 	2017-12-13 11:48:00	Maria	Escritor
luzmilenaria.mp30@gmail.com	Miriam 	Perea Becerra	2017-12-14 18:41:00	Maria	Escritor
torresguzman.raul@gmail.com	RAUL 	TORRES  GUZMAN	2017-12-14 18:56:00	Maria	Escritor
ivanizquierdof@gmail.com	Iván	Izquierdo  Fernández	2017-12-14 19:26:00	Maria	Escritor
ma.muttmusic@gmail.com	Ma	Mutt  	2017-12-15 11:52:00	Maria	Escritor
ADIATEATRO@hotmail.com	Francisco 	Burgos 	2017-12-15 11:58:00	Maria	Escritor
waltertobias77@yahoo.com	Walter 	Tobias 	2017-12-15 12:25:00	Maria	Escritor
nereidaturmero@gmail.com	Nereida 	Turmero 	2017-12-15 13:10:00	Maria	Escritor
aranzazurodriguez.26@gmail.com	Aránzazu	Rodríguez 	2017-12-18 19:33:00	Maria	Escritor
pedrojmariblancacorrales@hotmail.com	Pedro José 	Mariblanca Corrales	2017-12-19 11:01:00	Maria	Escritor
info@pennybrush.es	Oscar 	Escalera 	2017-12-19 14:53:00	Maria	Escritor
josefragoso@fragosoart.com	JOSE 	FRAGOSO 	2017-12-19 15:01:00	Maria	Escritor
ankaidrissi.hajar@gmail.com	Hajar	Anka  Idrissi	2017-12-20 13:18:00	Maria	Escritor
carmen.sanchez-canizares@plants.ox.ac.uk	Carmen 	Sánchez  Cañizares	2017-12-21 17:59:00	Maria	Escritor
dcucaresse@gmail.com	Diego 	Javier  Cucaresse	2017-12-22 11:14:00	Maria	Escritor
NEUSI79@YAHOO.ES	NEUS	.. 	2017-12-22 11:22:00	Maria	Escritor
estelamoreno@us.es	Estela	.. 	2017-12-22 11:31:00	Maria	Escritor
elechon@reicaz.com	Elena 	Lechon Fleta	2017-12-22 15:25:00	Maria	Escritor
irroca.160@gmail.com	Isidoro 	Irroca 	2017-12-22 15:35:00	Maria	Escritor
magalbo@live.com	Colin 	Renou 	2017-12-27 15:31:00	Maria	Escritor
decaro0945@hotmail.com	David Jacinto 	Gutierrez  Caro	2017-12-27 15:47:00	Maria	Escritor
briandaetxea@hotmail.com	Dory 	Lansorena 	2017-12-27 18:04:00	Maria	Escritor
garcia6196@gmail.com	Lara 	García  Pastor	2017-12-29 13:25:00	Maria	Escritor
rbarbaropaula@gmail.com	Paula 	Barbaro 	2017-12-29 14:57:00	Maria	Escritor
patriciamerchan@edu.xunta.es	PATRICIA	MERCHÁN RÍO	2017-12-29 15:30:00	Maria	Escritor
Montsesouto25@gmail.com	Montse	Souto 	2018-01-02 10:40:00	Maria	Escritor
pontevedra57@gmail.com	Manuel 	Oubiña  Losada	2018-01-03 13:20:00	Maria	Escritor
rachel.casasus@gmail.com	Raquel 	LÓPEZ  CASASÚS	2018-01-04 19:31:00	Maria	Escritor
lidia-buffy@hotmail.com	Lidia 	Blanco  Blanco	2018-01-04 19:33:00	Maria	Escritor
aidapaziglesias@gmail.com	AIDA 	PAZ 	2018-01-08 10:47:00	Maria	Escritor
antonioboschconde@gmail.com	Antonio 	Bosch  Conde	2018-01-08 18:45:00	Maria	Escritor
lgarcia6196@gmail.com	Lara 	.. 	2018-01-09 17:18:00	Maria	Escritor
info@rafaeljurado.es	Rafa	Jurado 	2018-01-10 15:23:00	Maria	Escritor
tabitaperalta@gmail.com	Tabita 	Peralta 	2018-01-10 18:36:00	Maria	Escritor
guillermocastilloleon@hotmail.com	Guillermo 	Castillo  León	2018-01-10 20:24:00	Maria	Escritor
sesmachus@hotmail.com	Mª Jesús 	Sesma 	2018-01-10 20:30:00	Maria	Escritor
malgomrod@hotmail.es	Alejandra	Gómez 	2018-01-12 13:29:00	Maria	Escritor
noelia4trigo@gmail.com	Noelia 	Trigo  Martinez 	2018-01-12 15:09:00	Maria	Escritor
carlakacic@yahoo.com.ar	Carla 	Kacic 	2018-01-16 10:56:00	Maria	Escritor
tebatan@gmail.com	María Carolina 	Fernández  Arriaga	2018-01-16 14:20:00	Maria	Escritor
marcelomontero779@gmail.com	Marcelo 	Montero 	2018-01-17 12:34:00	Maria	Escritor
m.rodenasvargas@gmail.com	MIRIAM	.. 	2018-01-17 12:43:00	Maria	Escritor
rcigarruista@gmail.com	RODRIGO F.	CIGARRUISTA 	2018-01-17 12:47:00	Maria	Escritor
mildrehernandez@gmail.com	Mildre	Hernandez  Barrios	2018-01-17 13:16:00	Maria	Escritor
blanca.richi@yahoo.com	Blanca 	Richi  Zavala	2018-01-17 13:19:00	Maria	Escritor
helena-cs@hotmail.com	Elena 	.. 	2018-01-17 13:42:00	Maria	Escritor
antonio.gzlz.grc@gmail.com	Antonio 	González  García	2018-01-17 13:55:00	Maria	Escritor
marajabu@hotmail.com	Juan Jorge	.. 	2018-01-17 13:58:00	Maria	Escritor
conalguien@msn.com	Elisabet 	CO 	2018-01-17 15:15:00	Maria	Escritor
mariocuestahernando@gmail.com	Mario 	Cuesta 	2018-01-17 15:20:00	Maria	Escritor
indampopa@yahoo.es	Cristina 	Menendez  	2018-01-17 18:34:00	Maria	Escritor
rpicazog@gmail.com	Ricardo 	Picazo 	2018-01-18 14:06:00	Maria	Escritor
iraiderr@gmail.com	Iraide 	Roldan  Retolaza	2018-01-22 15:32:00	Maria	Escritor
sigridamoros@gmail.com	Sigrid 	Amorós  Martínez	2018-01-22 19:14:00	Maria	Escritor
mgloriagamarra@gmail.com	GLORIA	GAMARRA 	2018-01-22 19:48:00	Maria	Escritor
susijerezortega@gmail.com	Susana 	Jerez  Ortega	2018-01-22 19:53:00	Maria	Escritor
marcsansrius@hotmail.com	Marc 	Sans 	2018-01-23 14:30:00	Maria	Escritor
carolyngilchrist@gmail.com	Carolyn 	Gilchrist 	2018-01-23 14:33:00	Maria	Escritor
elenruiz.lopez@gmail.com	Elena	Ruiz  López	2018-01-23 17:59:00	Maria	Escritor
claudiavelez35@yahoo.com	Claudia	Velez 	2018-01-23 19:28:00	Maria	Escritor
tbrandt@movemarketing.es	Tanja 	Brandt 	2018-01-24 13:48:00	Maria	Escritor
VUELVEAROMA@HOTMAIL.COM	GLORIA 	GAMARRA 	2018-01-24 18:04:00	Maria	Escritor
proyecto.datap@gmail.com	Aroa 	Ramos 	2018-01-25 19:25:00	Maria	Escritor
amparo.hellin@gmail.com	Amparo 	Sánchez  	2018-01-26 11:08:00	Maria	Escritor
labiona01@hotmail.com	Reyes	Barrero  Herro	2018-01-26 12:05:00	Maria	Escritor
novalnueva@gmail.com	Manuel 	Noval  Moro	2018-01-26 13:28:00	Maria	Escritor
smj270288@gmail.com	Sheila	.. 	2018-01-30 13:35:00	Maria	Escritor
cristinson1975@yahoo.es	Cristina	Daniela  Totelecan	2018-01-30 15:14:00	Maria	Escritor
santaruthi@yahoo.es	Ruth 	Santa  María	2018-01-31 15:05:00	Maria	Escritor
aldomo64@yahoo.es	Doris 	Mosquera  	2018-01-31 17:59:00	Maria	Escritor
beticatravel@gmail.com	Ignacio 	Aguado Fernández	2018-02-01 10:20:00	Maria	Escritor
alynunez@gmail.com	Alicia	Núñez 	2018-02-01 13:35:00	Maria	Escritor
iaymarevl@gmail.com	ADRIANA 	VERDEGUER LAJARA	2018-02-01 17:23:00	Maria	Escritor
joancampstejero@hotmail.com	Juan 	Camps  Tejero	2018-02-01 17:33:00	Maria	Escritor
sanchez.rebollo@hotmail.com	Paula 	Sánchez Rebollo	2018-02-01 17:37:00	Maria	Escritor
johanne2369@gmail.com	Johanne 	Jacome  Hassidoff	2018-02-01 19:23:00	Maria	Escritor
silponscaldero@gmail.com	Sílvia	Pons 	2018-02-02 13:02:00	Maria	Escritor
fernandezsecadas@hotmail.com	Paula 	Fernández  Secadas	2018-02-02 14:24:00	Maria	Escritor
coordinacion@fundacionimo.org	Irene	.. 	2018-02-02 15:09:00	Maria	Escritor
avilacinthia.psp@gmail.com	Cinthia 	Avila 	2018-02-02 15:12:00	Maria	Escritor
delgadocarrilero@gmail.com	David	.. 	2018-02-05 19:49:00	Maria	Escritor
antoniomartinezpolo51@gmail.com	Antonio 	Martinez Polo	2018-02-05 19:59:00	Maria	Escritor
infocentreanima@gmail.com	Raquel 	Porcel  Gordon	2018-02-06 14:42:00	Maria	Escritor
suescunjc@gmail.com	Juan Carlos 	Suescun 	2018-02-06 15:05:00	Maria	Escritor
noemisinacento@gmail.com	Noemi 	García 	2018-02-06 15:29:00	Maria	Escritor
c.exposito.escalona@gmail.com	Cristina 	Expósito  Escalona	2018-02-07 14:55:00	Maria	Escritor
familiacabellojimenez@gmail.com	Manuel	Cabello Galvan	2018-02-08 17:39:00	Maria	Escritor
chamaidafajardo@gmail.com	Chamaida 	Fajardo  Morales	2018-02-08 17:42:00	Maria	Escritor
cristinadespacho@yahoo.es	Cristina 	Rodriguez De Oña	2018-02-09 15:27:00	Maria	Escritor
iraidacarolina@yahoo.com	Iraida 	Carolina  Mejia 	2018-02-12 13:19:00	Maria	Escritor
paulaforconi@gmail.com	Paula	.. 	2018-02-12 17:28:00	Maria	Escritor
cana_ria08@hotmail.com	Isabel 	Díaz 	2018-02-12 17:33:00	Maria	Escritor
maluischacon@hotmail.com	Luis 	Chacon 	2018-02-13 14:18:00	Maria	Escritor
noemibartolomeariza@gmail.com	Noemi 	Bartolome 	2018-02-14 14:57:00	Maria	Escritor
silviacamposvidal@gmail.com	Silvia 	Campos 	2018-02-14 20:05:00	Maria	Escritor
marietaleylo127@gmail.com	María	.. 	2018-02-15 19:54:00	Maria	Escritor
ceititirimundi@hotmail.com	Javier 	Ávila 	2018-02-15 20:00:00	Maria	Escritor
unviajemagico2017@gmail.com	Esmeralda 	Muriel 	2018-02-16 11:06:00	Maria	Escritor
bermudezcecilia@hotmail.com	Cecilia 	Von Bonin 	2018-02-16 11:13:00	Maria	Escritor
pablowaisman@hotmail.com	Pablo 	Waisman 	2018-02-20 13:56:00	Maria	Escritor
kupsa03@kupsa.es	Carlos 	López Carlos López Pérez	2018-02-20 18:47:00	Maria	Escritor
info@thenomadicdreamer.com	Cesar	Mercadal 	2018-02-21 11:39:00	Maria	Escritor
yolandagreus10@live.com	Yolanda 	Greus  Calabuig	2018-02-21 12:10:00	Maria	Escritor
nayraesteban@gmail.com	Naira 	Esteban  De Las Heras	2018-02-21 13:29:00	Maria	Escritor
mariasuch.dn@gmail.com	Maria 	Such 	2018-02-21 13:44:00	Maria	Escritor
rakylee@hotmail.com	Raquel	.. 	2018-02-22 11:39:00	Maria	Escritor
anarojovillar@icloud.com	Ana 	Rojo  	2018-02-22 14:26:00	Maria	Escritor
beatriziabustamante@gmail.com	Beatriz 	Iáñez  Bustamante	2018-02-23 10:29:00	Maria	Escritor
esteffi75@gmail.com	Estefanía 	Esteban 	2018-02-23 11:46:00	Maria	Escritor
haydeeramoscadena80@gmail.com	Haydee 	Ramos  	2018-02-23 11:49:00	Maria	Escritor
mcgonzalez66@gmail.com	Mari Carmen 	González 	2018-02-23 11:53:00	Maria	Escritor
carminavidal36@hotmail.com	Carmina 	Vidal 	2018-02-26 15:21:00	Maria	Escritor
esther_md19@hotmail.com	Esther 	Vallejo  Castro	2018-02-26 15:26:00	Maria	Escritor
lauramarinruiz@hotmail.com	Laura 	Marín  Ruiz	2018-02-27 19:23:00	Maria	Escritor
cristinapsolano@gmail.com	Cristina 	Prieto 	2018-03-01 14:14:00	Maria	Escritor
aliciaborras@gmail.com	Alicia 	Borras  	2018-03-01 14:25:00	Maria	Escritor
elisabetsc@hotmail.com	Elisabet 	Serrano 	2018-03-01 14:49:00	Maria	Escritor
vanesadiez10@yahoo.es	Vanesa 	Díez  Rodriguez	2018-03-02 14:44:00	Maria	Escritor
viuflau@gmail.com	Laura 	Pérez 	2018-03-02 14:55:00	Maria	Escritor
angela.arganda@gmail.com	Ángela 	Arganda 	2018-03-02 15:57:00	Maria	Escritor
juancarlos@felicacia.com	Juan Carlos 	Maestro Arcos	2018-03-05 12:44:00	Maria	Escritor
nenamelenas@hotmail.es	Josefina 	Aguilo  Garrido	2018-03-05 14:58:00	Maria	Escritor
amega4765@hotmail.com	Ana 	Mejias  Galvez	2018-03-05 17:46:00	Maria	Escritor
virginia.sanzmoreno@gmail.com	Virginia 	Sanz  Moreno	2018-03-05 18:22:00	Maria	Escritor
sonialgui78@gmail.com	Sonia 	Alonso Guinaldo	2018-03-05 18:24:00	Maria	Escritor
maria.galian@gmail.com	Maria 	Galian 	2018-03-05 18:48:00	Maria	Escritor
vitsifu@gmail.com	Gerardo	Moreno  	2018-03-06 12:38:00	Maria	Escritor
maria.aguilardomingo@unibo.it	Sol 	Aguilar Domingo	2018-03-06 12:48:00	Maria	Escritor
bereaana@yahoo.com	Ana Paula 	Berea  Novoa	2018-03-07 11:29:00	Maria	Escritor
mariropero74@gmail.com	Mari 	Ropero 	2018-03-07 13:01:00	Maria	Escritor
anabelendaganzo22@hotmail.com	Ana Belén 	Yebra 	2018-03-07 13:04:00	Maria	Escritor
inversionescl@gmail.com	Tomas 	Cabrera 	2018-03-07 13:34:00	Maria	Escritor
castelloliana@gmail.com	Liana 	Castello 	2018-03-07 19:42:00	Maria	Escritor
sonfli@hotmail.com	Sonia 	Sanabria  Alvarado	2018-03-08 13:57:00	Maria	Escritor
smabel_rh@hotmail.com	Selene Mabel 	Ruiz  Cardiel	2018-03-08 14:20:00	Maria	Escritor
anarodmon@gmail.com	Ana 	Rodríguez  Monge	2018-03-08 14:41:00	Maria	Escritor
meryjo_2junio@hotmail.com	Maria Jose 	Rueda 	2018-03-08 19:18:00	Maria	Escritor
tousa81@hotmail.com	ELISABETH 	HERRERA 	2018-03-09 14:53:00	Maria	Escritor
mariacristina.hoyos@salud.madrid.org	Maria Cristina	De Hoyos  López	2018-03-12 13:58:00	Maria	Escritor
juanconcejal@live.com	Juan Clemente 	Gómez  García	2018-03-12 14:28:00	Maria	Escritor
marcos.thiago.silva@gmail.com	Thiago 	Marcos Da Silva	2018-03-13 10:13:00	Maria	Escritor
bvl10@hotmail.com	Blas 	Vicente 	2018-03-13 13:12:00	Maria	Escritor
teles-bea@live.com	Beatriz 	Obrados 	2018-03-13 19:47:00	Maria	Escritor
titiunquera@hotmail.com	Manuel Ángel 	Ortega 	2018-03-14 15:21:00	Maria	Escritor
eva.fuentes.usea@gmail.com	Eva 	Fuentes 	2018-03-15 14:01:00	Maria	Escritor
trinky_f@hotmail.com	Fernando 	Calvo  Trincado	2018-03-19 14:29:00	Maria	Escritor
barbara.vespa@gmail.com	Bárbara	Vespa 	2018-03-19 18:58:00	Maria	Escritor
esdudel@yahoo.es	Esperanza	- 	2018-03-20 19:49:00	Maria	Escritor
bell.reyes73@gmail.com	Belkis	Reyes Soto	2018-03-21 19:31:00	Maria	Escritor
martinc.amoros@gmail.com	Martin 	Campos 	2018-03-22 12:26:00	Maria	Escritor
lortegavesga@gmail.com	Laura 	Ortega 	2018-03-22 12:49:00	Maria	Escritor
corbellota@hotmail.com	MARISA	LATORRE 	2018-03-23 15:10:00	Maria	Escritor
Maite.c.herrero@gmail.com	Maite 	Camacho  Herrero	2018-03-23 15:15:00	Maria	Escritor
paulaborrasruiz@yahoo.es	Paula 	Borrás Ruiz	2018-03-23 15:52:00	Maria	Escritor
lorenapaguirre@gmail.com	Lorena P	Aguirre 	2018-03-26 11:56:00	Maria	Escritor
lectivicia@hotmail.com	Robin 	Swan 	2018-03-26 12:00:00	Maria	Escritor
ccanocab@xtec.cat	Carolina 	Cano-Caballero  Romero	2018-03-26 12:09:00	Maria	Escritor
zreikanthony@hotmail.com	Anthony 	Zreik 	2018-03-26 12:12:00	Maria	Escritor
psanchezveronica@gmail.com	Verónica 	Parro 	2018-03-27 10:16:00	Maria	Escritor
kar.esparza.lop@hotmail.com	Magda 	Karina  	2018-03-27 14:47:00	Maria	Escritor
jrdecea@hotmail.com	José Ramón	.. 	2018-04-03 09:29:00	Maria	Escritor
vegacala@hotmail.com	Virginia	De La Calle 	2018-04-03 09:35:00	Maria	Escritor
monicaog31@hotmail.com	Monica 	Ospino 	2018-04-03 09:43:00	Maria	Escritor
beatrizberrocal12@gmail.com	Beatriz 	Berrocal  Pérez	2018-04-03 10:45:00	Maria	Escritor
leirert@hotmail.com	LEIRE 	RUBIO 	2018-04-03 14:19:00	Maria	Escritor
veronica.paradinas.duro@gmail.com	Verónica 	Paradinas Duro	2018-04-03 16:30:00	Maria	Escritor
sandra.gonzalez.b.94@gmail.com	Sandra	Gonzalez 	2018-04-03 16:38:00	Maria	Escritor
djimenezlopez@educa.madrid.org	David 	Jiménez  López	2018-04-03 17:37:00	Maria	Escritor
silvia@mutkids.es	Silvia	.. 	2018-04-04 13:00:00	Maria	Escritor
lydia_luque@yahoo.es	Lydia 	Luque 	2018-04-05 09:35:00	Maria	Escritor
bmoriana@hotmail.com	Begoña 	Moriana 	2018-04-05 10:27:00	Maria	Escritor
essanmu@gmail.com	ESTHER 	SÁNCHEZ 	2018-04-05 10:29:00	Maria	Escritor
solarediparna@gmail.com	Jessica H. 	Llamas 	2018-04-05 11:38:00	Maria	Escritor
ki_z_m@hotmail.com	Karla 	Ivonne  Zenteno	2018-04-05 11:41:00	Maria	Escritor
aldebarantauro@gmail.com	José Gregorio 	González  Márquez	2018-04-09 10:51:00	Maria	Escritor
dabenedicte@facebook.com	David 	Benedicte  Escudero	2018-04-09 11:05:00	Maria	Escritor
silviadolz@hotmail.com	Silvia 	Dolz  Dominguez	2018-04-09 13:33:00	Maria	Escritor
paulifersa@gmail.com	Paula 	Fernández 	2018-04-10 10:36:00	Maria	Escritor
melailustradora@gmail.com	CARMEN	.. 	2018-04-10 14:17:00	Maria	Escritor
sylvain_pernet@yahoo.fr	Sylvain 	Pernet 	2018-04-10 17:15:00	Maria	Escritor
annaseguranyes7@yahoo.es	Anna 	Seguranyes 	2018-04-10 17:48:00	Maria	Escritor
araceli.rodrigo.chacon@gmail.com	Araceli 	Chacon 	2018-04-10 18:28:00	Maria	Escritor
quecuatsa@gmail.com	Annamaría 	Pandza 	2018-04-10 18:37:00	Maria	Escritor
gpuertas@live.com	Gema	Puertas 	2018-04-10 18:39:00	Maria	Escritor
eearwen@gmail.com	Marta 	Gaitero 	2018-04-11 10:19:00	Maria	Escritor
jalvaredo@gmail.com	Javier 	Alvaredo  Castro	2018-04-11 10:25:00	Maria	Escritor
ideaasia@gmail.com	Abi	Lindsay  Clark	2018-04-11 13:44:00	Maria	Escritor
mgl.escritura@gmail.com	Gabriela	Lovera 	2018-04-11 13:47:00	Maria	Escritor
esthercita.esther@gmail.com	Esther	.. 	2018-04-11 14:22:00	Maria	Escritor
f_palazuelos@yahoo.es	Fernando 	Palazuelos  	2018-04-11 18:49:00	Maria	Escritor
artobreg@gmail.com	Arturo 	Moreno  Obregón	2018-04-12 11:29:00	Maria	Escritor
ibeltranj83@gmail.com	Irene 	Beltrán 	2018-04-12 13:03:00	Maria	Escritor
lafuncionariaasesina@gmail.com	Mari Carmen 	Castillo  Peñarocha	2018-04-12 13:21:00	Maria	Escritor
alayrach69@hotmail.com	Miguel 	Alayrach  Martínez	2018-04-12 13:28:00	Maria	Escritor
yaels@vera.com.uy	Yael 	Szajnholc  	2018-04-12 16:53:00	Maria	Escritor
dieg_ba@hotmail.com	Diego 	Barreto 	2018-04-16 15:10:00	Maria	Escritor
claudia.ferosa@gmail.com	Claudia Ferosa	Claudia Ferosa 	2018-04-16 15:12:00	Maria	Escritor
bajistapopero@hotmail.com	Manuel 	Gordillo  Torres	2018-04-17 13:11:00	Maria	Escritor
ronlemley@hotmail.com	Ronald	William Lemley	2018-04-17 14:57:00	Maria	Escritor
augustoruedagonzalez@gmail.com	Augusto	Rueda González 	2018-04-17 15:07:00	Maria	Escritor
victorja.navarro@yahoo.es	Victor	Navarro 	2018-04-17 15:10:00	Maria	Escritor
mariapilar.hp@gmail.com	Pilar 	Hernández Plaza	2018-04-18 12:00:00	Maria	Escritor
virgycll@hotmail.com	Virginia 	Carrasco  Llamas	2018-04-19 13:21:00	Maria	Escritor
glopaz7@gmail.com	Gloria 	Fernández  Paz	2018-04-19 13:26:00	Maria	Escritor
javi4000@gmail.com	Javier 	Albalat  Vicente	2018-04-19 14:29:00	Maria	Escritor
mrhr.laura@gmail.com	Laura	Mora  Hernani	2018-04-19 14:31:00	Maria	Escritor
nohemihdzc@yahoo.com.mx	Nohemi 	Hernandez  Carrillo	2018-04-19 14:33:00	Maria	Escritor
sandraaliasperez@icloud.com	Sandra 	Alías  Pérez	2018-04-23 09:52:00	Maria	Escritor
pedroencinascerezo@gmail.com	Pedro 	Encinas  Cerezo	2018-04-23 10:07:00	Maria	Escritor
ignasigarciab@gmail.com	IGNASI 	GARCIA  BARBA	2018-04-23 16:16:00	Maria	Escritor
macotera1952@gmail.com	Antonio	Blázquez Madrid	2018-04-23 17:27:00	Paz	Escritor
cambioderumboya@gmail.com	Vanesa 	Nogales 	2018-04-23 18:35:00	Maria	Escritor
noeliamainer@gmail.com	Noelia 	Mainer  Casanova	2018-04-23 18:39:00	Maria	Escritor
ticoandus@gmail.com	Silvia	.. 	2018-04-24 10:09:00	Maria	Escritor
beralva@telefonica.net	Beralva 	Beralva 	2018-04-24 13:18:00	Maria	Escritor
susanazapata28@gmail.com	Susana 	Zapata 	2018-04-24 13:22:00	Maria	Escritor
ainhoaberistain@gmail.com	Ainhoa 	Beristain 	2018-04-24 13:28:00	Maria	Escritor
vanessa@vanessadelpino.com	Vanessa	Delpino 	2018-04-24 13:47:00	Maria	Escritor
rafaalej@hotmail.com	Rafa	Alé  	2018-04-24 14:03:00	Maria	Escritor
urreaeduardo@hotmail.com	Eduardo 	Urrea 	2018-04-24 14:09:00	Maria	Escritor
caren_ca@hotmail.com	CAREN 	IZQUIERDO 	2018-04-25 12:42:00	Maria	Escritor
martingales.martingales@gmail.com	Mar 	Olivé  Nieto	2018-04-25 13:15:00	Maria	Escritor
abpliego@gmail.com	Ana	.. 	2018-04-25 13:16:00	Maria	Escritor
anabichologa@gmail.com	Ana 	Villegas  Ramírez	2018-04-25 13:56:00	Maria	Escritor
beltran.s.clara@gmail.com	Clara 	Beltrán  Sánchez	2018-04-25 14:21:00	Maria	Escritor
yolandatowandacuentos@gmail.com	Yolanda 	Towanda 	2018-04-26 11:43:00	Maria	Escritor
mc_ribes@hotmail.com	Carmen 	Ribes  Balfagó	2018-04-26 11:49:00	Maria	Escritor
germanchavezr@gmail.com	Germán	Chávez 	2018-04-26 12:57:00	Maria	Escritor
memetristan@hotmail.com	Mercedes	Tristan Miranda	2018-04-26 14:25:00	Maria	Escritor
olobat@gmail.com	Oscar	Lobaton 	2018-04-26 14:30:00	Maria	Escritor
victor.ruiz596@gmail.com	Victor 	RUIZ  MUÑOZ	2018-04-26 16:20:00	Maria	Escritor
mariabadra@hotmail.com	Maria 	Badenes  Ramón	2018-04-26 16:46:00	Maria	Escritor
paulapitarch.illustra@gmail.com	Paula 	Pitarch Romero	2018-04-26 17:40:00	Maria	Escritor
favialsan31@hotmail.com	Fuencisla	Avial 	2018-04-27 10:36:00	Maria	Escritor
karo.valderrama8@gmail.com	Carolina	Valderrama 	2018-04-27 11:05:00	Maria	Escritor
jcamaraju@gmail.com	JUSTO	CÁMARA JUARROS	2018-04-30 13:19:00	Maria	Escritor
jackbabiloni@jackbabiloni.es	Jack 	Babiloni 	2018-04-30 16:20:00	Maria	Escritor
ign.abella@gmail.com	Ignacio	Abella  Mina	2018-04-30 16:30:00	Maria	Escritor
hadamapy@gmail.com	Pilar	Román 	2018-05-02 10:29:00	Maria	Escritor
jcrlupi@hotmail.es	María Del Pilar	López 	2018-05-02 17:19:00	Maria	Escritor
nmoralesrdz@gmail.com	Ninfa Alicia 	Morales 	2018-05-02 18:32:00	Maria	Escritor
muzrodri@gmail.com	Elena 	Muñoz  Millán	2018-05-03 10:51:00	Maria	Escritor
ipardocaballero@gmail.com	Inmaculada 	Pardo  Caballero	2018-05-03 10:54:00	Maria	Escritor
sandraalias@hotmail.com	Sandra 	Alías  Pérez	2018-05-03 12:41:00	Maria	Escritor
adanylb@yahoo.com	Adanyl 	Brignoni  Sanfeliz	2018-05-03 16:43:00	Maria	Escritor
steruel1964@gmail.com	Silvia	Teruel 	2018-05-03 17:35:00	Maria	Escritor
palovc@hotmail.com	Paloma	Valle 	2018-05-03 18:52:00	Maria	Escritor
tfons@xtec.cat	Teresa	.. 	2018-05-04 13:24:00	Maria	Escritor
rmolerofeeme@gmail.com	Roberto 	Molero  Romero	2018-05-04 14:03:00	Maria	Escritor
marina_sgb@hotmail.com	Marina	Izquierdo  Rodrigo	2018-05-07 10:17:00	Maria	Escritor
okosketch@gmail.com	Nicole	Domínguez 	2018-05-07 17:46:00	Maria	Escritor
lau.cano@yahoo.com.mx	Laura 	Cano 	2018-05-08 17:10:00	Maria	Escritor
capuchina_95@hotmail.com	Raquel	Jimenez 	2018-05-08 17:41:00	Maria	Escritor
algare88@gmail.com	Elisa 	Alcalá  García	2018-05-08 17:44:00	Maria	Escritor
soy1alg@hotmail.com	ANA 	MORATA 	2018-05-08 18:33:00	Maria	Escritor
telegrafy@gmail.com	Luis 	Goldberg 	2018-05-08 18:39:00	Maria	Escritor
laurasuarezlorca@hotmail.com	Laura 	Suárez  Lorca	2018-05-09 10:55:00	Maria	Escritor
amondarainrmt@hotmail.com	TERESA 	AMONDARAIN  RAMOS	2018-05-09 11:30:00	Maria	Escritor
tstxtz@hotmail.com	Samuel	.. 	2018-05-09 11:35:00	Maria	Escritor
sandra-montes-troyano@hotmail.com	Sandra	Montes Troyano	2018-05-09 13:36:00	Maria	Escritor
nedel.aguila@nauta.cu	Nedel 	Aguila  Gimeranez	2018-05-09 13:51:00	Maria	Escritor
almudenamariapuebla@hotmail.com	Almudena Maria	Puebla 	2018-05-09 13:55:00	Maria	Escritor
nataliaromogonzalez@gmail.com	Natalia 	Romo  González	2018-05-09 16:32:00	Maria	Escritor
cuentik@cuentik.com	Hermanos Reina	.. 	2018-05-09 16:44:00	Maria	Escritor
bhrm988@hotmail.com	Beatriz	.. 	2018-05-09 17:15:00	Maria	Escritor
iluame@live.com	Maite	. 	2018-05-10 16:45:00	Maria	Escritor
xaviereguiguren@hotmail.es	Xavier	Eguiguren 	2018-05-10 17:13:00	Maria	Escritor
mimundoconellostres@gmail.com	Libertad	Freire Rodríguez	2018-05-11 13:05:00	Maria	Escritor
georginaponceblascol@gmail.com	Georgina	Ponce 	2018-05-11 13:10:00	Maria	Escritor
jmocenterprise@gmail.com	Oriol	Corcoll 	2018-05-11 14:42:00	Maria	Escritor
gnesisvent@gmail.com	Genesis	Ventura 	2018-05-14 11:15:00	Maria	Escritor
muriasls@hotmail.com	Sofia	Murias López	2018-05-14 11:17:00	Maria	Escritor
ea.asignatura@gmail.com	Eva María	. 	2018-05-14 11:40:00	Maria	Escritor
fgarza@salud.aragon.es	Fernando	Garza Benito	2018-05-14 12:43:00	Maria	Escritor
irene.andivia@yahoo.es	Irene	Andivia Reyes	2018-05-14 14:04:00	Maria	Escritor
inma.p_r@hotmail.com	Irene	Pérez Rodríguez	2018-05-15 11:01:00	Maria	Escritor
rociofluc@gmail.com	Rocío	Fernández Lucena	2018-05-16 11:15:00	Maria	Escritor
yorma_ulloa@hotmail.com	Yorma	Ulloa 	2018-05-16 13:24:00	Maria	Escritor
dani.ortiguera@gmail.com	Daniel	García Rodríguez	2018-05-16 18:18:00	Maria	Escritor
luzmariamarte44@gmail.com	Luz María	Marte 	2018-05-17 09:02:00	Maria	Escritor
olile2003@gmail.com	Lola	. 	2018-05-17 16:30:00	Maria	Escritor
Leticiaernst94@gmail.com	Letecia	Martin Ernst	2018-05-17 16:35:00	Maria	Escritor
iulyaiulya14@gmail.com	Liana	Evdochimov 	2018-05-17 18:25:00	Maria	Escritor
jcarlosmestre@gmail.com	Juan Carlos	Maestre 	2018-05-18 09:02:00	Maria	Escritor
monicaestradaroyuela@gmail.com	Mónica	Estrada 	2018-05-18 10:13:00	Maria	Escritor
jimenasanhueza@hotmail.com	Jimena	Sanhueza Martínez	2018-05-18 11:28:00	Maria	Escritor
larajuansilvia@gmail.com	Silvia	Lara Juan	2018-05-18 12:03:00	Maria	Escritor
infantildana@gmail.com	Mari Carmen	Seoane Alonso-Ablanedo	2018-05-21 11:50:00	Maria	Escritor
carpediem7veces@hotmail.com	Elena	González Sanz	2018-05-21 11:55:00	Maria	Escritor
ferry.el8@gmail.com	Ferran	Jurado Valderrama	2018-05-21 12:23:00	Maria	Escritor
willyyeva5@gmail.com	Eva 	López 	2018-05-23 09:44:00	Maria	Escritor
alxfernandezm@gmail.com	Alejandra	Fernández 	2018-05-23 09:51:00	Maria	Escritor
albertopiru@yahoo.es	Alberto	Piñeiro 	2018-05-23 11:33:00	Maria	Escritor
patriciaejesus2015@gmail.com	Patricia	. 	2018-05-23 16:22:00	Maria	Escritor
susana@srueda.com	Susana	Rueda 	2018-05-23 17:43:00	Maria	Escritor
jmdefrutos@hsjd.es	Jose María	Bermejo De Frutos	2018-05-23 18:15:00	Maria	Escritor
pujolantonia8@gmail.com	Antonia	Pujol Miralles	2018-05-24 11:19:00	Maria	Escritor
dapibooks@gmail.com	Valentino 	Martin  Davio	2018-05-25 11:39:00	Maria	Escritor
mons811@hotmail.com	Montse 	Ponte 	2018-05-28 18:59:00	Maria	Escritor
martacalles_1990@hotmail.es	Marta 	Cardona  Ballester	2018-05-29 09:58:00	Maria	Escritor
eliafdez@hotmail.es	Elia 	Fernandez 	2018-05-29 11:28:00	Maria	Escritor
raquelcarpes@gmail.com	Raquel 	Carpes  Garrido	2018-05-29 12:32:00	Maria	Escritor
mistesoros19@hotmail.com	Graciela 	Berlanga  Valdés	2018-05-29 17:43:00	Maria	Escritor
isabelagueraes@gmail.com	Isabel 	Agüera  Espejo-Saavedra	2018-05-30 13:19:00	Maria	Escritor
Rebemartinz@gmail.com	Rebeca	.. 	2018-05-30 13:24:00	Maria	Escritor
eugemiqueo@gmail.com	María Eugenia 	Miqueo 	2018-05-30 18:27:00	Maria	Escritor
gcasmir5@hotmail.com	Gregoria	Casas 	2018-05-30 18:37:00	Maria	Escritor
contact@ecodecoco.com	Gabriela 	Valle 	2018-06-04 14:13:00	Maria	Escritor
Carlos.Gait@finandino.com.ar	Carlos	Gait 	2018-06-04 14:16:00	Maria	Escritor
martagomezbu@gmail.com	Marta 	Gomez 	2018-06-04 14:33:00	Maria	Escritor
haciendoways@yahoo.com	Ari 	Pinelli 	2018-06-04 17:05:00	Maria	Escritor
josesaezolmos@gmail.com	José	Sáez 	2018-06-04 18:05:00	Maria	Escritor
helenasomon@gmail.com	Helena 	Soler  Montero	2018-06-05 12:50:00	Maria	Escritor
msg1988abril@gmail.com	María 	SánzG 	2018-06-05 13:27:00	Maria	Escritor
cruzcolm@yahoo.com	Cruz 	Colmenares 	2018-06-05 13:29:00	Maria	Escritor
alquimistadeacero6@gmail.com	 Ana 	Zepeda 	2018-06-05 14:30:00	Maria	Escritor
fsanchezencasa@gmail.com	Francisco José 	Sanchez Juan	2018-06-06 13:38:00	Maria	Escritor
m.quintanasilva@hotmail.com	María 	Quintana  Silva	2018-06-07 10:50:00	Maria	Escritor
eryka_navega@hotmail.com	Erika	Navarro Vega	2018-06-07 11:43:00	Maria	Escritor
esthersomalo@hotmail.com	Esther 	Somalo  Blanco	2018-06-07 13:23:00	Maria	Escritor
berges77@yahoo.es	PAULA 	LÓPEZ-BERGES 	2018-06-07 13:27:00	Maria	Escritor
nuriselv@gmail.com	Núria	.. 	2018-06-08 12:31:00	Maria	Escritor
nkogomemba1994@gmail.com	SALVADOR 	NKOGO MEMBA.	2018-06-11 09:21:00	Maria	Escritor
nayatkaidferron@me.com	Nayat 	Kaid  	2018-06-11 09:59:00	Maria	Escritor
pedro53luis@gemail.com	Pedro Luis	Vazquez Garcia	2018-06-11 11:57:00	Maria	Escritor
jocelyner1@outlook.es	Jocelyne 	Rodriguez 	2018-06-11 12:27:00	Maria	Escritor
ariaznabea@live.com	Beatriz	Alcazar Gonzalez	2018-06-11 14:23:00	Maria	Escritor
paula.olmedo88@gmail.com	Paula	Olmedo 	2018-06-11 17:30:00	Maria	Escritor
isabel.lopsor@gmail.com	Isabel	Lopez Soriano	2018-06-11 17:51:00	Maria	Escritor
juan_alcudia@yahoo.es	Juan 	Alcudia 	2018-06-12 16:41:00	Maria	Escritor
vegalur@hotmail.com	Lourdes 	KDOS 	2018-06-12 18:58:00	Maria	Escritor
info@lunasanjuan.com	Luna 	San Juan 	2018-06-13 12:34:00	Maria	Escritor
kathrynsescribano@gmail.com	Kathryn Susana 	Escribano  Hawken	2018-06-13 13:14:00	Maria	Escritor
ibusht@dian.gov.co	INDIRA	.. 	2018-06-14 10:37:00	Maria	Escritor
bvgema@hotmail.com	 Gema 	Bermejo 	2018-06-15 09:26:00	Maria	Escritor
minbark10@gmail.com	MIN JUNG	BARK  JUNG	2018-06-18 14:25:00	Maria	Escritor
paula.casal@upf.edu	Paula 	Casal 	2018-06-18 14:28:00	Maria	Escritor
juliamaria1@yahoo.com	Julia Maria 	Fernandez 	2018-06-19 12:29:00	Maria	Escritor
mcnavarroruiz@hotmail.com	Maria Del Carmen 	Navarro  Ruiz	2018-06-19 12:57:00	Maria	Escritor
saramorarte@gmail.com	Sara 	Arís  Moreno	2018-06-19 14:10:00	Maria	Escritor
melgreenhb@gmail.com	Melisa	Holmgren 	2018-06-19 18:09:00	Maria	Escritor
nagozubi@gmail.com	Nagore	Zubillaga 	2018-06-20 14:05:00	Maria	Escritor
angeles@penteofilms.es	María De Los Ángeles 	García 	2018-06-20 14:16:00	Maria	Escritor
estherlimones@gmail.com	Esther 	Limones 	2018-06-21 14:11:00	Maria	Escritor
info@jabatostudio.com	Alex 	Pérez 	2018-06-22 10:57:00	Maria	Escritor
albertoperezbolea@hotmail.com	Alberto	.. 	2018-06-22 12:50:00	Maria	Escritor
mariobsdm@gmail.com	Mario	B.S 	2018-06-22 14:28:00	Maria	Escritor
estherpinva@gmail.com	Esther 	Pintado 	2018-06-25 11:07:00	Maria	Escritor
lakiniano@hotmail.es	José 	L.A 	2018-06-26 09:57:00	Maria	Escritor
lauratores@yahoo.com.ar	Laura Marcela 	Tores 	2018-06-26 12:44:00	Maria	Escritor
albadpb08@gmail.com	Little 	Craft  	2018-06-26 13:19:00	Maria	Escritor
ambrosiosanchezr@gmail.com	ROSA 	AMBROSIO  SANCHEZ	2018-06-29 09:53:00	Maria	Escritor
phalma727@gmail.com	PILAR 	ÁLVAREZ  MARIN	2018-06-29 11:17:00	Maria	Escritor
alealejandrafuente@gmail.com	Alejandra 	De La Fuente 	2018-06-29 11:41:00	Maria	Escritor
terecarreteropoblete@gmail.com	Teresa 	Carretero 	2018-06-29 14:43:00	Maria	Escritor
info@miradasonline.com	María Ángeles	Santos 	2018-07-02 13:33:00	Maria	Escritor
mibeldo@hotmail.com	Miriam	.. 	2018-07-02 17:41:00	Maria	Escritor
silvarecuerorobert@gmail.com	Maria De Los Angeles	.. 	2018-07-02 18:19:00	Maria	Escritor
aracelijuan@msn.com	Araceli 	Chiquilla Mesa	2018-07-03 13:44:00	Maria	Escritor
fillafonta@yahoo.es	Jorge Eliécer 	Triviño  Rincón	2018-07-03 13:47:00	Maria	Escritor
yerrieche@yahoo.com.ar	Erica 	Echeveste 	2018-07-03 13:49:00	Maria	Escritor
martinnieto_94@hotmail.com	Martín	.. 	2018-07-04 10:45:00	Maria	Escritor
flamencalle@gmail.com	Amanda 	Espinel  	2018-07-04 17:15:00	Maria	Escritor
marielbillinghurst@yahoo.com.ar	Mariel	.. 	2018-07-04 18:01:00	Maria	Escritor
anadelgadofiguera@hotmail.com	Ana 	Delgado 	2018-07-04 18:06:00	Maria	Escritor
walter.carzon@gmail.com	Walter	Carzon 	2018-07-05 11:43:00	Maria	Escritor
mandreu8@xtec.cat	MARIA CARMEN 	ANDREU  ROSELL	2018-07-05 13:10:00	Maria	Escritor
santiagopulecio01@gmail.com	Santiago	.. 	2018-07-05 16:39:00	Maria	Escritor
marilonorte@gmail.com	Mariló	.. 	2018-07-05 17:04:00	Maria	Escritor
tambre77@gmail.com	Nel 	Escudero 	2018-07-05 17:57:00	Maria	Escritor
brunogvalencia@gmail.com	Bruno	G Valencia	2018-07-06 13:20:00	Maria	Escritor
diana327@hotmail.com	Diana	García Cabezas	2018-07-06 13:31:00	Maria	Escritor
Maria.b_23@hotmail.com	María	Bernabe 	2018-07-09 12:31:00	Maria	Escritor
virtudes.ferrer@aliceadsl.fr	Elizabeth	Garcés 	2018-07-09 12:56:00	Maria	Escritor
madasimbae@gmail.com	Mada 	Simbae 	2018-07-09 13:34:00	Maria	Escritor
zamoranaluis@gmail.com	Ana Isabel 	Luis  Campos	2018-07-09 14:15:00	Maria	Escritor
angelica.ccadavid@gmail.com	Angélica 	Cardozo 	2018-07-09 16:49:00	Maria	Escritor
mpl@mercedesponcedeleon.com	Mercedes 	Ponce  De León	2018-07-09 17:51:00	Maria	Escritor
sarrabal@hotmail.com	Susana 	Arrabal  López	2018-07-10 10:33:00	Maria	Escritor
mariela1967@hotmail.es	Mariela 	Santamarina 	2018-07-10 14:20:00	Maria	Escritor
Rafa.J.Cordero@hotmail.com	Rafa 	Cordero 	2018-07-10 16:43:00	Maria	Escritor
kekacolmenero@gmail.com	BEATRIZ 	COLMENERO ARENADO	2018-07-10 17:22:00	Maria	Escritor
fernandohr96@gmail.com	Fernando	Huertas 	2018-07-10 18:39:00	Maria	Escritor
antoniolibros@hotmail.com	Antonio 	Aguilar  Diaz	2018-07-11 12:53:00	Maria	Escritor
luischavez@hotmail.com	Luis 	Chávez 	2018-07-11 12:57:00	Maria	Escritor
laura.bm1@hotmail.comâ€¯	Laura	Barberán Muniesa	2018-07-12 10:28:00	Maria	Escritor
lizartonne.ilustracion@gmail.com	Dani 	Rodríguez 	2018-07-12 10:48:00	Maria	Escritor
ramonricojimenez@gmail.com	Ramon	Rico 	2018-07-12 12:56:00	Maria	Escritor
unratoparami@gmail.com	SUSANA	ZQUIERDO GIL	2018-07-12 18:58:00	Maria	Escritor
mireiajaques.mail@gmail.com	Mireia 	Jaques 	2018-07-13 09:57:00	Maria	Escritor
pgruth13@hotmail.com	Ruth	Perez 	2018-07-16 12:23:00	Maria	Escritor
manuelmateosmorillo@yahoo.es	 Manuel 	Mateos 	2018-07-16 14:30:00	Maria	Escritor
emma_sanchez_varela@hotmail.com	Emma 	Varela 	2018-07-16 17:35:00	Maria	Escritor
ubeimarfranco@gmail.com	Ubeimar	Franco 	2018-07-17 10:22:00	Maria	Escritor
Raquel.Fernandez@uv.es	Raquel	Fernández Serrano	2018-07-17 11:03:00	Maria	Escritor
evteeva_maria@hotmail.com	María	Benisty 	2018-07-17 12:59:00	Maria	Escritor
esteladediego.art@gmail.com	Estela	De Diego 	2018-07-18 11:40:00	Maria	Escritor
uscateguialejandro@gmail.com	Alejandro	Uscategui 	2018-07-18 12:23:00	Maria	Escritor
crlpgn@gmail.com	Carolina 	Pingarrón 	2018-07-18 12:42:00	Maria	Escritor
lacuentadenuriaarmesto@gmail.com	Nuria 	Armesto 	2018-07-19 11:53:00	Maria	Escritor
ojuben@gmail.com	Omar	Jurado Benedit	2018-07-20 12:12:00	Maria	Escritor
tamara_titan@hotmail.com	Tamara	Martín 	2018-07-20 12:17:00	Maria	Escritor
adacollet@gmail.com	Ada	Collet 	2018-07-20 12:38:00	Maria	Escritor
ivankacerqueira97@gmail.com	Ivanka	Cerqueira 	2018-07-20 12:40:00	Maria	Escritor
jabatostudio@gmail.com	Alex 	Pérez 	2018-07-20 12:54:00	Maria	Escritor
hola@ladelosrizos.com	Macarena	Campo 	2018-07-23 11:03:00	Maria	Escritor
pimaruiz@hotmail.com	Pilar 	Martín 	2018-07-23 11:04:00	Maria	Escritor
josecarlosantequera@yahoo.es	José Carlos 	Antequera  Roa	2018-07-23 12:21:00	Maria	Escritor
mubdi250@gmail.com	Alicia 	Martínez  Martínez	2018-07-23 14:19:00	Maria	Escritor
merketruiz@gmail.com	María Mercedes 	Ruiz 	2018-07-23 14:50:00	Maria	Escritor
josefabiorivas@hotmail.com	Jose Fabio 	Rivas  Guerrero	2018-07-26 13:49:00	Maria	Escritor
yazmila@gmail.com	Ana	.. 	2018-07-27 09:55:00	Maria	Escritor
teixidorrosamaria@gmail.com	Rosa 	Teixidor 	2018-07-27 11:00:00	Maria	Escritor
pazoloren@gmail.com	Lorena	Garcia Pazos	2018-07-27 12:35:00	Maria	Escritor
f10galan@yahoo.es	ELOY FERNANDO	GALAN 	2018-07-27 14:00:00	Maria	Escritor
aletorrecillas@yahoo.com.mx	Alejandra 	Torres 	2018-07-27 14:02:00	Maria	Escritor
ibardera@telefonica.net	MARIA INMACULADA 	BARDERA 	2018-07-27 14:04:00	Maria	Escritor
arquitectolizcano@gmail.com	Jose	Lizcano 	2018-07-27 14:06:00	Maria	Escritor
martanahe@hotmail.com	Marta 	Navarro Heredia	2018-07-31 08:41:00	Maria	Escritor
Melu_k-pa_29@hotmail.com	Melisaa	.. 	2018-07-31 08:54:00	Maria	Escritor
patribp82@hotmail.com	Patricia	.. 	2018-07-31 08:56:00	Maria	Escritor
ansago71@yahoo.es	Ángela	SanGómez 	2018-07-31 10:33:00	Maria	Escritor
fidalgohelena@hotmail.com	Helena 	Fidalgo 	2018-07-31 10:50:00	Maria	Escritor
cvhenrycasanova@gmail.com	Henry 	Casanova 	2018-07-31 12:04:00	Maria	Escritor
sofiamagdalenaeriksson@gmail.com	Sofia 	Eriksson 	2018-08-01 09:28:00	Maria	Escritor
pilopavila@gmail.com	Pilar 	López  Ávila	2018-08-01 12:23:00	Maria	Escritor
mellamo.ohane@gmail.com	Marina 	Bayonas  Hernández	2018-08-01 12:37:00	Maria	Escritor
cuentacuentosavila@gmail.com	Natalia 	Vallejo Guerro	2018-08-06 09:49:00	Maria	Escritor
david-lc-@hotmail.com	DAVID 	LANDERAS  CICERO	2018-08-07 14:30:00	Maria	Escritor
juanmayor327@hotmail.com	Juan David 	Mayor  	2018-08-09 14:50:00	Maria	Escritor
lpressacco@hotmail.com	Leandra 	Pressacco 	2018-08-10 13:02:00	Maria	Escritor
goldrajch.rita@gmail.com	Rita 	Goldrajch 	2018-08-10 13:48:00	Maria	Escritor
mijibau@yahoo.es	Miguel 	Jiménez De Cisneros	2018-08-13 11:32:00	Maria	Escritor
kelsyquiroz90@gmail.com	Kelsy	Quiroz 	2018-08-13 13:17:00	Maria	Escritor
miguelangelhg@live.com	Miguel Angel 	Herrera Gumiel 	2018-08-14 10:28:00	Maria	Escritor
newczar2012@gmail.com	Newton 	Cesar  	2018-08-14 11:18:00	Maria	Escritor
lamaestradept@hotmail.com	Rocio	Olivares 	2018-08-14 14:17:00	Maria	Escritor
csantacruz@diadapsicologia.es	Cynthia 	Santacruz 	2018-08-16 12:29:00	Maria	Escritor
florianosc90@gmail.com	Antonio	Floriano 	2018-08-17 12:20:00	Maria	Escritor
darlamey@hotmail.com	Elena	Hernandez Gonzalez	2018-08-20 12:11:00	Maria	Escritor
jecarruiz@hotmail.com	Jesus 	Cárdenas  Ruiz	2018-08-20 12:16:00	Maria	Escritor
yaizatorresbeffa@gmail.com	Yaiza 	Torres 	2018-08-20 14:00:00	Maria	Escritor
dussanmateo1989@gmail.com	Mateo	Dussan 	2018-08-21 13:13:00	Maria	Escritor
evars1991@gmail.com	Eva	Rodríguez 	2018-08-21 13:16:00	Maria	Escritor
julge26@gmail.com	Gema De Jesús	López Martín	2018-08-21 13:25:00	Maria	Escritor
nonielfo@yahoo.es	Choni 	Fernández Villaseñor	2018-08-27 15:02:00	Maria	Escritor
auxi.maestre@gmail.com	MARIA AUXILIADORA	.. 	2018-08-27 15:09:00	Maria	Escritor
ali_guoman@hotmail.com	Alicia 	González Mancha	2018-08-27 17:19:00	Maria	Escritor
mleobarbieri@gmail.com	Mirta 	Castro 	2018-08-27 17:24:00	Maria	Escritor
antondebenito@gmail.com	Antonio	De Benito 	2018-08-27 17:28:00	Maria	Escritor
magovi_@hotmail.com	Marian 	Vinuesa 	2018-08-28 09:29:00	Maria	Escritor
adrianfresno@hotmail.com	Adrián 	Fresno 	2018-08-28 11:27:00	Maria	Escritor
rossana.sala@gmail.com	Rossana 	Sala 	2018-08-29 13:55:00	Maria	Escritor
lucindaparkerroberts@gmail.com	Lucinda	.. 	2018-08-29 13:59:00	Maria	Escritor
amaiacalzadagorricho@gmail.com	Amaia 	Calzada  Gorricho	2018-08-29 14:01:00	Maria	Escritor
esteladalila@gmail.com	Stela	Navarro 	2018-08-30 12:02:00	Maria	Escritor
patri2870@outlook.com	Patricia 	Suarez  Santamaria	2018-08-30 12:55:00	Maria	Escritor
mentemocional@hotmail.com	Yhovana 	Carrion 	2018-08-30 13:01:00	Maria	Escritor
jrgomez@mostachin.com	José Ramón 	Gómez 	2018-08-31 12:19:00	Maria	Escritor
jomacumo29@gmail.com	Jorge 	Cuerda 	2018-09-03 12:57:00	Maria	Escritor
frank65llobregat@gmail.com	Francisco Javier 	Llobregat  Garcia	2018-09-03 17:15:00	Maria	Escritor
amaravenayas@hotmail.com	Amara 	Venayas  Rodriguez	2018-09-04 13:36:00	Maria	Escritor
garciaromeroalicia@gmail.com	Alicia 	Garcia 	2018-09-04 14:08:00	Maria	Escritor
tzuluagac@gmail.com	Tere 	Zuluaga 	2018-09-04 18:19:00	Maria	Escritor
mariajosefinatm@hotmail.com	Maria Josefina 	Trujillo  Mayz	2018-09-04 18:36:00	Maria	Escritor
magelademarco@hotmail.com	Magela 	Demarco 	2018-09-07 11:54:00	Maria	Escritor
topakarting@gmail.com	Elena 	Quintero  Roldán	2018-09-07 13:52:00	Maria	Escritor
gransosia@gmail.com	David 	De La Fuente  Alonso	2018-09-07 14:40:00	Maria	Escritor
m.santos@rojumar.com	Monica 	Santos 	2018-09-07 14:43:00	Maria	Escritor
anab.mifer@hotmail.com	Ana Belén	- 	2018-03-12 17:49:00	Paz	Escritor
jaenenri@gmail.com	Enri	Jota 	2018-03-12 17:57:00	Paz	Escritor
chema-96@hotmail.es	José Manuel	Bermudo López	2018-03-12 17:59:00	Paz	Escritor
elcuponeroleganes@gmail.com	Ángel Luis	García Martínez	2018-03-12 18:02:00	Paz	Escritor
pilarmontorocadiz@gmail.com	Pilar	Montoro Cadiz	2018-03-12 18:06:00	Paz	Escritor
escritormad@outlook.es	Daniel	E Gómez	2018-03-12 18:09:00	Paz	Escritor
JoseGonzalezDecoracion@hotmail.com	Jose	González 	2018-03-14 17:30:00	Paz	Escritor
aguileraromerorocio@gmail.com	Rocío	Romero 	2018-03-14 17:36:00	Paz	Escritor
joseantoniopv06@hotmail.com	José Antonio	Prades Villanueva	2018-03-14 18:10:00	Paz	Escritor
elias_gc56@hotmail.com	Elías	García Chávez	2018-03-14 18:19:00	Paz	Escritor
kophrodess@hotmail.es	Jordi	Moreno 	2018-03-19 17:54:00	Paz	Escritor
david.ariza@delcolegio.es	David 	Ariza 	2018-03-19 18:43:00	Paz	Escritor
ivan1976sanchez@hotmail.com	Ivan	Sánchez 	2018-03-19 19:01:00	Paz	Escritor
carloshugoasperilla@gmail.com	Carlos	Hugo Asperilla	2018-03-22 17:09:00	Paz	Escritor
dostolov@gmail.com	Daniel	Fernández 	2018-04-02 16:21:00	Paz	Escritor
astray.winter@gmail.com	Eduardo	Astray Viqueira	2018-04-02 16:35:00	Paz	Escritor
abetetamas87@hotmail.com	Albert	Beteta 	2018-04-05 16:54:00	Paz	Escritor
j_anaya_flores@yahoo.es	Jerónimo	Anaya Flores	2018-04-09 16:12:00	Paz	Escritor
jaime.roda@gmail.com	Jaime	Rodas Bruce	2018-04-09 18:00:00	Paz	Escritor
siluetadenadie@gmail.com	Daniel Félix	Pizarro Santos	2018-04-12 16:27:00	Paz	Escritor
florhaya@hotmail.com	Lucía	Haya 	2018-04-12 16:52:00	Paz	Escritor
matime72@hotmail.com	Matilde	Mendieta Goicoechea	2018-04-16 09:54:00	Paz	Escritor
mjgomezm6@gmail.com	María Jesús	Gómez 	2018-04-17 08:03:00	Paz	Escritor
neusgonzalezborrell@gmail.com	Neus	González Borrel	2018-04-17 13:02:00	Paz	Escritor
martag897@gmail.com	Marta	G Adrián	2018-04-23 16:39:00	Paz	Escritor
monteromarco999@gmail.com	Marek	Polack 	2018-04-23 16:49:00	Paz	Escritor
raineres@nauta.cu	Rainer	Castellá 	2018-04-23 17:14:00	Paz	Escritor
mejimanx@hotmail.com	David	Bernal Castillo	2018-04-23 17:17:00	Paz	Escritor
noepoli43@gmail.com	Tony	. 	2018-04-23 17:29:00	Paz	Escritor
Ivan_larreaa@hotmail.com	Iván	Larrea 	2018-04-23 17:34:00	Paz	Escritor
malala29.6@gmail.com	Mariana De Jesús	Pereira Severino	2018-04-24 16:00:00	Paz	Escritor
zitges8@hotmail.com	Patricia	Haro Guerrero	2018-04-30 16:13:00	Paz	Escritor
quircewines@gmail.com	Javier	Quince 	2018-05-03 16:30:00	Paz	Escritor
lauratriasc@gmail.com	Laura	Trías Cañete	2018-05-07 15:48:00	Paz	Escritor
laurazarraga.m@gmail.com	María Laura	Dávila Zárraga	2018-05-09 16:58:00	Paz	Escritor
ana.merino00@csioviedo.com	Ana	Merino 	2018-05-09 17:12:00	Paz	Escritor
sebastianjorgi@hotmail.com	Sebastián	Jorgi 	2018-05-09 17:17:00	Paz	Escritor
c.cebriangonzalez@hotmail.com	Carlos	Cebrián González	2018-05-14 09:34:00	Paz	Escritor
creaciondemanuela@hotmail.es	Manuela	Oliver Torets	2018-05-14 09:36:00	Paz	Escritor
ordas79@gmail.com	Juan José	Ordás Fernández	2018-05-14 09:44:00	Paz	Escritor
paaablooo23@gmail.com	Pablo	Muñoz 	2018-05-14 09:50:00	Paz	Escritor
senovillo@gmail.com	Paco	Rojas 	2018-05-14 09:53:00	Paz	Escritor
ventas@socialcommunicationskills.org	Mariana	Man Mora	2018-05-16 09:38:00	Paz	Escritor
carolinacerruti@gmail.com	Carolina	Cerruti 	2018-05-17 16:12:00	Paz	Escritor
rosmer@telefonica.net	Rosa	Moreno 	2018-05-21 09:08:00	Paz	Escritor
holaflamencolica@gmail.com	Rocío	Hellín Sánchez	2018-05-23 16:31:00	Paz	Escritor
manujimenezmoure@gmail.com	Manu	Jimenez Moure	2018-05-28 15:46:00	Paz	Escritor
valoratuself@hotmail.com	Renzo Reynaldo	Gamarra Manrique	2018-05-28 15:50:00	Paz	Escritor
paco-rojas@hotmail.es	Francisco	Rojas Gómez	2018-05-28 16:53:00	Paz	Escritor
narlumargarita@gmail.com	Margarita	Montes 	2018-06-04 15:51:00	Paz	Escritor
montsehh@gmail.com	Montserrat	Hernández Hernández	2018-06-04 16:07:00	Paz	Escritor
mabisancristobal@gmail.com	Mabi 	B 	2018-06-04 16:12:00	Paz	Escritor
mariareiki17@yahoo.com	María	García Suárez	2018-06-04 17:17:00	Paz	Escritor
franciscojavierveralopez@hotmail.com	Francisco Javier	Vera López	2018-06-04 17:21:00	Paz	Escritor
scasaseca@gmail.com	Salus	Casaseca 	2018-06-19 16:11:00	Paz	Escritor
eduardom57@nauta.cu	Eduardo	. 	2018-06-19 16:21:00	Paz	Escritor
juansalvadorpinero@gmail.com	Juan Salvador	Piñero Ruiz	2018-06-19 16:23:00	Paz	Escritor
joxnmahiques@gmail.com	Joan	Mahiques Bou	2018-06-19 16:31:00	Paz	Escritor
maykamartinez1961@gmail.com	Carmen	Martínez Villareal	2018-06-19 16:59:00	Paz	Escritor
cuerdage@gmail.com	Esther	De La Cuerda 	2018-06-19 17:08:00	Paz	Escritor
ragaujavier@gmail.com	Javier	Ragau 	2018-06-19 17:14:00	Paz	Escritor
oyesmira@bizkaia.eu	Sonia 	Cuesta Clemente	2018-06-19 17:21:00	Paz	Escritor
sacoal@hotmail.com	Saúl	Contreras 	2018-06-21 16:24:00	Paz	Escritor
inesprats70@gmail.com	Ines	Prats 	2018-06-25 09:04:00	Paz	Escritor
luzdeatlante@hotmail.com	Vanesa	Ramírez Ramos	2018-07-03 10:05:00	Paz	Escritor
ramiant76@gmail.com	Antonio	Ramírez 	2018-07-03 10:08:00	Paz	Escritor
bedoyarti@hotmail.com	Lourdes	Bedoya 	2018-07-05 10:39:00	Paz	Escritor
ferreirar@intramed.net	Ricardo	Ferreira 	2018-07-11 16:56:00	Paz	Escritor
fernando.delablanca@gmail.com	Fernando	Barrientos De La Blanca	2018-07-11 17:00:00	Paz	Escritor
majoduma@yahoo.es	María Jose	Duque Martín	2018-07-16 10:02:00	Paz	Escritor
Amadorsanchezdeleonana@gmail.com	Ana María 	Amador Sánchez De León	2018-07-17 11:49:00	Paz	Escritor
laspalmasbusftv@gmail.com	Héctor	López Moreno	2018-07-20 12:36:00	Paz	Escritor
mjcarreira@hotmail.com	María José	Carreira Pastor	2018-07-23 09:57:00	Paz	Escritor
carminaid@gmail.com	Carmina	Iñigo Dies	2018-07-23 10:03:00	Paz	Escritor
aicila1961@hotmail.com	Alicia	Martín Cabezas	2018-07-25 11:12:00	Paz	Escritor
jp.defrutos.barrero@gmail.com	Juan Pedro	De Frutos 	2018-07-25 11:16:00	Paz	Escritor
albertoatienza@yahoo.es	Alberto	Atienza Benedicto	2018-07-31 10:28:00	Paz	Escritor
persiguiendomissuenos1205@gmail.com	Tamara	. 	2018-07-31 10:51:00	Paz	Escritor
rafaelsaenzdecabezon@gmail.com	Rafael	Saenz De Cabezon	2018-07-31 10:57:00	Paz	Escritor
ratakeli@outlook.com	Raquel	Calabuig 	2018-08-14 10:38:00	Paz	Escritor
uve2arq@gmail.com	Víctor 	Marín Gómez	2018-08-16 10:26:00	Paz	Escritor
silentmml25@gmail.com	Miguel	Mas Lozoya	2018-11-15 10:15:21.467567	Paz	Escritor
m.francosanchez24@gmail.com	manuel	franco	2018-10-24 19:08:14.84614	admin	Escritor
paumezquita@hotmail.com	Pau	Mezquita Romero	2018-10-26 08:41:34.248007	paz	Escritor
sylviamelo.as@gmail.com	Sylvia	Melo Linares	2018-10-31 09:46:56.224011	paz	Escritor
DMJORDI@HOTMAIL.COM	Jorge	Ramón García	2018-10-31 09:48:54.051907	paz	Escritor
marialeivag1993@hotmail.com	María	Leiva	2018-10-31 09:51:09.143844	paz	Escritor
max.darck01@gmail.com	A. J	Manrique	2018-10-31 09:53:17.996594	paz	Escritor
anamizquierdo@ono.com	Ana	Izquierdo García	2018-11-05 09:15:58.767358	paz	Escritor
nikkho@gmail.com	Javier	Gutiérrez Chamorro	2018-11-05 09:28:43.313452	paz	Escritor
susrizo@yahoo.es	Susana 	Rizo	2018-11-05 09:31:22.36493	paz	Escritor
carol.arbu.76@gmail.com	Carolina	Arbués	2018-11-15 10:19:26.541488	Paz	Escritor
marioluisborges@hotmail.com	Mario L	Borges Sáiz	2018-11-15 10:28:26.980713	Paz	Escritor
caryom2017@gmail.com	Caridad	O'Farrill Montero	2018-11-05 09:37:17.948815	paz	Escritor
nueva-via@hotmail.com	Fernando	Acedo Macías	2018-11-05 09:42:51.742823	paz	Escritor
info@marcovaleriolama.com	Marco	Valerio Lama	2018-11-07 11:20:42.812687	Paz	Escritor
delia.bento@gmail.com	Delia	Bento Sánchez	2018-11-07 11:45:20.268455	Paz	Escritor
manuelmac78@msn.com	Manuel	Martín Arroyo	2018-11-12 09:19:56.567659	Paz	Escritor
krismaec@gmail.com	Marta	Montes	2018-11-12 12:17:35.075576	Maria	Escritor
LCH179102XVII@gmail.com	Lorenzo	Chaparro	2018-11-13 09:58:18.418072	Paz	Escritor
mota.ja@outlook.com	Juan Antonio	Mota	2018-11-14 09:04:39.70909	Paz	Escritor
rubenymontse7@gmail.com	Montserrat	Sánchez Castilla	2018-11-15 10:48:59.316946	Paz	Escritor
quinopena@hotmail.com	Joaquín	Peña Manzano	2018-11-15 10:55:10.440965	Paz	Escritor
teamchilalynn@gmail.com	Chila	Lynn	2018-11-21 10:26:57.288317	Paz	Escritor
pueblasdelgados@gmail.com	Cristóbal	Puebla García	2018-11-15 10:17:39.345832	Paz	Escritor
baix46@yahoo.es	José	Sancho Hernández	2018-11-19 09:14:39.381581	Paz	Escritor
piratilla75@hotmail.com	Javier 	García Martín	2018-11-19 09:24:14.019946	Paz	Escritor
angeledorobles@gmail.com	Ángel	Edo Robles	2018-11-21 10:39:42.526305	Paz	Escritor
mrojas.escritora@gmail.com	Mónica	Rojas Rubín	2018-11-27 09:30:54.917551	Paz	Escritor
LordyMesias@gmail.com	Víctor	Mesías María Martínez	2018-11-16 12:35:26.52843	Paz	Escritor
apallares@informance.es	Albert	Pallarés	2018-11-27 09:45:04.365658	Maria	Escritor
arajay@hotmail.es	Antonio	Reynosa	2018-11-27 10:05:16.598428	Maria	Escritor
priscillaylazingara@gmail.com	Rosana	Gil	2018-11-27 12:54:06.21079	Maria	Escritor
kgmencia@hotmail.com	Carol 	García	2018-11-27 17:21:04.509278	Maria	Escritor
felizconmigo@libero.it	Serena 	Fava	2018-11-27 13:07:09.828965	Maria	Escritor
marta@babidibulibros.com	Marta	Presidenta	2018-11-28 09:56:07.09921	Paz	Escritor
zidac@hotmail.com	Maria	Calero 	2018-08-23 17:55:00	marta	Escritor
montesmart@hotmail.com	Marta	Montes	2018-11-29 11:18:52.743273	Marta	Escritor
igoneurrutikoetxea@yahoo.com	Igone	Urrutikoetxea	2018-12-03 15:18:53.170175	Paz	Escritor
alpe0@hotmail.com	Alvaro 	Perez	2018-12-04 15:26:34.76275	Maria	Escritor
trimonant@gmail.com	Antonio	-	2018-12-05 09:28:06.616601	Paz	Escritor
nahuel.exposito@yahoo.com.ar	Nahuel	Expósito	2018-12-05 16:00:10.692885	Maria	Escritor
bherrer5@xtec.cat	Beatriz	Herrera Córdoba	2018-12-07 08:49:24.862585	Maria	Escritor
pinkiwinkishop@gmail.com	Leire 	Conde Abella	2018-12-07 10:35:56.677337	Maria	Escritor
psicochamos@gmail.com	Melissa 	Rosales	2018-12-07 10:57:22.416262	Maria	Escritor
lizhrowland@yahoo.com	Isabel 	Hernandez Rowland	2018-12-10 10:54:58.763869	Maria	Escritor
andk_3@hotmail.com	Andrea 	Caterina 	2018-12-10 12:08:49.85873	Maria	Escritor
josemarse@hotmail.com	José Manuel 	Martínez Segura	2018-12-10 12:47:10.042555	Maria	Escritor
deliaorella@gmail.com	Delia 	Orella	2018-12-10 15:51:23.955097	Maria	Escritor
maestrorealmanuel@icloud.com	Manuel	Maestro Real	2018-12-11 10:27:07.508966	Paz	Escritor
antonello.puglia@gmail.com	Los Caballeros del Rey Arturo	..	2018-12-12 16:21:14.704946	Maria	Escritor
torres.cecilia7@gmail.com	Cecilia	Torres	2018-12-12 16:23:20.259658	Maria	Escritor
alvaromarbangarcia1989@gmail.com	Álvaro 	Marbán García	2018-12-12 16:29:35.490414	Maria	Escritor
maloroberto@hotmail.com	ROBERTO 	MALO	2018-12-12 16:55:19.710286	Maria	Escritor
nerear47@gmail.com	Nerea 	Riveiro	2018-12-12 17:23:33.528725	Maria	Escritor
cira12@hotmail.es	Sonia	Domingo	2018-12-13 10:59:55.55647	Paz	Escritor
terapiasutu@gmx.es	Paco	Purito Arconada	2018-12-13 11:05:58.579172	Paz	Escritor
diana_garcia_21@hotmail.com	Diana	García Santiago	2018-12-13 11:07:45.357145	Paz	Escritor
CARYYPEDRO2011@hotmail.com	Pedro 	Vázquez	2018-12-14 09:49:06.303266	Paz	Escritor
raulduemon@hotmail.es	Raúl 	Dueñas Montes	2018-12-14 11:51:57.443629	Maria	Escritor
vazquez.sanchez.patricia@gmail.com	Patricia 	Vázquez	2018-12-14 11:56:07.00159	Maria	Escritor
luanka78@gmail.com	Carolina	Sánchez Pérez	2018-12-14 12:13:00.867804	Maria	Escritor
androidon69@gmail.com	María de los Ángeles	Diez Rodríguez	2018-12-17 10:06:01.31089	Paz	Escritor
jeronimo.martinez3@carm.es	Jerónimo	Martínez Laborda	2018-12-17 10:11:01.321109	Maria	Escritor
octavi@octavi-franch.com	Octavi	Franch	2018-12-17 11:59:18.054202	Maria	Escritor
tonitello85@gmail.com	Toni Miquel	Tello	2018-12-17 12:33:04.492163	Maria	Escritor
tobaliuss@hotmail.com	Cristóbal	Ramirez	2018-12-17 12:41:01.023814	Maria	Escritor
lapinadefraybentos@hotmail.com	Celia	Pierina Gola	2018-12-17 12:41:34.401762	Maria	Escritor
evamoyag@gmail.com	Eva María	Moya Gómez	2018-12-17 12:44:21.139746	Maria	Escritor
\.


--
-- Data for Name: cadenas_correo; Type: TABLE DATA; Schema: public; Owner: manu
--

COPY public.cadenas_correo (id, orden, texto, asunto, editorial) FROM stdin;
2	2	Hola [Autor/a],\n\n¿Recibiste nuestro email con la información requerida para realizar el contrato de edición? No tengo respuesta y esperaba recibirlo en estos días. Te recuerdo que los datos que necesito son estos:\nTitulo de la obra:\nNombre fiscal:\nNombre comercial: El que aparecerá en el libro y webs\nTipo de autor: Escritor / Ilustrador / Escritor-ilustrador\nDNI: Sin espacios ni guiones\nCuenta bancaria (I.B.A.N): Para pago de royalties. Solo autores residentes en la UE.\nCuenta Paypal: Para pago de royalties. Autores no residentes en la UE.\nTeléfono(s):\nEmail:\nDirección: lo más clara posible, sobre todo para autores no residentes en España.\nC.P:\nLocalidad:\nProvincia / Estado:\nPaís:\nAño de nacimiento:\nLugar de nacimiento:\nWeb:\nNecesito factura de empresa/autónomo: SÍ / No.\n\n\nPor favor, no dudes en hacerme llegar la información que precises para seguir avanzando.\n\nUn saludo y gracias por atenderme.\n\n	Datos para contrato de edición	babidibu
2	3	Hola de nuevo [Autor/a],\n\nEra para hacerte saber que te envié un email hace unos días donde te explicaba los datos que necesitaba de ti para completar el contrato de tu libro. No estoy segura de haber recibido respuesta y necesitaba saber si tenías alguna duda o querías hacerme llegar un comentario.\n\nTe pido que me hagas llegar tus comentarios.\n\nEspero tu respuesta.\n\nDe nuevo, gracias por tu atención.\n\n	Datos para contrato de edición	babidibu
2	4	Hola [Autor/a],\n\nSigo esperando que nos hagas llegar tus datos personales para enviarte el contrato de edición cumplimentado. ¿Lo recibiste? Dímelo para reenviártelo de nuevo.\n\nSi ya lo recibiste, te recuerdo que quedo a tu completa disposición para resolver lo que necesites.\n\nGracias de nuevo por tu atención y espero pronto tu respuesta.\n\nUn saludo.\n\n	Datos para contrato de edición	babidibu
2	5	Muy buenas [Autor/a],\n\nHace algunos días que te envié un email pidiéndote tus datos personales para terminar de redactar el contrato de edición de tu libro.\n\nEspero que los hayas recibido. Sigo a tu disposición y espero tu respuesta y poder enviártelo completo de nuestra parte cuanto antes.\n\nUn saludo y gracias por tu atención.\n\n	Datos para contrato de edición	babidibu
2	6	Hola de nuevo [Autor/a],\n\nSigo a la espera de que me hagas llegar tus datos personales para poder reenviarte ya completo el contrato de edición. ¿Lo recibiste?\n\nSigo, como siempre, a tu entera disposición.\n\nUn saludo y gracias por tu atención.\n\n	Datos para contrato de edición	babidibu
4	2	Hola [Autor/a],\n\n¿Recibiste nuestro email con solicitud de información sobre tu proyecto editorial?\n\nEstaríamos encantados de poder valorarlo…\n\nHazme llegar cualquier duda o comentario que puedas tener.\n\nUn saludo,\n\n	Proyecto editorial	babidibu
4	3	Hola [Autor/a],\n\nTe recuerdo que estamos esperando una respuesta a nuestro email enviado hace algunos días.\n\nDinos algo, por favor.\n\nUn saludo y gracias por tu atención,\n\n	Proyecto editorial	babidibu
4	4	Hola [Autor/a],\n\nSeguimos esperando tus comentarios al email que te enviamos hace ya algunos días, ¿lo recibiste?\n\nHázmelo saber para reenviártelo de nuevo, y dime si tienes alguna duda al respecto.\n\nUn saludo y gracias de nuevo,\n\n	Proyecto editorial	babidibu
4	5	Hola [Autor/a],\n\nLlevo varios días sin recibir ningún comentario sobre la información sobre tu proyecto editorial. ¿recibiste mis emails?\n\nUn saludo,\n\n	Proyecto editorial	babidibu
4	6	Hola [Autor/a],\n\n¿Has recibido nuestros últimos emails? \n\nSeguimos a tu entera disposición para lo que necesites.\n\nUn saludo y gracias,\n\n	Proyecto editorial	babidibu
1	1	Agradecemos el interés que has mostrado por nuestra editorial.\n\nHemos recibido tu obra y esta se encuentra a la espera de evaluación por parte de nuestro Dpto. de Lectura, que será el que determine su adecuación a nuestra línea editorial.\n\nSi es posible, te solicitamos que nos reenvíes el cuadro que te adjuntamos, debidamente cumplimentado con tus datos como autor y con los datos de tu obra. Para nuestro Comité de Lecturas, este cuadro es una herramienta indispensable de control y organización administrativa que se anexiona al expediente de cada obra durante todo el proceso de la evaluación.\n\nSi te surgiera cualquier duda al respecto, estaremos encantados de resolverla. Nuestros teléfonos son el 912665684 y el 653754771, o bien puedes pedirnos por mail que te llamemos a un tlf de contacto en tu horario preferente.\n\nEn cualquier caso, en unos días nos pondremos en contacto contigo, tras la valoración.\n\nUn saludo y muchas gracias por tu atención. \n\n(FIRMA SEGÚN USUARIO).	Pre-evaluación y condiciones	babidibu
13	2	Hola [Autor/a],\n\nQuería saber si recibiste nuestro email con la información requerida sobre tu libro.\n\nPor favor, hazme llegar cualquier duda o comentario para que podamos seguir avanzando.\n\nUn saludo y gracias por tu atención.\n\n	Contacto Mirahadas	mirahadas
13	3	Hola de nuevo [Autor/a],\n\n¿Recibiste mi email con la información requerida respecto a tu libro?\n\nPor parte de la Editorial, queremos seguir avanzando. Dinos algo al respecto.\n\nUn saludo y gracias por tu atención.\n\n	Contacto Mirahadas	mirahadas
10	2	Hola [Autor/a],\n\n¿Has recibido el email con el contrato de edición? No tengo constancia de tu respuesta y seguimos pendientes de recibir el contrato.\n\nHazme llegar cualquier duda al respecto para seguir avanzando.\n\nUn saludo y gracias por tu atención,\n\n	Contrato de edición	mirahadas
10	3	Hola de nuevo [Autor/a],\n\nQuería saber si recibiste nuestro email enviado hace algunos días con el contrato de edición. No tengo constancia de haber recibido respuesta de tu parte y quiero saber si finalmente vas a editar con nosotros. \n\nTe recuerdo que puedes enviárnoslo por email o, si lo deseas y nos avisas, podemos mandar un mensajero a recogerlo a donde nos digas.\n\nHazme llegar cualquier comentario al respecto.\n\nEsperamos tu respuesta.\n\nGracias por tu atención,\n\n	Contrato de edición	mirahadas
6	1	Reiteramos el agradecimiento por el interés que has mostrado por nuestra editorial.\n\nTras la recepción de tu obra y tras la primera evaluación por parte de nuestro Dto. de Lecturas, y dado el posible potencial que vemos en ella y su adecuación a nuestra línea editorial, nos complace comunicarte que estaríamos interesados en publicarla dentro de nuestra colección _______________________________ para, posteriormente, promocionarla y distribuirla en nuestros canales (tradicionales y plataformas online).\n¡¡¡¡¡¡¡¡¡¡¡¡Hablar de la COLECCIÓN.!!! COPIAR DE LA WEB Y PEGAR ENLACE!!!!!!!!!!!!!!!\nTu libro, aparte de pertenecer a esta colección, formará parte de un sello editorial especializado en literatura infantil. Somos una de las 25 editoriales infantiles más relevantes del panorama español actual, las cuales se caracterizan por ir a la vanguardia de la edición infantil española, creando precedentes y tendencias a imitar por el resto de editoriales.\nVisualiza los siguientes enlaces de interés que avalan lo dicho anteriormente.\nhttps://www.julianmarquina.es/24-editoriales-especializadas-en-libros-infantiles-que-no-puedes-perder-de-vista/\nhttps://blog.babidibulibros.com/f/juvenalia-un-año-más-contará-con-la-participación-babidi-bú\n\nAdemás, somos la 3ª editorial especializada en literatura infantil y juvenil según la Biblioteca Nacional de España.\nhttp://www.bne.es/es/Micrositios/Guias/Literatura_Infantil/EditorialesLibrerias/EditorialesEspecializadas/\nTambién te será útil visualizar los siguientes videos en los que autores que ya han publicado con BABIDI-BÚ cuentan su experiencia.\nhttps://www.babidibulibros.com/p/videopiniones_65/\n\nNo olvides que formarás parte de un selecto grupo de autores entre los que figuran firmas literarias tan relevantes como Ana Rossetti.\nhttps://blog.babidibulibros.com/f/%C2%BFqu%C3%A9-es-poes%C3%ADa-ana-rossetti\n\nComo ya te hemos comentado el texto de tu obra nos interesa mucho, aunque esta, por el público sensible al que va dirigido, necesita una corrección (esto es normal, lo hacemos con todos textos que nos llegan, hasta con los de autores de extensa trayectoria).\nTodas nuestras obras se corrigen de forma obligatoria, esto es muy importante, más aún en literatura infantil. Además, posiblemente podamos enriquecer tu obra con una serie de ideas complementarias y actividades que mejorarán el resultado final, sobre todo de cara al uso del libro en colegios. Esto se tratará de forma personalizada con el corrector asignado.\nSegún la colección a la que pertenecería tu obra y para que resulte un producto comercial lo más competitivo, las características técnicas deberían ser:\n\nENCUADERNACIÓN:  TAPA DURA / TAPA BLANDA.\nFORMATO: ____ x ____ mms.\nN.º PÁGINAS (aprox.): _____.\nPAPEL INTERIOR: Offset ahuesado - Offset ahuesado / Offset blanco.\nCOLOR / ByN: número de págs. a color (¡lustraciones + portadilla !!!!!!!).\n\nPor otra parte, según este tipo de libro, hemos calculado que el PVP óptimo deberá ser _______ Euros (IVA incluido).\nTras pasar el filtro indispensable de nuestro Comité de Lectura, pasamos a informarte, de forma personalizada, de las condiciones para lleva a cabo esta publicación.\nEres un autor novel que nos ofrece una obra de calidad que se adecua a nuestra línea editorial y que transmite valores implícitos y, además, vemos en dicha obra el potencial comercial necesario para publicarla, apostando por ella, entre otras. Sin embargo, son muchos los manuscritos que nos llegan al mes con estas mismas características: autores sin trayectoria previa, pero con grandes ideas. Por lo que tenemos que volver a seleccionar, por falta de capacidad para asumirlas todas.\nPor lo tanto, para dar viabilidad inmediata a tu proyecto, mostrando tú también tu confianza en tu obra y en que esta va a funcionar entre el público al que se dirige (comenzando por tu entorno cercano y tu ámbito local de influencia y abriendo, poco a poco, el camino a la distribución nacional), necesitamos que impulses el proyecto con una compra mínima de _________ ejemplares, con un descuento del _______ % del PVP, que te permitirá un beneficio tras su venta directa.\nSi te interesarse comprar más libros por motivos propios (¡sin compromiso!) debes saber que los descuentos de autor sobre los mismos suben de forma progresiva, para que puedas lucrarte con tu compra de forma progresiva también (consultar cada caso).\nPor lo que el presupuesto de la compra es: ____ Euros - ____ % x ___ ejemplares = ___ Euros (IVA incluido).\nPor otra parte, presupuesto de corrección/adaptación será de ______ Euros, asumiendo el autor el solo el 60% del precio: ______ Euros.\nTambién te puede interesar la creación de tu obra en formato e-book y su distribución en todas las plataformas digitales. Este tendrá un coste adicional de 100 Euros (los derechos devengados de esta edición, se explicaran en en contrato).\nAdemás, recientemente hemos empezado a enriquecer nuestros libros con audiolibros de calidad máxima cuyos textos están narrados por las mejores actrices y actores de doblaje español. Pídenos información personalizada.\nhttps://www.babidibulibros.com/p/making-of-trending-topic-babidi-bu-para-opinar/\nEste importe se fraccionará en dos pagos (o varios pagos según te convenga), el primero de los mismos, incluirá el total de la corrección.\nDe esta publicación, de los libros que nosotros pongamos en distribución y venta online (NO de los tuyos) recibirás, en concepto de derechos de autor, el ____ % del PVP (sin IVA) del libro.\nNuestro Protocolo de Edición está fundamentado principalmente en la colaboración entre el Autor y la Editorial, de tal forma que se asegure su máxima difusión por ambas partes. Así que necesitamos involucrarte activamente en la promoción de tu libro y, para esto, siempre recomendamos realizar, como mínimo, una presentación oficial en un lugar acordado por ambas partes, y en la que puedas vender tus primeros libros, que servirán de promoción a otras ventas posteriores en librerías.\n\nLa editorial se comprometerá a editar y terminar el libro, consensuando contigo el diseño, la maquetación y la publicación final. Además, se comprometerá a distribuirla en todos nuestros canales de venta y a darle la máxima difusión. Así mismo, nuestros libros son ofertados a licitaciones estatales en países de habla hispana con un notable éxito de ventas. Por último, y todo en paralelo, intentaremos vender los derechos de edición para otros idiomas, llevando nuestro catálogo a las principales ferias internacionales del sector a las que asistimos, y manteniéndote al tanto de cualquier posible negociación.\nUna vez firmado el contrato, el corrector asignado se pondrá en contacto contigo para tratar el tema de la corrección de forma personalizada.\nDespués la obra pasará a nuestro equipo de diseñadores, para que vayan realizando los trabajos de diseño de cubierta y maquetación del libro.\nAl mismo tiempo, se irán realizando también las gestiones de promoción y de pre-distribución (ficha literaria, nota de prensa, redes sociales, etc.), la preparación del BookTrailer, el eBook (opcional) y las gestiones legales necesarias para su publicación (Agencia del ISBN, código de barras, etc.).\nQuedamos atentos a tus noticias y atenderemos cualquier duda o consulta que te surja.\n\nRecibe un cordial saludo.\n	Aceptación para edición y publicación	babidibu
6	2	Buenos días,\n\nQuería saber si recibiste nuestro último email con la información sobre la valoración positiva de tu obra.\n\nPor favor, dime si tienes cualquier duda para que podamos seguir avanzando.\n\nUn saludo,\n	Aceptación para edición y publicación	babidibu
6	3	Hola de nuevo,\n\n¿Recibiste mi email con la información sobre tu libro?\n\nPor parte nuestra queremos seguir avanzando. Dinos algo, por favor.\n\nUn saludo y gracias por tu atención,\n	Aceptación para edición y publicación	babidibu
6	4	Buenos días,\n\nSigo esperando tus comentarios al correo que te envié hace algunos días, ¿lo has recibido?\n\nDime algo para volver a enviártelo si no lo recibiste, o si tienes dudas que aclarar al respecto.\n\nUn saludo.\n	Aceptación para edición y publicación	babidibu
6	5	Buenos días,\n\nSeguimos pendiente desde hace días en recibir comentario alguno sobre la información enviada sobre tu libro. ¿La tienes?\n\nUn saludo.\n	Aceptación para edición y publicación	babidibu
6	6	Buenos días,\n\n¿Has recibido mis últimos emails con la información que te hicimos llegar sobre tu libro?\n\nSigo a tu disposición para lo que precises.\n\nUn saludo y gracias,\n	Aceptación para edición y publicación	babidibu
5	1	Agradecemos el interés que has mostrado por nuestra editorial.\n Tras la recepción de tu obra y tras la primera evaluación por parte de nuestro Dto. de Lecturas, y dado el posible potencial que vemos en ella y su adecuación a nuestra línea editorial, nos complace comunicarte que estaríamos interesados en publicarla dentro de nuestra colección _______________________________ para, posteriormente, promocionarla y distribuirla en nuestros canales (tradicionales y plataformas online).\n ¡¡¡¡¡¡¡¡¡¡¡¡Hablar de la COLECCIÓN.!!! COPIAR DE LA WEB Y PEGAR ENLACE!!!!!!!!!!!!!!!\n Tu libro, aparte de pertenecer a esta colección, formará parte de un sello editorial especializado en literatura infantil. Somos una de las 25 editoriales infantiles más relevantes del panorama español actual, las cuales se caracterizan por ir a la vanguardia de la edición infantil española, creando precedentes y tendencias a imitar por el resto de editoriales.\nVisualiza los siguientes enlaces de interés que avalan lo dicho anteriormente.\nhttps://www.julianmarquina.es/24-editoriales-especializadas-en-libros-infantiles-que-no-puedes-perder-de-vista/\nhttps://blog.babidibulibros.com/f/juvenalia-un-año-más-contará-con-la-participación-babidi-bú\n\n Además, somos la 3ª editorial especializada en literatura infantil y juvenil según la Biblioteca Nacional de España.\nhttp://www.bne.es/es/Micrositios/Guias/Literatura_Infantil/EditorialesLibrerias/EditorialesEspecializadas/\nTambién te será útil visualizar los siguientes videos en los que autores que ya han publicado con BABIDI-BÚ cuentan su experiencia.\nhttps://www.babidibulibros.com/p/videopiniones_65/\n\nNo olvides que formarás parte de un selecto grupo de autores entre los que figuran firmas literarias tan relevantes como Ana Rossetti.\nhttps://blog.babidibulibros.com/f/%C2%BFqu%C3%A9-es-poes%C3%ADa-ana-rossetti\n\nComo ya te hemos comentado el texto de tu obra nos interesa mucho, aunque esta, por el público sensible al que va dirigido, necesita una corrección (esto es normal, lo hacemos con todos textos que nos llegan, hasta con los de autores de extensa trayectoria).\n\nTodas nuestras obras se corrigen de forma obligatoria, esto es muy importante, más aún en literatura infantil. Además, posiblemente podamos enriquecer tu obra con una serie de ideas complementarias y actividades que mejorarán el resultado final, sobre todo de cara al uso del libro en colegios. Esto se tratará de forma personalizada con el corrector asignado.\n Según la colección a la que pertenecería tu obra y para que resulte un producto comercial lo más competitivo, las características técnicas deberían ser:\n\n ENCUADERNACIÓN: TAPA DURA / TAPA BLANDA.\nFORMATO: ____ x ____ mms.\nN.º PÁGINAS (aprox.): _____.\nPAPEL INTERIOR: Offset ahuesado - Offset ahuesado / Offset blanco.\nCOLOR / ByN: número de págs. a color (¡lustraciones + portadilla !!!!!!!).\n\n Por otra parte, según este tipo de libro, hemos calculado que el PVP óptimo deberá ser _______ Euros (IVA incluido).\nTras pasar el filtro indispensable de nuestro Comité de Lectura, pasamos a informarte, de forma personalizada, de las condiciones para lleva a cabo esta publicación.\nEres un autor novel que nos ofrece una obra de calidad que se adecua a nuestra línea editorial y que transmite valores implícitos y, además, vemos en dicha obra el potencial comercial necesario para publicarla, apostando por ella, entre otras. Sin embargo, son muchos los manuscritos que nos llegan al mes con estas mismas características: autores sin trayectoria previa, pero con grandes ideas. Por lo que tenemos que volver a seleccionar, por falta de capacidad para asumirlas todas.\nPor lo tanto, para dar viabilidad inmediata a tu proyecto, mostrando tú también tu confianza en tu obra y en que esta va a funcionar entre el público al que se dirige (comenzando por tu entorno cercano y tu ámbito local de influencia y abriendo, poco a poco, el camino a la distribución nacional), necesitamos que impulses el proyecto con una compra mínima de _________ ejemplares, con un descuento del _______ % del PVP, que te permitirá un beneficio tras su venta directa.\nSi te interesarse comprar más libros por motivos propios (¡sin compromiso!) debes saber que los descuentos de autor sobre los mismos suben de forma progresiva, para que puedas lucrarte con tu compra de forma progresiva también (consultar cada caso).\nPor lo que el presupuesto de la compra es: ____ Euros - ____ % x ___ ejemplares = ___ Euros (IVA incluido).\nPor otra parte, presupuesto de corrección/adaptación será de ______ Euros, asumiendo el autor el solo el 60% del precio: ______ Euros.\nTambién te puede interesar la creación de tu obra en formato e-book y su distribución en todas las plataformas digitales. Este tendrá un coste adicional de 100 Euros (los derechos devengados de esta edición se explicarán en contrato).\nAdemás, recientemente hemos empezado a enriquecer nuestros libros con audiolibros de calidad máxima cuyos textos están narrados por las mejores actrices y actores de doblaje español. Pídenos información personalizada.\nhttps://www.babidibulibros.com/p/making-of-trending-topic-babidi-bu-para-opinar/\n\nEste importe se fraccionará en dos pagos (o varios según te convenga), el primero de los mismos, incluirá el total de la corrección.\nDe esta publicación, de los libros que nosotros pongamos en distribución y venta online (NO de los tuyos) recibirás, en concepto de derechos de autor, el ____ % del PVP (sin IVA) del libro. El otro ___ % será para el ilustrador que colabore en la misma.\nNuestro Protocolo de Edición está fundamentado principalmente en la colaboración entre el Autor y la Editorial, de tal forma que se asegure su máxima difusión por ambas partes. Así que necesitamos involucrarte activamente en la promoción de tu libro y, para esto, siempre recomendamos realizar, como mínimo, una presentación oficial en un lugar acordado por ambas partes, y en la que puedas vender tus primeros libros, que servirán de promoción a otras ventas posteriores en librerías.\nLa editorial se comprometerá a editar y terminar el libro, consensuando contigo el diseño, la maquetación y la publicación final. Además, se comprometerá a distribuirla en todos nuestros canales de venta y a darle la máxima difusión. Así mismo, nuestros libros son ofertados a licitaciones estatales en países de habla hispana con un notable éxito de ventas. Por último, y todo en paralelo, intentaremos vender los derechos de edición para otros idiomas, llevando nuestro catálogo a las principales ferias internacionales del sector a las que asistimos, y manteniéndote al tanto de cualquier posible negociación.\nUna vez firmado el contrato, el corrector asignado se pondrá en contacto contigo para tratar el tema de la corrección de forma personalizada.\nAsí mismo, te ofreceremos un listado de ilustradores colaboradores de la editorial, para que selecciones los que te parezcan más interesantes y, después de firmar contrato con uno de ellos, te pondremos en contacto con él, para que, con el consenso mutuo de ambos, este vaya trabajando en las ilustraciones de la obra.\nCuando tengamos todo el contenido listo (texto e ilustraciones), la obra pasará a nuestro equipo de diseñadores, para que vayan realizando los trabajos de diseño de cubierta y maquetación del libro.\nAl mismo tiempo, se irán realizando también las gestiones de promoción y de pre-distribución (ficha literaria, nota de prensa, redes sociales, etc.), la preparación del BookTrailer, el eBook (opcional) y las gestiones legales necesarias para su publicación (Agencia del ISBN, código de barras, etc.).\nQuedamos atentos a tus noticias y atenderemos cualquier duda o consulta que te surja.\nRecibe un cordial saludo.\n	Aceptación para edición y publicación	babidibu
5	2	Buenos días,\nQuería saber si recibiste nuestro último email con la información sobre la valoración positiva de tu obra.\nPor favor, dime si tienes cualquier duda para que podamos seguir avanzando.\nUn saludo,\n	Aceptación para edición y publicación	babidibu
5	3	Hola de nuevo,\n\n¿Recibiste mi email con la información sobre tu libro?\n\nPor parte nuestra queremos seguir avanzando. Dinos algo, por favor.\n\nUn saludo y gracias por tu atención,\n	Aceptación para edición y publicación	babidibu
5	4	Buenos días,\n\nSigo esperando tus comentarios al correo que te envié hace algunos días, ¿lo has recibido?\n\nDime algo para volver a enviártelo si no lo recibiste, o si tienes dudas que aclarar al respecto.\n\nUn saludo.\n	Aceptación para edición y publicación	babidibu
5	5	Buenos días,\n\nSeguimos pendiente desde hace días en recibir comentario alguno sobre la información enviada sobre tu libro. ¿La tienes?\n\nUn saludo.\n	Aceptación para edición y publicación	babidibu
12	1	Reiteramos el agradecimiento por el interés que has mostrado por nuestra editorial.\nTras la evaluación de tu obra por parte de nuestro Dto. de Lecturas, y dado el posible potencial que vemos en ella y su adecuación a nuestra línea editorial, nos complace comunicarte que estamos interesados en publicarla dentro de nuestro de nuestro sello de New Adults, para posteriormente, promocionarla y distribuirla en nuestros canales (tradicionales y plataformas online).\n\nAun así, el texto necesita una corrección (esto es normal, lo hacemos con todos textos que nos llegan, hasta con los de autores de extensa trayectoria). Todas nuestras obras se corrigen de forma obligatoria, esto es muy importante para no bajar la calidad literaria de nuestras obras.\n\nHemos consensuado que, para que resulte un producto comercial lo más competitivo, las características técnicas deberían ser:\n\nCARACTERÍSTICAS TÉCNICAS:\n\nFORMATO: ____ x ____ mm.\nNº PÁGINAS (aprox.):\nPAPEL INTERIOR: Offset Ahuesado 90 g.\n\nPor otra parte, según este tipo de libro, hemos calculado que el PVP óptimo deberá ser _______ Euros (IVA incluido).\n\nTras pasar el filtro indispensable de nuestro Comité de Lectura, pasamos a informarte, de forma personalizada, de las condiciones para lleva a cabo esta publicación.\n\nEres un autor novel que nos ofrece una obra de calidad que se adecua a nuestra línea editorial y que transmite valores implícitos y, además, vemos en dicha obra el potencial comercial necesario para publicarla, apostando por ella, entre otras. Sin embargo, son muchos los manuscritos que nos llegan al mes con estas mismas características: autores sin trayectoria previa, pero con grandes ideas. Por lo que tenemos que volver a seleccionar, por falta de capacidad para asumirlas todas.\n\nPor lo tanto, para dar viabilidad inmediata a tu proyecto, mostrando tú también tu confianza en tu obra y en que esta va a funcionar entre el público al que se dirige (comenzando por tu entorno cercano y tu ámbito local de influencia y abriendo, poco a poco, el camino a la distribución nacional), necesitamos que impulses el proyecto con una compra mínima de _________ ejemplares, con un descuento del _______ % del PVP.\n\nSi te interesarse comprar más libros por motivos propios (¡sin compromiso!) debes saber que los descuentos de autor sobre los mismos suben de forma progresiva, para que puedas lucrarte con tu compra de forma progresiva también (consultar cada caso).\n\nPor lo que el presupuesto de la compra es: ____ Euros - ____ % x ___ ejemplares = ___ Euros (IVA incluido).\n\nPor otra parte, presupuesto de corrección/adaptación será de ______ Euros + IVA, asumiendo el autor el solo el 60% del precio: ______ Euros + IVA.\n\nEste importe se fraccionará en dos pagos, el primero de los mismos, incluirá el total de la corrección. Aunque, si te resultase más cómoda otra opción, estamos abiertos a cualquier sugerencia.  También contamos con una financiera externa a 12 meses sin intereses.\n\nDe esta publicación, de los libros que nosotros pongamos en distribución y venta online (no de los tuyos) recibirás, en concepto de derechos de autor, el ____ % del PVP (sin IVA) del libro.\n\nNuestro Protocolo de Edición está fundamentado principalmente en la colaboración entre el Autor y la Editorial, de tal forma que se asegure su máxima difusión por ambas partes. Así que necesitamos involucrarte activamente en la promoción de tu libro y, para esto, siempre recomendamos realizar, como mínimo, una presentación oficial en un lugar acordado por ambas partes, y en la que puedas vender tus primeros libros, que servirán de promoción a otras ventas posteriores en librerías.\n\nLa editorial se comprometerá a editar y terminar el libro, consensuando contigo el diseño, la maquetación y la publicación final. Además, se comprometerá a distribuirla en todos nuestros canales de venta y a darle la máxima difusión. Así mismo, nuestros libros son ofertados a licitaciones estatales en países de habla hispana con un notable éxito de ventas. Por último, y todo en paralelo, intentaremos vender los derechos de edición para otros idiomas, llevando nuestro catálogo a las principales ferias internacionales del sector a las que asistimos, y manteniéndote al tanto de cualquier posible negociación.\n\nPara proceder a la redacción del contrato de edición, por favor, mándanos los siguientes datos:\n\nResumen datos de autor (un cuadro por autor, si hay más de uno):\nNombre fiscal:\nNombre comercial: El que aparecerá en el libro y webs\nTipo de autor: Escritor / Ilustrador / Escritor-ilustrador\nDNI: Sin espacios ni guiones\nCuenta bancaria (I.B.A.N): Para pago de royalties. Solo autores residentes en la UE.\nCuenta Paypal: Para pago de royalties. Autores no residentes en la UE.\nTeléfono(s):\nEmail:\nDirección: lo más clara posible, sobre todo para autores no residentes en España.\nC.P:\nLocalidad:\nProvincia / estado:\nPaís:\nAño de nacimiento:\nLugar de nacimiento:\nWeb:\nNecesito factura de empresa/autónomo: SÍ / No.\n\nSi necesitas factura, y los datos de facturación son distintos a los datos de autor, es muy importante que nos lo comuniques ahora.\n\nSi tus datos de beneficiario de royalties son distintos a los anteriores (Autor y/o cliente) háznoslo saber ahora, en caso contrario entenderemos que el beneficiario es el autor.\n\nUna vez firmado el contrato, el corrector asignado se pondrá en contacto contigo para tratar el tema de la corrección de forma personalizada.\n\nDespués la obra pasará a nuestro equipo de diseñadores, para que vayan realizando los trabajos de diseño de cubierta y maquetación del libro.\n\nAl mismo tiempo, se irán realizando también las gestiones de promoción y de pre-distribución (ficha literaria, nota de prensa, redes sociales, etc.), la preparación del BookTrailer, el eBook (opcional) y las gestiones legales necesarias para su publicación (Agencia del ISBN, código de barras, etc.).\n\nQuedamos atentos a tus noticias y atenderemos cualquier duda o consulta que te surja.\n\nRecibe un cordial saludo.	Aceptacion para edicion y publicacion	mirahadas
12	2	Buenos días,\n\nQuería saber si recibiste nuestro último email con la información sobre la valoración positiva de tu obra.\n\nPor favor, dime si tienes cualquier duda para que podamos seguir avanzando.\n\nUn saludo.	Aceptacion para edicion y publicacion	mirahadas
12	3	Hola de nuevo,\n\n¿Recibiste mi email con la información sobre tu libro?\n\nPor parte nuestra queremos seguir avanzando. Dinos algo, por favor.\n\nUn saludo y gracias por tu atención,	Aceptacion para edicion y publicacion	mirahadas
12	4	Hola,\n\nSigo esperando tus comentarios al correo que te envié hace algunos días, ¿lo has recibido?\n\nDime algo para volver a enviártelo si no lo recibiste, o si tienes dudas que aclarar al respecto.\n\nUn saludo.	Aceptacion para edicion y publicacion	mirahadas
12	5	Buenos días,\n\nSeguimos pendiente desde hace días en recibir comentario alguno sobre la información enviada sobre tu libro. ¿La tienes?\n\nUn saludo.	Aceptacion para edicion y publicacion	mirahadas
12	6	Hola,\n\n¿Has recibido mis últimos emails con la información que te hicimos llegar sobre tu libro?\n\nSigo a tu disposición para lo que precises.\n\nUn saludo y gracias.	Aceptacion para edicion y publicacion	mirahadas
10	4	Hola [Autor/a],\n\nSigo a la espera de recibir el contrato de edición que te enviamos hace algunos días, ¿lo has recibido?\n\nDime si lo has recibido o no para reenviártelo de nuevo. Y si lo has recibido, te recuerdo que estamos a tu entera disposición para resolver cualquier duda que puedas tener o cualquier dificultad que tengas para enviárnoslo. Nosotros nos podemos hacer cargo del envío y te mandaremos un mensajero si no puedes enviárnoslo por email.\n\nEsperamos tus comentarios,\n	Contrato de edición	mirahadas
10	5	Muy buenas [Autor/a],\n\nLlevamos algunos días pendientes de recibir el contrato de edición firmado de tu parte.\n\nNo sabemos si lo has recibido. Sigo a tu disposición y espero cualquier comentario de tu parte.\n\nUn saludo y gracias de nuevo por tu atención,\n\n	Contrato de edición	mirahadas
10	6	Hola de nuevo [Autor/a],\n\nSigo a la espera para que nos hagas llegar cualquier cuestión o duda a resolver respecto al contrato de edición que te enviamos hace algunas semanas ya.\n\nSuponemos que no habrás tenido ocasión de ver nuestros emails. Te lo podemos reenviar de nuevo si nos lo indicas.\n\nQuedo a tu entera disposición.\n\nUn saludo y gracias por tu atención,\n\n	Contrato de edición	mirahadas
13	4	Hola [Autor/a],\n\nSigo esperando tus comentarios al correo que te envié hace algunos días, ¿lo has recibido?\n\nDime algo para reenviártelo de nuevo y si tienes alguna cosa que aclarar al respecto.\n\nUn saludo y gracias de nuevo.\n\n	Contacto Mirahadas	mirahadas
13	5	Hola [Autor/a],\n\nSigo pendiente desde hace días en recibir algún comentario respecto a la información sobre tu libro que te enviamos. ¿La recibiste?\n\nUn saludo,\n\n	Contacto Mirahadas	mirahadas
13	6	Hola [Autor/a],\n\n¿Has recibido nuestros emails respecto a la información que te hicimos llegar sobre tu libro? \n\nSeguimos a tu disposición para lo que necesites.\n\nUn saludo y gracias,\n\n	Contacto Mirahadas	mirahadas
3	1	Buenos días,\n\nTe envío el contrato de edición completo y firmado por nuestra parte.\n\nCuando lo tengas firmado nos lo puedes devolver escaneado vía email a esta misma dirección.\n\nSi hay alguna cosa que se nos haya pasado o algo que haya que modificar, por favor, dímela.\n\nQuedamos a la espera,\n\nUn saludo y gracias.\n	Contrato de edición	babidibu
3	2	Hola [Autor/a],\n\n¿Has recibido el email con el contrato de edición? No tengo constancia de tu respuesta y seguimos pendientes de recibir el contrato.\n\nHazme llegar cualquier duda al respecto para seguir avanzando.\n\nUn saludo y gracias por tu atención,\n	Contrato de edición	babidibu
7	1	Escribe aqui	Contacto Babidi-Bú	babidibu
7	2	Hola [Autor/a],\n\nQuería saber si recibiste nuestro email con la información requerida sobre tu libro.\n\nPor favor, hazme llegar cualquier duda o comentario para que podamos seguir avanzando.\n\nUn saludo y gracias por tu atención.\n	Contacto Babidi-Bú	babidibu
7	3	Hola de nuevo [Autor/a],\n\n¿Recibiste mi email con la información requerida respecto a tu libro?\n\nPor parte de la Editorial, queremos seguir avanzando. Dinos algo al respecto.\n\nUn saludo y gracias por tu atención.\n	Contacto Babidi-Bú	babidibu
7	4	Hola [Autor/a],\n\nSigo esperando tus comentarios al correo que te envié hace algunos días, ¿lo has recibido?\n\nDime algo para reenviártelo de nuevo y si tienes alguna cosa que aclarar al respecto.\n\nUn saludo y gracias de nuevo.\n	Contacto Babidi-Bú	babidibu
7	5	Hola [Autor/a],\n\nSigo pendiente desde hace días en recibir algún comentario respecto a la información sobre tu libro que te enviamos. ¿La recibiste?\n\nUn saludo,\n	Contacto Babidi-Bú	babidibu
7	6	Hola [Autor/a],\n\n¿Has recibido nuestros emails respecto a la información que te hicimos llegar sobre tu libro?\n\nSeguimos a tu disposición para lo que necesites.\n\nUn saludo y gracias,\n	Contacto Babidi-Bú	babidibu
10	1	Te envío el contrato de edición completo y firmado por nuestra parte.\n\nCuando lo tengas firmado, nos lo puedes devolver escaneado vía email a esta misma dirección.\n\nSi hay alguna cosa que se nos haya pasado o algo que haya que modificar, por favor, dímela.\n\nQuedamos a la espera.\n\nUn saludo y gracias.\n	Contrato de edición	mirahadas
3	3	Hola de nuevo [Autor/a],\n\nQuería saber si recibiste nuestro email enviado hace algunos días con el contrato de edición. No tengo constancia de haber recibido respuesta de tu parte y quiero saber si finalmente vas a editar con nosotros.\n\nTe recuerdo que puedes enviárnoslo por email o, si lo deseas y nos avisas, podemos mandar un mensajero a recogerlo a donde nos digas.\n\nHazme llegar cualquier comentario al respecto.\n\nEsperamos tu respuesta.\n\nGracias por tu atención,\n	Contrato de edición	babidibu
3	4	Hola [Autor/a],\n\nSigo a la espera de recibir el contrato de edición que te enviamos hace algunos días, ¿lo has recibido?\n\nDime si lo has recibido o no para reenviártelo de nuevo. Y si lo has recibido, te recuerdo que estamos a tu entera disposición para resolver cualquier duda que puedas tener o cualquier dificultad que tengas para enviárnoslo. Nosotros nos podemos hacer cargo del envío y te mandaremos un mensajero si no puedes enviárnoslo por email.\n\nEsperamos tus comentarios,\n	Contrato de edición	babidibu
4	1	Buenos días,\n\nContactaste hace algún tiempo con nosotros porque estabas ultimando un libro.\n\nQuería saber cómo llevabas el proyecto editorial ¿Finalmente lo terminaste?\n\nNos interesaría poder valorarlo para ver si encaja en alguna de nuestras colecciones.\n\nSi tienes cualquier duda, puedes llamarme al teléfono abajo indicado.\n\nUn saludo y gracias por tu atención.\n\nEspero tus comentarios.\n	Proyecto editorial	babidibu
11	1	Buenos días,\n\nContactaste hace algún tiempo con nosotros porque estabas ultimando un libro.\n\nQuería saber cómo llevabas el proyecto editorial ¿Finalmente lo terminaste?\n\nNos interesaría poder valorarlo para ver si encaja en alguna de nuestras colecciones.\n\nSi tienes cualquier duda, puedes llamarme al teléfono abajo indicado.\n\nUn saludo y gracias por tu atención.\n\nEspero tus comentarios.\n	Proyecto editorial	mirahadas
11	2	Hola [Autor/a],\n\n¿Recibiste nuestro email con solicitud de información sobre tu proyecto editorial?\n\nEstaríamos encantados de poder valorarlo…\n\nHazme llegar cualquier duda o comentario que puedas tener.\n\nUn saludo,\n	Proyecto editorial	mirahadas
11	3	Hola [Autor/a],\n\nTe recuerdo que estamos esperando una respuesta a nuestro email enviado hace algunos días.\n\nDinos algo, por favor.\n\nUn saludo y gracias por tu atención,\n	Proyecto editorial	mirahadas
2	1	Buenos días,\n\nPara proceder a la redacción del contrato de edición, por favor, mándanos los siguientes datos:\n\nResumen datos de autor (un cuadro por autor, si hay más de uno):\n\nTitulo de la obra:\nNombre fiscal:\nNombre comercial: El que aparecerá en el libro y webs\nTipo de autor: Escritor / Ilustrador / Escritor-ilustrador\nDNI: Sin espacios ni guiones\nCuenta bancaria (I.B.A.N): Para pago de royalties. Solo autores residentes en la UE.\nCuenta Paypal: Para pago de royalties. Autores no residentes en la UE.\nTeléfono(s):\nEmail:\nDirección: lo más clara posible, sobre todo para autores no residentes en España.\nC.P:\nLocalidad:\nProvincia / Estado:\nPaís:\nAño de nacimiento:\nLugar de nacimiento:\nWeb:\nNecesito factura de empresa/autónomo: SÍ / No.\n\nEn el caso de que tú aportes al ilustrador, ¿quieres que este aparezca en la cubierta del libro, en los créditos y en la web?: Sí / No.\nSi la respuesta es "SÍ", no olvides mandarnos su nombre completo, nombre comercial (si lo tiene), biografía, fotografía y año de nacimiento durante el proceso de maquetación (NO AHORA).\n\nSi necesitas factura, y los datos de facturación son distintos a los datos de autor, es muy importante que nos lo comuniques ahora.\n\nSi tus datos de beneficiario de royalties son distintos a los anteriores (Autor y/o cliente) háznoslo saber ahora, en caso contrario entenderemos que el beneficiario es el autor.\n\nQuedamos atentos a tus noticias y atenderemos cualquier duda o consulta que te surja.\n\nRecibe un cordial saludo.\n	Datos para contrato de edición	babidibu
3	5	Muy buenas [Autor/a],\n\nLlevamos algunos días pendientes de recibir el contrato de edición firmado de tu parte.\n\nNo sabemos si lo has recibido. Sigo a tu disposición y espero cualquier comentario de tu parte.\n\nUn saludo y gracias de nuevo por tu atención,\n	Contrato de edición	babidibu
3	6	Hola de nuevo [Autor/a],\n\nSigo a la espera para que nos hagas llegar cualquier cuestión o duda a resolver respecto al contrato de edición que te enviamos hace algunas semanas ya.\n\nSuponemos que no habrás tenido ocasión de ver nuestros emails. Te lo podemos reenviar de nuevo si nos lo indicas.\n\nQuedo a tu entera disposición.\n\nUn saludo y gracias por tu atención,\n	Contrato de edición	babidibu
5	6	Buenos días,\n¿Has recibido mis últimos emails con la información que te hicimos llegar sobre tu libro?\n\nSigo a tu disposición para lo que precises.\n\nUn saludo y gracias,\n	Aceptación para edición y publicación	babidibu
11	5	Hola [Autor/a],\n\nLlevo varios días sin recibir ningún comentario sobre la información sobre tu proyecto editorial. ¿recibiste mis emails?\n\nUn saludo,\n	Proyecto editorial	mirahadas
13	1	Escribe aqui	Contacto Mirahadas	mirahadas
8	1	Agradecemos el interés que has mostrado por nuestra editorial.\n\nHemos recibido tu obra correctamente y esta se encuentra a la espera de evaluación por parte de nuestro Dpto. de Lectura, que será el que determine su adecuación a nuestra línea editorial.\n\nSi es posible, te solicitamos que nos reenvíes el cuadro que te adjuntamos, debidamente cumplimentado con tus datos como autor y con los datos de tu obra. Para nuestro Comité de Lecturas, este cuadro es una herramienta indispensable de control y organización administrativa que se anexiona al expediente de cada obra durante todo el proceso de la evaluación.\n\nSi te surgiera cualquier duda al respecto, estaremos encantados de resolverla. Nuestros teléfonos son el 912665684 y el 653754771, o bien puedes pedirnos por mail que te llamemos a un tlf de contacto en tu horario preferente.\n\nEn cualquier caso, en unos días nos pondremos en contacto contigo, tras la valoración.\n\nUn saludo y muchas gracias por tu atención.\n	Pre-evaluacion y condiciones.	mirahadas
11	4	Hola [Autor/a],\n\nSeguimos esperando tus comentarios al email que te enviamos hace ya algunos días, ¿lo recibiste?\n\nHázmelo saber para reenviártelo de nuevo, y dime si tienes alguna duda al respecto.\n\nUn saludo y gracias de nuevo,\n	Proyecto editorial	mirahadas
11	6	Hola [Autor/a],\n\n¿Has recibido nuestros últimos emails? \n\nSeguimos a tu entera disposición para lo que necesites.\n\nUn saludo y gracias,\n	Proyecto editorial	mirahadas
8	2	Hola [Autor/a],\n\nQuería saber si recibiste mi email de pre-evaluación de tu obra y la solicitud del cuadro informativo para poder continuar con el proceso de valoración, si nuestras condiciones de edición fueran de tu interés.\n\nNo he recibido respuesta aún y continuamos a la espera de tus posibles comentarios.\n\nSi lo prefieres, podemos hablar por tlf. Puedes llamarnos al 912.665.684 o al 653.754.771, para solventar las dudas que puedas tener o darnos tu tlf para llamarte.\n\nUn saludo y gracias por tu atención.\n\n	Pre-evaluacion y condiciones.	mirahadas
8	3	Hola [Autor/a],\n\n¿Recibiste nuestro email con nuestro Protocolo de Edición?\n\nHazme llegar cualquier duda o comentario que puedas tener\n.\nUn saludo,\n\n	Pre-evaluacion y condiciones.	mirahadas
8	4	Hola [Autor/a],\n\nTe recuerdo que estamos esperando una respuesta a nuestro email enviado hace algunos días.\n\nNosotros queremos seguir avanzando con la valoración de tu libro. Dinos algo al respecto.\n\nUn saludo y gracias por tu atención.\n\n	Pre-evaluacion y condiciones.	mirahadas
8	5	Hola [Autor/a],\n\nSeguimos esperando tus comentarios al email que te enviamos hace ya algunos días, ¿lo recibiste?\n\nHázmelo saber para reenviártelo de nuevo y dime si tienes alguna duda al respecto.\n\nUn saludo y gracias de nuevo,\n\n	Pre-evaluacion y condiciones.	mirahadas
1	2	Hola [Autor/a],\n\nQuería saber si recibiste mi email de pre-evaluación de tu obra y la solicitud del cuadro informativo para poder continuar con el proceso de valoración, si nuestras condiciones de edición fueran de tu interés.\n\nNo he recibido respuesta aún y continuamos a la espera de tus posibles comentarios.\n\nSi lo prefieres, podemos hablar por tlf. Puedes llamarnos al 912.665.684 o al 653.754.771, para solventar las dudas que puedas tener o darnos tu tlf para llamarte.\n\nUn saludo y gracias por tu atención.\n\n	Pre-evaluación y condiciones	babidibu
1	3	Hola [Autor/a],\n\n¿Recibiste nuestro email con nuestro Protocolo de Edición?\n\nHazme llegar cualquier duda o comentario que puedas tener\n.\nUn saludo,\n\n	Pre-evaluación y condiciones	babidibu
1	4	Hola [Autor/a],\n\nTe recuerdo que estamos esperando una respuesta a nuestro email enviado hace algunos días.\n\nNosotros queremos seguir avanzando con la valoración de tu libro. Dinos algo al respecto.\n\nUn saludo y gracias por tu atención.\n\n	Pre-evaluación y condiciones	babidibu
1	5	Hola [Autor/a],\n\nSeguimos esperando tus comentarios al email que te enviamos hace ya algunos días, ¿lo recibiste? \n\nHázmelo saber para reenviártelo de nuevo y dime si tienes alguna duda al respecto.\n\nUn saludo y gracias de nuevo,\n\n	Pre-evaluación y condiciones	babidibu
\.


--
-- Data for Name: comentarios; Type: TABLE DATA; Schema: public; Owner: manu
--

COPY public.comentarios (emailautor, usernameeditor, comentario, fecha) FROM stdin;
m.francosanchez24@gmail.com	admin	comentario prueba	2018-10-25 16:42:17.884322
m.francosanchez24@gmail.com	admin	comentarios 2	2018-10-25 16:42:29.08619
m.francosanchez24@gmail.com	admin	Lo tengo que llamar el viernes	2018-10-25 16:43:17.154238
susrizo@yahoo.es	paz	Ella es la representante del autor y por ahora dice que no pueden. 	2018-11-05 09:32:52.704893
caryom2017@gmail.com	paz	Se lo está pensando y hablamos esta semana. 	2018-11-05 09:39:04.369846
nueva-via@hotmail.com	paz	Se queja de los porcentajes y Marta le responde en mi nombre.	2018-11-05 09:44:35.931278
paumezquita@hotmail.com	paz	No tiene dinero	2018-11-05 09:50:56.64629
info@marcovaleriolama.com	Paz	Está pensándoselo	2018-11-07 11:21:20.923221
sylviamelo.as@gmail.com	Paz	Le digo que lo reduzca	2018-11-08 11:04:26.63721
krismaec@gmail.com	Maria	que bella es mi presidenta	2018-11-12 12:18:32.223452
mota.ja@outlook.com	Paz	Pregunta para enviarme poesía	2018-11-14 09:05:05.03645
anamizquierdo@ono.com	Paz	Le propongo división o reducción	2018-11-15 11:50:39.256432
ratakeli@outlook.com	Paz	Quiere auto-edición porque lo cobra entero y que nosotros no le garantizamos nada de distribución	2018-11-16 12:14:38.839961
silentmml25@gmail.com	Paz	Lo recupero de la web y le pido los datos. Le envío el protocolo de edición. 	2018-11-16 12:22:21.173729
pueblasdelgados@gmail.com	Paz	Recupero de la web. La obra ya la tiene publicada pero puede enviar otras. 	2018-11-16 12:25:21.328222
anamizquierdo@ono.com	Paz	Tiene un familiar enfermo y no puede atenderme en estos momentos. Lo apunto para diciembre	2018-11-16 12:31:14.9479
quinopena@hotmail.com	Paz	Tiene problemas personales, ya lo retomaremos. Emplazo a un par de meses	2018-11-19 09:29:32.953325
delia.bento@gmail.com	Paz	Nos envía una obra que no es 	2018-11-19 10:00:01.94084
sylviamelo.as@gmail.com	Paz	Se lo divido en pág 243 (cap 77). Pero le digo que incluso se podría dividir en 3	2018-11-19 11:41:41.69089
LCH179102XVII@gmail.com	Paz	Le llamo y no tengo respuesta. Le aviso por e-mail, si mañana no tengo respuesta le vuelvo a llamar. 	2018-11-20 10:28:29.132066
LCH179102XVII@gmail.com	Paz	Le envío contrato	2018-11-21 10:00:17.45733
rubenymontse7@gmail.com	Paz	Es la que envía el libro página por página. Ya está diciendo que no quiere pagar	2018-11-21 10:20:00.992168
mota.ja@outlook.com	Paz	Pendiente de portada	2018-11-21 10:25:46.189035
teamchilalynn@gmail.com	Paz	Ella es la representante	2018-11-21 10:28:37.933909
LordyMesias@gmail.com	Paz	Envía muchísimas obras.	2018-11-26 10:47:15.082774
nikkho@gmail.com	Paz	Autor de Contrareloj, le digo que finalmente no podemos adquirir proyectos propios. 	2018-11-26 11:56:54.684565
mrojas.escritora@gmail.com	Paz	Es de BBB. Le envío aceptación.  SU MANUSCRITO ESTÁ ORGANIZADO EN 35 PÁGINAS PERO HABRÍA QUE ILUSTRARSELO POR LO QUE SE LO DEJO EN 28 + 8 , ES DECIR, HAY QUE REESTRUCTURAR EL TEXTO. 	2018-11-27 09:32:09.193891
mrojas.escritora@gmail.com	Maria	Hablo con ella por teléfono, es mejicana pero vive en Suecia	2018-11-27 13:51:20.187392
trimonant@gmail.com	Paz	Pendiente de que me envíe obra	2018-12-05 09:28:26.416089
maestrorealmanuel@icloud.com	Paz	Envía varias obras , a la espera del cuadro para las portadas	2018-12-11 10:39:42.215139
terapiasutu@gmx.es	Paz	Manda datos como si fuera autoedición. 	2018-12-13 11:06:29.44361
manuelmac78@msn.com	Paz	No puede asumir el coste. Emplazo. 	2018-12-13 12:07:38.109829
\.


--
-- Data for Name: editores; Type: TABLE DATA; Schema: public; Owner: manu
--

COPY public.editores (username, password) FROM stdin;
admin	$2b$12$bsYA3ucv0xWAsHYEOAen5eVUd18UpgiiraB/15q5x8q7UakAcXI3q
marta	$2b$12$zENSsCx0c4ZFQA7K1TebMeOeEEuSho9J4B6B8hJ/M5FkX16rsF71S
paz	$2b$12$UY3zqo1xfRmLQsU5M4AE1Om4bha8hRbQ0eeLQccXcDHugnHE9U/m2
Maria	$2b$12$MQUAGhZ5ndKp8.toWpWotOpt6z15GcdrCvkUroL/kKIAV.5lz50gq
Paz	$2b$12$XOUBobK8UWy3AimnYB3DlODjZAYRtN2uvXGU.BcRhUEvbqh6SqeD6
Marta	$2b$12$c0x7Od9FwMEPIPIFX7jYiee1xgHOt9unUOtV6l620oHOBjp.HdF6u
rafa	$2b$12$ya8uxIi.BbJgmqOp8nQXseg2.9nR0DbtkCeTQVNrPC7o1pQ98O.ea
Rafa	$2b$12$Dq/MFVhqCgxJ2xuTOi3xjOwPqsVrVPKdrtJzzG91zoOaun7XdsAvu
Pablo	$2b$12$HDvvMmchZTfVtSCYp4Px4uwDlNXWFJu2yU3oUwWOI2nsWctib4H7C
\.


--
-- Data for Name: editorialdeleditor; Type: TABLE DATA; Schema: public; Owner: manu
--

COPY public.editorialdeleditor (nombreeditorial, usernameeditor) FROM stdin;
\.


--
-- Data for Name: editoriales; Type: TABLE DATA; Schema: public; Owner: manu
--

COPY public.editoriales (nombre) FROM stdin;
Babidi-Bú
Mirahadas
\.


--
-- Data for Name: envios_correo; Type: TABLE DATA; Schema: public; Owner: manu
--

COPY public.envios_correo (emailautor, idcorreo, fecha_envio, frecuencia, correo_actual, texto, fichero) FROM stdin;
cira12@hotmail.es	8	2018-12-20 00:00:00	7	2	Buenos días Sonia\r\n\r\nAgradecemos el interés que has mostrado por nuestro grupo editorial. Hemos recibido tu obra correctamente y esta se encuentra a la espera de evaluación por parte de nuestro Dpto. de Lectura, que será el que determine su adecuación a nuestra línea editorial.\r\n\r\nTe solicitamos que nos reenvíes el cuadro que te adjuntamos, debidamente cumplimentado con tus datos como autor y con los datos de tu obra. Para nuestro Comité de Lecturas, este cuadro es una herramienta indispensable de control y organización administrativa que se anexiona al expediente de cada obra durante todo el proceso de la evaluación.  \r\n\r\nPor otra parte nos gustaría comentarte que somos una editorial pequeña y nuestros presupuestos como tal son también reducidos. Es por ello que nos gustaría conocer si por tu parte estarías interesado/a en adquirir ejemplares de tu obra con un descuento sobre el PVP para poder así impulsarla. Esto es muy importante para nuestro Dpto  Financiero ya que así podemos evaluar cada manuscrito teniendo en cuenta sus posibilidad inmediatas de ser publicado o no.\r\n\r\nTambién nos gustaría que nos hablaras un poco sobre tus contactos, es decir, ¿por casualidad tienes contactos con centros cívicos, colegios, grupos de lectura…? Cualquier factor que pueda hacer que tu obra se difunda mucho más rápido es muy importante para nosotros.\r\n\r\nSi te surgiera cualquier duda al respecto, estaremos encantados de resolverla. Nuestros teléfonos son el 912.665.684 y el 653.754.771, o bien puedes pedirnos por mail que te llamemos a un teléfono de contacto en tu horario preferente.\r\n\r\nUn saludo y muchas gracias por tu atención.\r\n\r\nPaz M.	CUADRO_PRE-EVALUACION.docx
rubenymontse7@gmail.com	12	2018-12-19 00:00:00	7	3	Buenos días Montserrat \r\n\r\nReiteramos el agradecimiento por el interés que has mostrado por nuestra editorial.\r\nTras la evaluación de tu obra por parte de nuestro Dto. de Lecturas, y dado el posible potencial que vemos en ella y su adecuación a nuestra línea editorial, nos complace comunicarte que estamos interesados en publicarla dentro de nuestro de nuestro sello de New Adults, para posteriormente, promocionarla y distribuirla en nuestros canales (tradicionales y plataformas online).\r\n\r\nAun así, el texto necesita una corrección (esto es normal, lo hacemos con todos textos que nos llegan, hasta con los de autores de extensa trayectoria). Todas nuestras obras se corrigen de forma obligatoria, esto es muy importante para no bajar la calidad literaria de nuestras obras.\r\n\r\nHemos consensuado que, para que resulte un producto comercial lo más competitivo, las características técnicas deberían ser:\r\n\r\nCARACTERÍSTICAS TÉCNICAS:\r\n\r\nFORMATO: 160 x 235 mm.\r\nNº PÁGINAS (aprox.): 80\r\nPAPEL INTERIOR: Offset Ahuesado 90 g.\r\n\r\nPor otra parte, según este tipo de libro, hemos calculado que el PVP óptimo deberá ser 10,95 Euros (IVA incluido).\r\n\r\nTras pasar el filtro indispensable de nuestro Comité de Lectura, pasamos a informarte, de forma personalizada, de las condiciones para lleva a cabo esta publicación.\r\n\r\nEres un autor que nos ofrece una obra de calidad que se adecua a nuestra línea editorial y que transmite valores implícitos y, además, vemos en dicha obra el potencial comercial necesario para publicarla, apostando por ella, entre otras. Sin embargo, son muchos los manuscritos que nos llegan al mes con estas mismas características. Por lo que tenemos que volver a seleccionar, por falta de capacidad para asumirlas todas.\r\n\r\nPor lo tanto, para dar viabilidad inmediata a tu proyecto, mostrando tú también tu confianza en tu obra y en que esta va a funcionar entre el público al que se dirige (comenzando por tu entorno cercano y tu ámbito local de influencia y abriendo, poco a poco, el camino a la distribución nacional), necesitamos que impulses el proyecto con una compra mínima de 100 ejemplares, con un descuento del 25 % del PVP.\r\n\r\nSi te interesarse comprar más libros por motivos propios (¡sin compromiso!) debes saber que los descuentos de autor sobre los mismos suben de forma progresiva, para que puedas lucrarte con tu compra de forma progresiva también (consultar cada caso).\r\n\r\nPor lo que el presupuesto de la compra es: 10,95 Euros - 25 % x 100 ejemplares = 821,25 Euros (IVA incluido).\r\n\r\nPor otra parte, presupuesto de corrección/adaptación será de 262 Euros + IVA, asumiendo el autor el solo el 60% del precio: 157 Euros + IVA.\r\n\r\nEste importe se fraccionará en dos pagos, el primero de los mismos, incluirá el total de la corrección. Aunque, si te resultase más cómoda otra opción, estamos abiertos a cualquier sugerencia.  También contamos con una financiera externa a 12 meses sin intereses.\r\n\r\nDe esta publicación, de los libros que nosotros pongamos en distribución y venta online (no de los tuyos) recibirás, en concepto de derechos de autor, el 10 % del PVP (sin IVA) del libro.\r\n\r\nNuestro Protocolo de Edición está fundamentado principalmente en la colaboración entre el Autor y la Editorial, de tal forma que se asegure su máxima difusión por ambas partes. Así que necesitamos involucrarte activamente en la promoción de tu libro y, para esto, siempre recomendamos realizar, como mínimo, una presentación oficial en un lugar acordado por ambas partes, y en la que puedas vender tus primeros libros, que servirán de promoción a otras ventas posteriores en librerías.\r\n\r\nLa editorial se comprometerá a editar y terminar el libro, consensuando contigo el diseño, la maquetación y la publicación final. Además, se comprometerá a distribuirla en todos nuestros canales de venta y a darle la máxima difusión. Así mismo, nuestros libros son ofertados a licitaciones estatales en países de habla hispana con un notable éxito de ventas. Por último, y todo en paralelo, intentaremos vender los derechos de edición para otros idiomas, llevando nuestro catálogo a las principales ferias internacionales del sector a las que asistimos, y manteniéndote al tanto de cualquier posible negociación.\r\n\r\nPara proceder a la redacción del contrato de edición, por favor, mándanos los siguientes datos:\r\n\r\nResumen datos de autor (un cuadro por autor, si hay más de uno):\r\nNombre fiscal:\r\nNombre comercial: El que aparecerá en el libro y webs\r\nTipo de autor: Escritor / Ilustrador / Escritor-ilustrador\r\nDNI: Sin espacios ni guiones\r\nCuenta bancaria (I.B.A.N): Para pago de royalties. Solo autores residentes en la UE.\r\nCuenta Paypal: Para pago de royalties. Autores no residentes en la UE.\r\nTeléfono(s):\r\nEmail:\r\nDirección: lo más clara posible, sobre todo para autores no residentes en España.\r\nC.P:\r\nLocalidad:\r\nProvincia / estado:\r\nPaís:\r\nAño de nacimiento:\r\nLugar de nacimiento:\r\nWeb:\r\nNecesito factura de empresa/autónomo: SÍ / No.\r\n\r\nSi necesitas factura, y los datos de facturación son distintos a los datos de autor, es muy importante que nos lo comuniques ahora.\r\n\r\nSi tus datos de beneficiario de royalties son distintos a los anteriores (Autor y/o cliente) háznoslo saber ahora, en caso contrario entenderemos que el beneficiario es el autor.\r\n\r\nUna vez firmado el contrato, el corrector asignado se pondrá en contacto contigo para tratar el tema de la corrección de forma personalizada.\r\n\r\nDespués la obra pasará a nuestro equipo de diseñadores, para que vayan realizando los trabajos de diseño de cubierta y maquetación del libro.\r\n\r\nAl mismo tiempo, se irán realizando también las gestiones de promoción y de pre-distribución (ficha literaria, nota de prensa, redes sociales, etc.), la preparación del BookTrailer, el eBook (opcional) y las gestiones legales necesarias para su publicación (Agencia del ISBN, código de barras, etc.).\r\n\r\nQuedamos atentos a tus noticias y atenderemos cualquier duda o consulta que te surja.\r\n\r\nRecibe un cordial saludo. 	False
diana_garcia_21@hotmail.com	8	2018-12-20 00:00:00	7	2	Buenos días Diana\r\n\r\nAgradecemos el interés que has mostrado por nuestro grupo editorial. Hemos recibido tu obra correctamente y esta se encuentra a la espera de evaluación por parte de nuestro Dpto. de Lectura, que será el que determine su adecuación a nuestra línea editorial.\r\n\r\nPor otra parte nos gustaría comentarte que somos una editorial pequeña y nuestros presupuestos como tal son también reducidos. Es por ello que nos gustaría conocer si por tu parte estarías interesado/a en adquirir ejemplares de tu obra con un descuento sobre el PVP para poder así impulsarla. Esto es muy importante para nuestro Dpto  Financiero ya que así podemos evaluar cada manuscrito teniendo en cuenta sus posibilidad inmediatas de ser publicado o no. \r\n\r\nTambién nos gustaría que nos hablaras un poco sobre tus contactos, es decir, ¿por casualidad tienes contactos con centros cívicos, colegios, grupos de lectura…? Cualquier factor que pueda hacer que tu obra se difunda mucho más rápido es muy importante para nosotros. \r\n\r\nSi te surgiera cualquier duda al respecto, estaremos encantados de resolverla. Nuestros teléfonos son el 912.665.684 y el 653.754.771, o bien puedes pedirnos por mail que te llamemos a un teléfono de contacto en tu horario preferente.\r\n\r\nUn saludo y muchas gracias por tu atención.\r\n\r\nPaz M.\r\n\r\n 	False
nahuel.exposito@yahoo.com.ar	7	2018-12-23 00:00:00	6	4	Agradecemos el interés que has mostrado por nuestra editorial.\r\n\r\nMándanos tu obra a ser posible en formato Word o Pdf, con las ilustraciones (si las tienes) y lo pasaremos a nuestro Dpto. de Lectura, para que lo valore.\r\nEsperando tu respuesta, recibe un saludo.\r\n\r\nMaría C\r\n\r\n	False
lapinadefraybentos@hotmail.com	1	2018-12-24 00:00:00	7	2	Buenos días Celia\r\n\r\nAgradecemos el interés que has mostrado por nuestra editorial.\r\n\r\nMándanos tu obra a ser posible en formato Word o Pdf, con las ilustraciones (si las tienes) y lo pasaremos a nuestro Dpto. de Lectura, para que lo valore\r\n\r\nSi es posible, te solicitamos que nos reenvíes el cuadro que te adjuntamos, debidamente cumplimentado con tus datos como autor y con los datos de tu obra. Para nuestro Comité de Lecturas, este cuadro es una herramienta indispensable de control y organización administrativa que se anexiona al expediente de cada obra durante todo el proceso de la evaluación.\r\n\r\nSi te surgiera cualquier duda al respecto, estaremos encantados de resolverla. Nuestros teléfonos son el 912665684 y el 653754771, o bien puedes pedirnos por mail que te llamemos a un tlf de contacto en tu horario preferente.\r\n\r\nEn cualquier caso, en unos días nos pondremos en contacto contigo, tras la valoración.\r\n\r\nUn saludo y muchas gracias por tu atención. \r\n\r\nMaría C. 	CUADRO_PRE-EVALUACION.docx
maestrorealmanuel@icloud.com	12	2018-12-24 00:00:00	7	2	Buenos días Manuel \r\n\r\nReiteramos el agradecimiento por el interés que has mostrado por nuestra editorial.\r\n\r\nTras la evaluación de tu obra por parte de nuestro Dto. de Lecturas, y dado el posible potencial que vemos en ella y su adecuación a nuestra línea editorial, nos complace comunicarte que estamos interesados en publicar La Despensa del Alma dentro de nuestro de nuestro sello de New Adults, para posteriormente, promocionarla y distribuirla en nuestros canales (tradicionales y plataformas online).\r\n\r\nAun así, el texto necesita una corrección (esto es normal, lo hacemos con todos textos que nos llegan, hasta con los de autores de extensa trayectoria). Todas nuestras obras se corrigen de forma obligatoria, esto es muy importante para no bajar la calidad literaria de nuestras obras.\r\n\r\nHemos consensuado que, para que resulte un producto comercial lo más competitivo, las características técnicas deberían ser:\r\n\r\nCARACTERÍSTICAS TÉCNICAS:\r\n\r\nFORMATO: 150 x 210 mm.\r\nNº PÁGINAS (aprox.): 125\r\nPAPEL INTERIOR: Offset Ahuesado 90 g.\r\n\r\nPor otra parte, según este tipo de libro, hemos calculado que el PVP óptimo deberá ser 12,95 Euros (IVA incluido).\r\n\r\nTras pasar el filtro indispensable de nuestro Comité de Lectura, pasamos a informarte, de forma personalizada, de las condiciones para lleva a cabo esta publicación.\r\n\r\nEres un autor que nos ofrece una obra de calidad que se adecua a nuestra línea editorial y que transmite valores implícitos y, además, vemos en dicha obra el potencial comercial necesario para publicarla, apostando por ella, entre otras. Sin embargo, son muchos los manuscritos que nos llegan al mes con estas mismas características. Por lo que tenemos que volver a seleccionar, por falta de capacidad para asumirlas todas.\r\n\r\nPor lo tanto, para dar viabilidad inmediata a tu proyecto, mostrando tú también tu confianza en tu obra y en que esta va a funcionar entre el público al que se dirige (comenzando por tu entorno cercano y tu ámbito local de influencia y abriendo, poco a poco, el camino a la distribución nacional), necesitamos que impulses el proyecto con una compra mínima de 50 ejemplares, con un descuento del 30 % del PVP.\r\n\r\nSi te interesarse comprar más libros por motivos propios (¡sin compromiso!) debes saber que los descuentos de autor sobre los mismos suben de forma progresiva, para que puedas lucrarte con tu compra de forma progresiva también (consultar cada caso).\r\n\r\nPor lo que el presupuesto de la compra es: 12,95 Euros - 30 % x 50 ejemplares = 453,25 Euros (IVA incluido).\r\n\r\nPor otra parte, presupuesto de corrección/adaptación será de 283 Euros + IVA, asumiendo el autor el solo el 60% del precio: 170 Euros + IVA.\r\n\r\nEste importe se fraccionará en dos pagos, el primero de los mismos, incluirá el total de la corrección. Aunque, si te resultase más cómoda otra opción, estamos abiertos a cualquier sugerencia.  También contamos con una financiera externa a 12 meses sin intereses.\r\n\r\nDe esta publicación, de los libros que nosotros pongamos en distribución y venta online (no de los tuyos) recibirás, en concepto de derechos de autor, el 10 % del PVP (sin IVA) del libro.\r\n\r\nNuestro Protocolo de Edición está fundamentado principalmente en la colaboración entre el Autor y la Editorial, de tal forma que se asegure su máxima difusión por ambas partes. Así que necesitamos involucrarte activamente en la promoción de tu libro y, para esto, siempre recomendamos realizar, como mínimo, una presentación oficial en un lugar acordado por ambas partes, y en la que puedas vender tus primeros libros, que servirán de promoción a otras ventas posteriores en librerías.\r\n\r\nLa editorial se comprometerá a editar y terminar el libro, consensuando contigo el diseño, la maquetación y la publicación final. Además, se comprometerá a distribuirla en todos nuestros canales de venta y a darle la máxima difusión. Así mismo, nuestros libros son ofertados a licitaciones estatales en países de habla hispana con un notable éxito de ventas. Por último, y todo en paralelo, intentaremos vender los derechos de edición para otros idiomas, llevando nuestro catálogo a las principales ferias internacionales del sector a las que asistimos, y manteniéndote al tanto de cualquier posible negociación.\r\n\r\nPara proceder a la redacción del contrato de edición, por favor, mándanos los siguientes datos:\r\n\r\nResumen datos de autor (un cuadro por autor, si hay más de uno):\r\nNombre fiscal:\r\nNombre comercial: El que aparecerá en el libro y webs\r\nTipo de autor: Escritor / Ilustrador / Escritor-ilustrador\r\nDNI: Sin espacios ni guiones\r\nCuenta bancaria (I.B.A.N): Para pago de royalties. Solo autores residentes en la UE.\r\nCuenta Paypal: Para pago de royalties. Autores no residentes en la UE.\r\nTeléfono(s):\r\nEmail:\r\nDirección: lo más clara posible, sobre todo para autores no residentes en España.\r\nC.P:\r\nLocalidad:\r\nProvincia / estado:\r\nPaís:\r\nAño de nacimiento:\r\nLugar de nacimiento:\r\nWeb:\r\nNecesito factura de empresa/autónomo: SÍ / No.\r\n\r\nSi necesitas factura, y los datos de facturación son distintos a los datos de autor, es muy importante que nos lo comuniques ahora.\r\n\r\nSi tus datos de beneficiario de royalties son distintos a los anteriores (Autor y/o cliente) háznoslo saber ahora, en caso contrario entenderemos que el beneficiario es el autor.\r\n\r\nUna vez firmado el contrato, el corrector asignado se pondrá en contacto contigo para tratar el tema de la corrección de forma personalizada.\r\n\r\nDespués la obra pasará a nuestro equipo de diseñadores, para que vayan realizando los trabajos de diseño de cubierta y maquetación del libro.\r\n\r\nAl mismo tiempo, se irán realizando también las gestiones de promoción y de pre-distribución (ficha literaria, nota de prensa, redes sociales, etc.), la preparación del BookTrailer, el eBook (opcional) y las gestiones legales necesarias para su publicación (Agencia del ISBN, código de barras, etc.).\r\n\r\nQuedamos atentos a tus noticias y atenderemos cualquier duda o consulta que te surja.\r\n\r\nRecibe un cordial saludo. 	False
alvaromarbangarcia1989@gmail.com	1	2018-12-18 00:00:00	6	2	 Agradecemos el interés que has mostrado por nuestra editorial.\r\n\r\nHemos recibido tu obra y esta se encuentra a la espera de evaluación por parte de nuestro Dpto. de Lectura, que será el que determine su adecuación a nuestra línea editorial.\r\n\r\nSi es posible, te solicitamos que nos reenvíes el cuadro que te adjuntamos, debidamente cumplimentado con tus datos como autor y con los datos de tu obra. Para nuestro Comité de Lecturas, este cuadro es una herramienta indispensable de control y organización administrativa que se anexiona al expediente de cada obra durante todo el proceso de la evaluación.\r\n\r\nSi te surgiera cualquier duda al respecto, estaremos encantados de resolverla. Nuestros teléfonos son el 912665684 y el 653754771, o bien puedes pedirnos por mail que te llamemos a un tlf de contacto en tu horario preferente.\r\n\r\nEn cualquier caso, en unos días nos pondremos en contacto contigo, tras la valoración.\r\n\r\nUn saludo y muchas gracias por tu atención. \r\n\r\nMaria C.	CUADRO_PRE-EVALUACION.docx
josemarse@hotmail.com	1	2018-12-18 00:00:00	4	3	Hola Jose Manuel,\r\n\r\nAgradecemos el interés que has mostrado por nuestra editorial.\r\n\r\nHemos recibido tu obra y esta se encuentra a la espera de evaluación por parte de nuestro Dpto. de Lectura, que será el que determine su adecuación a nuestra línea editorial.\r\n\r\nSi es posible, te solicitamos que nos reenvíes el cuadro que te adjuntamos, debidamente cumplimentado con tus datos como autor y con los datos de tu obra. Para nuestro Comité de Lecturas, este cuadro es una herramienta indispensable de control y organización administrativa que se anexiona al expediente de cada obra durante todo el proceso de la evaluación.\r\n\r\nSi te surgiera cualquier duda al respecto, estaremos encantados de resolverla. Nuestros teléfonos son el 912665684 y el 653754771, o bien puedes pedirnos por mail que te llamemos a un tlf de contacto en tu horario preferente.\r\n\r\nEn cualquier caso, en unos días nos pondremos en contacto contigo, tras la valoración.\r\n\r\nUn saludo y muchas gracias por tu atención. \r\n\r\nMaría C.	CUADRO_PRE-EVALUACION.docx
m.francosanchez24@gmail.com	1	2018-12-19 00:00:00	2	3	 Agradecemos el interés que has mostrado por nuestra editorial.\r\n\r\nHemos recibido tu obra y esta se encuentra a la espera de evaluación por parte de nuestro Dpto. de Lectura, que será el que determine su adecuación a nuestra línea editorial.\r\n\r\nSi es posible, te solicitamos que nos reenvíes el cuadro que te adjuntamos, debidamente cumplimentado con tus datos como autor y con los datos de tu obra. Para nuestro Comité de Lecturas, este cuadro es una herramienta indispensable de control y organización administrativa que se anexiona al expediente de cada obra durante todo el proceso de la evaluación.\r\n\r\nSi te surgiera cualquier duda al respecto, estaremos encantados de resolverla. Nuestros teléfonos son el 912665684 y el 653754771, o bien puedes pedirnos por mail que te llamemos a un tlf de contacto en tu horario preferente.\r\n\r\nEn cualquier caso, en unos días nos pondremos en contacto contigo, tras la valoración.\r\n\r\nUn saludo y muchas gracias por tu atención. \r\n\r\n(FIRMA SEGÚN USUARIO). 	False
\.


--
-- Data for Name: libros; Type: TABLE DATA; Schema: public; Owner: manu
--

COPY public.libros (nombre, nombreeditorial, emailautor, registro, estado) FROM stdin;
Las aventuras de Marta Montes	Babidi-Bú	krismaec@gmail.com	2018-11-12 12:19:16.14083	Contratado
Clementina	Babidi-Bú	mamaynaiaraleemos@gmail.com	2018-11-12 12:36:53.812952	Contratado
Mirahadas Montes	Mirahadas	krismaec@gmail.com	2018-11-12 12:25:24.507511	Pendiente
En el Valle del Darvo	Mirahadas	anamizquierdo@ono.com	2018-11-15 11:50:57.274443	Pendiente
A contrarreloj	Mirahadas	nikkho@gmail.com	2018-11-15 12:03:57.47218	Pendiente
En bata y con zapatos de tacón	Mirahadas	ratakeli@outlook.com	2018-11-16 12:15:04.897005	Desechado
¡Llévame contigo a Afganistán!	Mirahadas	LCH179102XVII@gmail.com	2018-11-20 10:25:51.832301	Contratado
La Sorda	Mirahadas	teamchilalynn@gmail.com	2018-11-21 10:28:48.249784	En espera
La mariquita azul	Babidi-Bú	mrojas.escritora@gmail.com	2018-11-27 09:32:22.95565	Pendiente
TXAROL	Mirahadas	igoneurrutikoetxea@yahoo.com	2018-12-03 15:20:50.448837	Pendiente
Los pasos en el vacío	Mirahadas	manuelmac78@msn.com	2018-12-13 12:08:41.506816	Desechado
Lazos de amistad	Mirahadas	CARYYPEDRO2011@hotmail.com	2018-12-14 09:55:06.589949	Pendiente
\.


--
-- Data for Name: recordatorios; Type: TABLE DATA; Schema: public; Owner: manu
--

COPY public.recordatorios (emailautor, nombre, apellido, enviando) FROM stdin;
felizconmigo@libero.it	Serena 	Fava	NO
marta@babidibulibros.com	Marta	Presidenta	NO
montesmart@hotmail.com	Marta	Montes	NO
josemarse@hotmail.com	José Manuel 	Martínez Segura	SI
pinkiwinkishop@gmail.com	Leire 	Conde Abella	NO
alvaromarbangarcia1989@gmail.com	Álvaro 	Marbán García	SI
cira12@hotmail.es	Sonia	Domingo	SI
diana_garcia_21@hotmail.com	Diana	García Santiago	SI
raulduemon@hotmail.es	Raúl 	Dueñas Montes	NO
m.francosanchez24@gmail.com	manuel	franco	SI
luanka78@gmail.com	Carolina	Sánchez Pérez	NO
lapinadefraybentos@hotmail.com	Celia	Pierina Gola	SI
maestrorealmanuel@icloud.com	Manuel	Maestro Real	SI
rubenymontse7@gmail.com	Montserrat	Sánchez Castilla	SI
nahuel.exposito@yahoo.com.ar	Nahuel	Expósito	SI
alpe0@hotmail.com	Alvaro 	Perez	NO
\.


--
-- Data for Name: telefonos; Type: TABLE DATA; Schema: public; Owner: manu
--

COPY public.telefonos (numero, emailautor) FROM stdin;
972871637	bredakimkaty@hotmail.com
981945274	sara_22-2-1996@hotmail.com
918434013	esther_shue@icloud.com
968295572	marisalopezsoria@gmail.com
916332524	csilva.velasco@gmail.com
933112929	auradevoradorademundos@hotmail.com
952417541	e_nanum@hotmail.com
913886146	angeldelaserna@grow.es
933033836	ddt@ddtsfx.com
932531501	coordinacion@fundacionimo.org
9512266173	haydeeramoscadena80@gmail.com
942717101	titiunquera@hotmail.com
936687972	jmocenterprise@gmail.com
963061281	jimenasanhueza@hotmail.com
963640836	willyyeva5@gmail.com
933568705	paula.casal@upf.edu
925712288	juliamaria1@yahoo.com
93516454877	ivankacerqueira97@gmail.com
958830354	valoratuself@hotmail.com
686468720	juanlucas98@gmail.com
686743963	victoriainglesrodrigo@gmail.com
622817286	apaquiva@hotmail.com
606042572	yasminarodriguez5@gmail.com
655766299	aslima22@hotmail.com
659951971	design@lamartinique.es
625462225	anaescobargp@gmail.com
696159996	info@fernando-artwork.com
622214574	palomaguerreroa@gmail.com
628132436	mvictoria.becerra.castro@gmail.com
652219671	escarlatasb@hotmail.com
656808457	pazlopez21@gmail.com
653680335	valeriamederer@hotmail.com
690070490	sergioayensa@gmail.com
697920546	gloriia.gg@gmail.com
699660525	gonzalezygarcia@gmail.com
690034361	anirmanx@yahoo.es
606571510	miquel@accio.es
677703131	m_o_r_i_@hotmail.com
670896265	carmensubiza9@gmail.com
649493994	adrippi@adrippi.com
696879244	fromthetree@yahoo.es
630740585	laclinicadellenguaje@gmail.com
607798997	vivianahabad@gmail.com
605093648	mamaynaiaraleemos@gmail.com
662542034	dvr90dvr90@gmail.com
619338468	almuqf@gmail.com
626436493	silviaferama@gmail.com
656591175	inforaffologia@gmail.com
669668429	hola@unapizcadeeducacion.com
629919001	beatriz_winter@yahoo.es
678915044	mlm5013@hotmail.com
686075283	nuriamarfy@gmail.com
659043114	beachaves@hotmail.com
679776044	tikituku13@gmail.com
654119460	marina.forato@gmail.com
659997674	info@manueltristante.com
610362962	mvelagarcia@gmail.com
660695194	mj.llorach@gmail.com
617232206	emiliagarciaserna1@gmail.com
620691748	davidmulla@hotmail.com
617173520	Misuperclase@hotmail.com
645931380	julianmontesinos@gmail.com
607937337	Paolaisabelsantos@gmail.com
695783726	umeirakasle@gmail.com
666000923	rafael.mora.espejo@juntadeandalucia.es
670662782	fran@eraestelar2012.com
654716637	letraenye@yahoo.es
653463247	catc78@yahoo.com
620669019	silviapereznebot@yahoo.es
639018877	milaruizpastor@gmail.com
696797440	patri.musica@hotmail.es
635760315	bosquedefantasias@gmail.com
627242989	Jakedegures@gmail.com
639618180	saqueslopez@yahoo.es
644313207	tlinaresm@gmail.com
625402458	gaur@gmx.es
699219242	torrente_lugones@hotmail.com
686912233	maneroriverola@gmail.com
620130680	djg.forense@gmail.com
669438311	pilar62tc@gmail.com
629647627	patnieto2011@gmail.com
679558360	montana.campon@terra.com
618606566	anamaria.navalon@gmail.com
657057840	rebecablanco50@gmail.com
696405786	jopelo109@hotmail.com
659383672	leno@diegolenoart.com
633661350	bigworldernight@gmail.com
697709613	kyla_jessy-k@hotmail.com
606100328	mariajezfez@yahoo.es
627300053	maypepon@gmail.com
631460096	adrianaproduccionesgraficas@gmail.com
655384099	nbpamies@gmail.com
628147929	carmecolomer@hotmail.com
699246913	charomm@us.es
686778250	ifortunyp@gmail.com
646386167	mpascualnavas@gmail.com
636339859	angelalonsovelasco@gmail.com
629423391	Aninagarcia64@gmail.com 
657396577	j.monzon72@hotmail.com
678241733	cuenterojhonardila@gmail.com
658018193	pinki_74@hotmail.com
608149411	annarosacar@hotmail.com
629182207	eguerra@xtec.cat
680860323	infantilescuentos@hotmail.es
606587345	fatimanoel2@gmail.com
661180620	antonioarilla@hotmail.com
618633851	rosarioochoa87@gmail.com
628667421	pepigonzalezplanelles@gmail.com
649807729	yolfelice@yahoo.es
625592086	blas.rueda@gmail.com
632780403	milarondoo@gmail.com
608044485	fgca1vivazapata@gmail.com
616757267	elrincondepardo@gmail.com
677061050	paugraci@tinet.cat
682696292	rosadevientos12@gmail.com
645373946	magdafire@gmail.com
615470548	benjulopez@gmail.com
655375499	currocorre@yahoo.es
677614504	miannaselka@gmail.com
659946512	macopeca19@gmail.com
606326776	beuka1234@gmail.com
627759556	cecibumcjs@gmail.com
680248897	niviolv@gmail.com
671403418	1974gll.cienife@gmail.com
622669394	hugosailing@icloud.com
652651476	Liamanthony39@gmail.com
698365294	lilalayers@gmail.com
677840537	lcrespillo@gmail.com
667638152	cataplinaencuentada@gmail.com
687677692	vasile370@yahoo.es
617647179	asiersanznieto@gmail.com
652463840	verdejade77@gmail.com
630730257	duendurky@hotmail.com
600373790	animontalvo@gmail.com
646018989	marta.mena86@gmail.com
686026965	palopher@gmail.com
652080749	peke85sds@hotmail.com
627695480	aliciabtm@hotmail.com
693762385	naiara.vidal@hotmail.com
628427663	prudencioterceronieto@gmail.com
695453386	meli.moreira@yahoo.es
652437013	alfredobalbascampo@gmail.com
644246534	mariabeltranfernandez@gmail.com
690859033	rainkproject@gmail.com
616441009	decospg@gmail.com
628932881	claratudela@outlook.es
618792688	lienamarlita@gmail.com
652847339	Nheabad@hotmail.com
665939034	boscola66@gmail.com
616689921	mahytemorales16@gmail.com
605454587	cristinapenaandres@gmail.com
625089893	a.espadas@ono.com
647978968	silvialopezchillaron@gmail.com
680621972	noeliamarquezcalamaro@yahoo.es
636690652	iresega@gmail.com
627350246	vero@comando-g.com
609129877	mmir@totalfreightw.com
610120984	hola@mariajosebarragan.com
688891605	eider.gonzalez@hotmail.com
645312360	mocaponce@gmail.com
633362335	chiara.boffi@gmail.com
685470708	carlosglz@mundo-r.com
655641806	palenciamarbella@hotmail.es
687788679	mpilar.saez@gmail.com
651426619	ceaysantos@hotmail.com
696081612	mfslgr@hotmail.com
653952920	guidoroiz@gmail.com
680764819	marcfreixasbanon@gemail.com
610936635	mercedesg.bueno@gmail.com
676809094	m_l_c_19@hotmail.com
616074841	afrofeminas@gmail.com
611450353	gatamaja@gmail.com
606215149	zeiram@hotmail.com
692563178	eerised819@hotmail.com
692097694	mcuetas@gmail.com
629647359	metge.juliette@gmail.com
656269292	luplame@yahoo.es
661510380	anaaguilera.periodista@gmail.com
653264825	pila_tierra@yahoo.es
695972415	raquelparrondo@lasallevp.es
630777905	oherrerog@gmail.com
676436028	mari29es@hotmail.com
615370271	pacoperez21@hotmail.com
661493851	auralez.contact@gmail.com
634983445	ferldiazdreams@gmail.com
639750626	meditaccion@meditaccion.es
665396587	evapalomargomez@gmail.com
639308033	mariosat@telefonica.net
633908478	nuriadelolmol@gmail.com
658777835	Layospeinado@hotmail.com
629902492	oscarescalera@hotmail.com
658557540	cristina.martin.artajo@gmail.com
606690558	cyberevabcn@hotmail.com
620300420	lasalasandra@hotmail.com
629412899	carolina.corvillo@gmail.com
657573425	lunasalinassevcik@gmail.com
652155276	blandelatorref@yahoo.es
622589069	cuentoslibres@live.com
656476805	enriquecarlosmartin@gmail.com
678908631	bogota8487@hotmail.com
639118743	patricia.gayan@gmail.com
629047084	lauraleonv@gmail.com
630788710	caballerolopezelizabeth@gmail.com
660149840	agnes@dennyefjorden.no
609666815	pinerocecilia@yahoo.es
620696017	polomaltes@hotmail.com
676701039	arcixformacion@arcix.net
639814813	lealcoach@gmail.com
625259088	becolu@hotmail.com
654860834	michelnamid@hotmail.com
687483295	annasoldevilab@gmail.com
622220524	elepizarronogues@hotmail.com
690643278	joselogopedia@gmail.com
606197911	baloola34@gmail.com
609203309	trilogiakayla@gmail.com
650415939	gloriaalmendariz@gmail.com
651043709	lorena_lima_millares@hotmail.com
630935068	edurne.asecas@hotmail.com
677844329	rosaluz_92@hotmail.com
659836015	chincheta15@hotmail.com
687810440	lucholaxe@hotmail.com
602102492	sirenaaguamar@gmail.com
679704207	lmmartinal@hotmail.com
633757555	catalinapark@hotmail.com
646183338	bellesartsvic@gmail.com
672591508	vicky131110@hotmail.com
622822733	jmontesaggi@gmail.com
646521825	luisa_temon@hotmail.com
667037879	carmen.rendueles@gmail.com
645788307	jjmesbailer@hotmail.com
680445513	saragodoy17@hotmail.com
657126331	stephanieleidinger@gmail.com
679310626	trucoseosem@gmail.com
678013533	raquelru@gmail.com
626910871	isabel.mfgt@gmail.com
637286444	belhicca@gmx.com
651370225	rsotoned@gmail.com
699674358	paquimerinos@gmail.com
636298565	a.perezhdz@gmail.com
616081267	albertcebrian@hotmail.com
658432052	raimundo.montava@gmail.com
663109884	jziuxar@hotmail.com
695319922	md.marina@outlook.com
607320284	sandra_ss22@hotmail.com
658240521	andreareyesru@gmail.com
625280128	ada_6015@hotmail.com
665313950	elenamoralesgar@gmail.com
696561993	ammmesas2016@gmail.com
677454418	izaskunaurrekoetxea30@gmail.com
631537741	erick.cuevas@gmail.com
673626260	paulacortes1963@gmail.com
647507965	rosanacuervo@hotmail.com
695198209	colmenares.vicky@gmail.com
654847358	patriciagarcia356@gmail.com
659696778	a.garciamolina@yahoo.es
674033732	sheivigo@hotmail.com
606106118	dolormarieg@gmail.com
676727298	ana@anazurita.com
608967628	ariacuale@gmail.com
622795955	paginaswebmataro@gmail.com
671344279	evpatoria7@yahoo.es
699730736	pgranaval@hotmail.com
601174244	emmayuste@gmail.com
650127109	spdelpino@gmail.com
653384095	matildetoimil@hotmail.com
675537097	mpfgfa@gmail.com
655442195	walkbrave@gmail.com
666069015	annelazpita@gmail.com
678163979	Ialvarezfernandez6@gmail.com
609189820	molina0783@gmail.com
638617865	trocitochocolate@gmail.com
650940223	trini_noa@yahoo.es
602427878	lucialarcon97@gmail.com
677150320	sarita_dlfd@hotmail.es
650614720	saradelasnieves@gmail.com
631831727	redaccionesalejandra@hotmail.com
639642916	elclubdelaslectorasypoetas@gmail.com
651171021	olga.lukinskaya@gmail.com
677061070	graci.moreno.peces@gmail.com
692065452	rebepati@hotmail.com
678250196	zapatosdesirena@gmail.com
619248663	marivihc@gmail.com
627278838	raulca8@hotmail.com
620451789	noelia.rguez.glez@gmail.com
645992118	pequeari82@hotmail.com
667078008	sandraura@hotmail.com
699257674	tamaraaguilar11@hotmail.com
687703039	irene.meurs@gmail.com
610642588	Yolandagg63@hotmail.com
609756294	l.gerpegarcia@gmail.com
652722755	natividad@bailenasesores.com
606694873	cristina.ayuso.guti@gmail.com
629811941	federico-correa@hotmail.com
686130164	cristinamateolopez84@gmail.com
616532561	lucasescary@hotmail.com
686117850	tamaraestrellamurillo@gmail.com
630291403	info@imagosg.com
657784430	nasarrisa@hotmail.com
667798065	revistacincoarcos@hotmail.com
687800053	laurarobado@gmail.com
669061825	fini_arenal@hotmail.es
616448913	soniafan2@gmail.com
635176038	constantinomenendez@gmail.com
652639762	adriana.oliveros.a@gmail.com
638880733	rmjulia94@gmail.com
657264130	aifos30@gmail.com
620273765	mcastilloserrano@loveandchild.com
687202088	luciarlorenzo@gmail.com
619216193	esterverd@hotmail.com
600924222	alma7477@gmail.com
699457648	yulianpopov@gmail.com
630485657	yayorivasmorales@hotmail.com
620191638	Evai1@hotmail.com
677974819	duvi.abril@gmail.com
651885408	beatrizroma.artistaplastica@gmail.com
659374413	chernandezgarri@gmail.com
699519734	yazminlor@yahoo.es
647085517	raiberdisseny@gmail.com
657544767	maria.santossanchez.81@gmail.com
618235369	minorinmakes@gmail.com
673623022	plblan86@gmail.com
617829439	dugoisa@gmail.com
600509933	marta.estringana@gmail.com
617381915	annamorato@yahoo.com
630739288	ogalla@dgraficos.com
699194762	carlosbahos@gmail.com
665677120	estirubi@hotmail.es
672476725	trsa_lc@msn.com
652421086	thais_plg@hotmail.com
677794249	carmencioranu@gmail.com
616865810	sofi.escude@gmail.com
654989419	cesarverduguez@gmail.com
629846233	artemanolo@hotmail.com
667306396	danhellez@gmail.com
679955733	artromyb@gmail.com
659578404	marta.revuelta@yahoo.es
630744914	monimg_1982@hotmail.com
625768681	anukicreationsinfo@gmail.com
649882537	ramaragacuentos@gmail.com
660586606	contacto@rafaestudio.com
645394408	davidpedrera@yahoo.es
653720195	ibagalan@gmail.com
628770449	donapozomarina@gmail.com
688649220	cgar@telefonica.net
626072126	carmenmujerdeluz@gmail.com
666076595	anasanchezmarin@hotmail.com
687814252	admin@islacuentacuentos.es
697289373	saraproubi@hotmail.com
662125091	albapalacioest@gmail.com
661483205	Barbora.Augustinova@seznam.cz
676603479	patricia.vilardebo@gmail.com
660804589	Isabelconesa@hotmail.com
681257464	emiliochec@gmail.com
638672881	cecilia.lapuente@gmail.com
609362277	gemmaalonsolope@hotmail.com
620819544	ensastories@gmail.com
677719630	i.navarro.juan@gmail.com
685257517	luchinguer_@hotmail.com
647500294	carolinasolano@mgtcomunicacion.com
686798520	josemariagil.design@gmail.com
600663122	c2e-@hotmail.com
699394202	asociacioncinetika@yahoo.es
645102593	evatricer@yahoo.es
645060590	floryquito@hotmail.com
622647063	cabalgando_juntos@hotmail.com
696802601	palomacamilojosecela@gmail.com
637032775	cgjaso@hotmail.com
650048667	cbescansaj@gmail.com
699515723	noemicita78@gmail.com
686114268	pabloibarsdemuller@gmail.com
694401417	lic.virginia.gutierrez@gmail.com
629976049	yolrc2015@gmail.com
622110754	veronicabernardinogonzalez@gmail.com
609736605	estef.ros@hotmail.com
697516976	jerselporcupine@gmail.com
678556433	mianmar26@hotmail.com
658152834	pepevillenagonzalez@gmail.com
607653750	dalilehpb@gmail.com
630465223	en.carni92@hotmail.es
625021609	spiderjuanita@ono.com
661930259	mariaveg@gmail.com
658743916	lauranavarrogarces@gmail.com
637057366	ferran.marti.alcaire@gmail.com
696632038	mireiaroal@hotmail.com
639782054	miprimerlibrodebomberos@gmail.com
670605611	beleniky@hotmail.com
609547651	nfo@creaciongrafica.es
646100285	juasma21@gmail.com
626111922	iademasllo@hotmail.com
679981707	csartit@gmail.com
670429225	laura.sanchez.maiz@gmail.com
606271406	AAD2016@outlook.es
665965269	oscaruco1977@hotmail.com
662246997	masbath@yahoo.es
626026729	elenamiguelcampos@yahoo.es
677200396	daniella.emiliani@gmail.com
640710565	carmen@gundemaro.com
610325425	emiliaaliaga@hotmail.es
629812560	rlanseros@gmail.com
628529878	ainasantamaria@gmail.com
661793581	dhvillamor@yahoo.es
650037758	JVILLARINOESPINO@gmail.com
677357045	pilarponcelara@gmail.com
601101138	abellaperal@hotmail.com
617107244	davidherranzgomez@hotmail.com
659655506	orozcogonzaleznoelia@gmail.com
676328406	elenviadodelrockandrollestaakidenuevo@hotmail.com
6946700367	eri.mavraki@gmail.com
633260701	gisela.laurenti@gmail.com
636752127	iztarju@hotmail.com
645970445	juana_torrijos@hotmail.com
645811051	rosaosuna@gmail.com
603594423	luzmilenaria.mp30@gmail.com
696643799	torresguzman.raul@gmail.com
633548788	ankaidrissi.hajar@gmail.com
625110011	NEUSI79@YAHOO.ES
669880933	estelamoreno@us.es
675598755	magalbo@live.com
659810688	briandaetxea@hotmail.com
651448327	garcia6196@gmail.com
688602152	Montsesouto25@gmail.com
679874712	pontevedra57@gmail.com
646426823	rachel.casasus@gmail.com
649992530	antonioboschconde@gmail.com
639906643	info@rafaeljurado.es
671148358	tabitaperalta@gmail.com
628744225	guillermocastilloleon@hotmail.com
699841792	sesmachus@hotmail.com
630069064	malgomrod@hotmail.es
655286053	m.rodenasvargas@gmail.com
636284632	rpicazog@gmail.com
600238421	sigridamoros@gmail.com
646850900	mgloriagamarra@gmail.com
610739956	tbrandt@movemarketing.es
618639011	proyecto.datap@gmail.com
678405709	amparo.hellin@gmail.com
629788516	novalnueva@gmail.com
649790924	santaruthi@yahoo.es
696561048	aldomo64@yahoo.es
669801846	alynunez@gmail.com
687791179	iaymarevl@gmail.com
626842489	joancampstejero@hotmail.com
679937652	silponscaldero@gmail.com
666882981	c.exposito.escalona@gmail.com
649721130	familiacabellojimenez@gmail.com
600599907	cristinadespacho@yahoo.es
672718157	paulaforconi@gmail.com
666778857	marietaleylo127@gmail.com
615163434	ceititirimundi@hotmail.com
687930283	unviajemagico2017@gmail.com
679745709	kupsa03@kupsa.es
643387380	yolandagreus10@live.com
620211532	nayraesteban@gmail.com
679759345	mariasuch.dn@gmail.com
670792114	mcgonzalez66@gmail.com
678110249	esther_md19@hotmail.com
617524510	lauramarinruiz@hotmail.com
650582910	cristinapsolano@gmail.com
629482725	elisabetsc@hotmail.com
657466070	angela.arganda@gmail.com
673109844	nenamelenas@hotmail.es
696969022	amega4765@hotmail.com
606150569	virginia.sanzmoreno@gmail.com
686474011	sonialgui78@gmail.com
627821993	vitsifu@gmail.com
617193604	anabelendaganzo22@hotmail.com
682589556	inversionescl@gmail.com
677312902	anarodmon@gmail.com
661844542	tousa81@hotmail.com
649693450	mariacristina.hoyos@salud.madrid.org
686395561	juanconcejal@live.com
6250708885	bvl10@hotmail.com
618003335	teles-bea@live.com
655490660	eva.fuentes.usea@gmail.com
626603709	trinky_f@hotmail.com
620464934	lortegavesga@gmail.com
654601548	corbellota@hotmail.com
690391994	Maite.c.herrero@gmail.com
661738500	ccanocab@xtec.cat
637044796	psanchezveronica@gmail.com
651931377	vegacala@hotmail.com
686122790	leirert@hotmail.com
619163224	silvia@mutkids.es
667411511	essanmu@gmail.com
620739710	dabenedicte@facebook.com
658194898	paulifersa@gmail.com
618069228	annaseguranyes7@yahoo.es
662368129	araceli.rodrigo.chacon@gmail.com
651669675	quecuatsa@gmail.com
699681081	gpuertas@live.com
628534231	jalvaredo@gmail.com
666897504	mgl.escritura@gmail.com
650986968	esthercita.esther@gmail.com
661857837	ibeltranj83@gmail.com
649564998	alayrach69@hotmail.com
617816354	bajistapopero@hotmail.com
610663669	victorja.navarro@yahoo.es
667756288	glopaz7@gmail.com
629912309	javi4000@gmail.com
667887083	mrhr.laura@gmail.com
654663941	pedroencinascerezo@gmail.com
659369676	macotera1952@gmail.com
660166515	cambioderumboya@gmail.com
699981773	susanazapata28@gmail.com
630069840	urreaeduardo@hotmail.com
661248673	anabichologa@gmail.com
654641232	yolandatowandacuentos@gmail.com
610781719	mc_ribes@hotmail.com
619056895	memetristan@hotmail.com
607606633	paulapitarch.illustra@gmail.com
654159460	favialsan31@hotmail.com
654413511	jcamaraju@gmail.com
660299289	jackbabiloni@jackbabiloni.es
663353192	hadamapy@gmail.com
616741239	muzrodri@gmail.com
626388695	ipardocaballero@gmail.com
606899953	palovc@hotmail.com
646594827	tfons@xtec.cat
609199578	rmolerofeeme@gmail.com
683608389	okosketch@gmail.com
634554128	algare88@gmail.com
618325598	soy1alg@hotmail.com
646279727	laurasuarezlorca@hotmail.com
626713719	amondarainrmt@hotmail.com
601348895	sandra-montes-troyano@hotmail.com
606658520	almudenamariapuebla@hotmail.com
647456943	cuentik@cuentik.com
630925997	iluame@live.com
684627281	xaviereguiguren@hotmail.es
666625070	mimundoconellostres@gmail.com
667555468	georginaponceblascol@gmail.com
645506836	gnesisvent@gmail.com
617774987	muriasls@hotmail.com
646755191	fgarza@salud.aragon.es
600240306	irene.andivia@yahoo.es
679836551	inma.p_r@hotmail.com
635672224	rociofluc@gmail.com
660928073	yorma_ulloa@hotmail.com
620950300	dani.ortiguera@gmail.com
656425260	luzmariamarte44@gmail.com
620874064	olile2003@gmail.com
640235502	Leticiaernst94@gmail.com
654240619	jcarlosmestre@gmail.com
625093177	monicaestradaroyuela@gmail.com
645953603	larajuansilvia@gmail.com
615595354	infantildana@gmail.com
609863186	carpediem7veces@hotmail.com
678747446	ferry.el8@gmail.com
636169846	alxfernandezm@gmail.com
667432832	albertopiru@yahoo.es
643039438	patriciaejesus2015@gmail.com
633799118	susana@srueda.com
646559743	pujolantonia8@gmail.com
678214869	mons811@hotmail.com
676592938	raquelcarpes@gmail.com
606546439	isabelagueraes@gmail.com
650235507	Rebemartinz@gmail.com
632205031	gcasmir5@hotmail.com
636007217	martagomezbu@gmail.com
603502279	helenasomon@gmail.com
639680026	eryka_navega@hotmail.com
646472384	berges77@yahoo.es
699100706	nuriselv@gmail.com
669214705	nayatkaidferron@me.com
625247805	pedro53luis@gemail.com
6672023338	jocelyner1@outlook.es
625344732	ariaznabea@live.com
644062388	juan_alcudia@yahoo.es
645132350	info@lunasanjuan.com
634405333	kathrynsescribano@gmail.com
680579151	saramorarte@gmail.com
696246767	nagozubi@gmail.com
669331120	info@jabatostudio.com
665600200	mariobsdm@gmail.com
686795839	estherpinva@gmail.com
664700383	ambrosiosanchezr@gmail.com
691657341	phalma727@gmail.com
622954302	alealejandrafuente@gmail.com
649111041	terecarreteropoblete@gmail.com
659898485	mibeldo@hotmail.com
662069172	silvarecuerorobert@gmail.com
639106732	aracelijuan@msn.com
607725662	anadelgadofiguera@hotmail.com
661238483	marilonorte@gmail.com
609313662	tambre77@gmail.com
653068075	Maria.b_23@hotmail.com
607475980	madasimbae@gmail.com
665875353	mariela1967@hotmail.es
616939899	kekacolmenero@gmail.com
616613797	ramonricojimenez@gmail.com
627218540	unratoparami@gmail.com
653224613	pgruth13@hotmail.com
645206036	emma_sanchez_varela@hotmail.com
617812532	Raquel.Fernandez@uv.es
600351387	esteladediego.art@gmail.com
677497427	crlpgn@gmail.com
606029766	ojuben@gmail.com
697369640	tamara_titan@hotmail.com
658087598	adacollet@gmail.com
670980892	pimaruiz@hotmail.com
661333580	josefabiorivas@hotmail.com
681222199	yazmila@gmail.com
678747198	pazoloren@gmail.com
630569617	f10galan@yahoo.es
626356817	ibardera@telefonica.net
661156011	martanahe@hotmail.com
630611403	patribp82@hotmail.com
636004528	ansago71@yahoo.es
629252698	fidalgohelena@hotmail.com
616661862	pilopavila@gmail.com
660057991	david-lc-@hotmail.com
626226001	lamaestradept@hotmail.com
625760309	darlamey@hotmail.com
636270100	yaizatorresbeffa@gmail.com
671350505	evars1991@gmail.com
620341717	julge26@gmail.com
619164853	zidac@hotmail.com
663433492	auxi.maestre@gmail.com
646093259	ali_guoman@hotmail.com
659894246	antondebenito@gmail.com
606869490	amaiacalzadagorricho@gmail.com
647137227	esteladalila@gmail.com
650553528	patri2870@outlook.com
653875272	jomacumo29@gmail.com
620862033	frank65llobregat@gmail.com
676362027	amaravenayas@hotmail.com
629766192	tzuluagac@gmail.com
652510701	topakarting@gmail.com
626693390	m.santos@rojumar.com
625932758	jaenenri@gmail.com
617276662	chema-96@hotmail.es
696408320	pilarmontorocadiz@gmail.com
650157266	joseantoniopv06@hotmail.com
653973791	kophrodess@hotmail.es
603561490	ivan1976sanchez@hotmail.com
619491633	carloshugoasperilla@gmail.com
647603087	dostolov@gmail.com
636639027	astray.winter@gmail.com
670793735	abetetamas87@hotmail.com
649737545	j_anaya_flores@yahoo.es
696259479	siluetadenadie@gmail.com
656978045	florhaya@hotmail.com
637294520	matime72@hotmail.com
662109964	mjgomezm6@gmail.com
666181713	neusgonzalezborrell@gmail.com
633463173	martag897@gmail.com
688462568	monteromarco999@gmail.com
605970902	noepoli43@gmail.com
687140543	Ivan_larreaa@hotmail.com
673206661	malala29.6@gmail.com
692182168	zitges8@hotmail.com
654357306	lauratriasc@gmail.com
673377893	ana.merino00@csioviedo.com
620947271	c.cebriangonzalez@hotmail.com
682180686	creaciondemanuela@hotmail.es
674680349	paaablooo23@gmail.com
635543632	carolinacerruti@gmail.com
670730529	rosmer@telefonica.net
658082536	holaflamencolica@gmail.com
653754771	paco-rojas@hotmail.es
650001047	montsehh@gmail.com
615284463	mariareiki17@yahoo.com
608239959	scasaseca@gmail.com
615639212	juansalvadorpinero@gmail.com
633510998	joxnmahiques@gmail.com
627424922	cuerdage@gmail.com
607817994	oyesmira@bizkaia.eu
630151560	sacoal@hotmail.com
642750278	inesprats70@gmail.com
632011373	luzdeatlante@hotmail.com
616461026	ramiant76@gmail.com
646829848	bedoyarti@hotmail.com
692176790	fernando.delablanca@gmail.com
636869399	majoduma@yahoo.es
603784412	Amadorsanchezdeleonana@gmail.com
653451304	aicila1961@hotmail.com
606955665	jp.defrutos.barrero@gmail.com
609365677	albertoatienza@yahoo.es
643492407	persiguiendomissuenos1205@gmail.com
627036914	rafaelsaenzdecabezon@gmail.com
636233573	ratakeli@outlook.com
4,91753E+11	diana327@hotmail.com
59899592832	dariko2013@hotmail.com
5411479167	ferreirar@intramed.net
787519873	tommyvillarreal@gmail.com
7074795370	ronlemley@hotmail.com
722478155	mejimanx@hotmail.com
876015406	info@thenomadicdreamer.com
8322351060	elias_gc56@hotmail.com
tomarviento@mail.com	\N
daviid.tc@gmail.com	\N
654704056	m.francosanchez24@gmail.com
75929916	caryom2017@gmail.com
None	pueblasdelgados@gmail.com
610358764	felizconmigo@libero.it
722479092	bherrer5@xtec.cat
661254121	terapiasutu@gmx.es
\.


--
-- Name: envios_correo envios_correo_pkey; Type: CONSTRAINT; Schema: public; Owner: manu
--

ALTER TABLE ONLY public.envios_correo
    ADD CONSTRAINT envios_correo_pkey PRIMARY KEY (emailautor);


--
-- Name: autores pkautor; Type: CONSTRAINT; Schema: public; Owner: manu
--

ALTER TABLE ONLY public.autores
    ADD CONSTRAINT pkautor PRIMARY KEY (email);


--
-- Name: cadenas_correo pkcadenas_correo; Type: CONSTRAINT; Schema: public; Owner: manu
--

ALTER TABLE ONLY public.cadenas_correo
    ADD CONSTRAINT pkcadenas_correo PRIMARY KEY (id, orden);


--
-- Name: comentarios pkcmentarios; Type: CONSTRAINT; Schema: public; Owner: manu
--

ALTER TABLE ONLY public.comentarios
    ADD CONSTRAINT pkcmentarios PRIMARY KEY (emailautor, usernameeditor, fecha);


--
-- Name: editores pkeditor; Type: CONSTRAINT; Schema: public; Owner: manu
--

ALTER TABLE ONLY public.editores
    ADD CONSTRAINT pkeditor PRIMARY KEY (username);


--
-- Name: editoriales pkeditorial; Type: CONSTRAINT; Schema: public; Owner: manu
--

ALTER TABLE ONLY public.editoriales
    ADD CONSTRAINT pkeditorial PRIMARY KEY (nombre);


--
-- Name: editorialdeleditor pkeditorialeditor; Type: CONSTRAINT; Schema: public; Owner: manu
--

ALTER TABLE ONLY public.editorialdeleditor
    ADD CONSTRAINT pkeditorialeditor PRIMARY KEY (nombreeditorial, usernameeditor);


--
-- Name: libros pklibro; Type: CONSTRAINT; Schema: public; Owner: manu
--

ALTER TABLE ONLY public.libros
    ADD CONSTRAINT pklibro PRIMARY KEY (nombre, nombreeditorial, emailautor);


--
-- Name: recordatorios pkrecordatorios; Type: CONSTRAINT; Schema: public; Owner: manu
--

ALTER TABLE ONLY public.recordatorios
    ADD CONSTRAINT pkrecordatorios PRIMARY KEY (emailautor);


--
-- Name: telefonos pktelefono; Type: CONSTRAINT; Schema: public; Owner: manu
--

ALTER TABLE ONLY public.telefonos
    ADD CONSTRAINT pktelefono PRIMARY KEY (numero);


--
-- Name: telefonos fkemailautor; Type: FK CONSTRAINT; Schema: public; Owner: manu
--

ALTER TABLE ONLY public.telefonos
    ADD CONSTRAINT fkemailautor FOREIGN KEY (emailautor) REFERENCES public.autores(email) ON DELETE CASCADE;


--
-- Name: comentarios fkemailautor; Type: FK CONSTRAINT; Schema: public; Owner: manu
--

ALTER TABLE ONLY public.comentarios
    ADD CONSTRAINT fkemailautor FOREIGN KEY (emailautor) REFERENCES public.autores(email) ON DELETE CASCADE;


--
-- Name: libros fkemailautor; Type: FK CONSTRAINT; Schema: public; Owner: manu
--

ALTER TABLE ONLY public.libros
    ADD CONSTRAINT fkemailautor FOREIGN KEY (emailautor) REFERENCES public.autores(email) ON DELETE CASCADE;


--
-- Name: recordatorios fkemailautor; Type: FK CONSTRAINT; Schema: public; Owner: manu
--

ALTER TABLE ONLY public.recordatorios
    ADD CONSTRAINT fkemailautor FOREIGN KEY (emailautor) REFERENCES public.autores(email) ON DELETE CASCADE;


--
-- Name: envios_correo fkemailautor; Type: FK CONSTRAINT; Schema: public; Owner: manu
--

ALTER TABLE ONLY public.envios_correo
    ADD CONSTRAINT fkemailautor FOREIGN KEY (emailautor) REFERENCES public.autores(email) ON DELETE CASCADE;


--
-- Name: libros fknombreeditorial; Type: FK CONSTRAINT; Schema: public; Owner: manu
--

ALTER TABLE ONLY public.libros
    ADD CONSTRAINT fknombreeditorial FOREIGN KEY (nombreeditorial) REFERENCES public.editoriales(nombre);


--
-- Name: editorialdeleditor fknombreeditorial; Type: FK CONSTRAINT; Schema: public; Owner: manu
--

ALTER TABLE ONLY public.editorialdeleditor
    ADD CONSTRAINT fknombreeditorial FOREIGN KEY (nombreeditorial) REFERENCES public.editoriales(nombre);


--
-- Name: editorialdeleditor fkusernameeditor; Type: FK CONSTRAINT; Schema: public; Owner: manu
--

ALTER TABLE ONLY public.editorialdeleditor
    ADD CONSTRAINT fkusernameeditor FOREIGN KEY (usernameeditor) REFERENCES public.editores(username);


--
-- Name: comentarios fkusernameeditor; Type: FK CONSTRAINT; Schema: public; Owner: manu
--

ALTER TABLE ONLY public.comentarios
    ADD CONSTRAINT fkusernameeditor FOREIGN KEY (usernameeditor) REFERENCES public.editores(username);


--
-- Name: autores fkusernameregistrador; Type: FK CONSTRAINT; Schema: public; Owner: manu
--

ALTER TABLE ONLY public.autores
    ADD CONSTRAINT fkusernameregistrador FOREIGN KEY (registrador) REFERENCES public.editores(username);


--
-- PostgreSQL database dump complete
--


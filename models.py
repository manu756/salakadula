from flask_bcrypt import generate_password_hash
from sqlalchemy import create_engine
from sqlalchemy.sql import text

db = create_engine('postgresql+psycopg2://testing:testing@localhost/babidi')
connection = db.connect()
class User():
    @staticmethod
    def get_user(user):
        result = db.execute(text
            ('select * from editores where username = :user'),user=user
            )

        return result
    @staticmethod
    def insert_user(user, password):
        db.execute(text
                  ('insert into editores values(:user, :password)'),
                   user=user,
                   password=generate_password_hash(password)
                   )

    @staticmethod
    def change_pass(user, password):
        db.execute(text
                ('update editores set password = :password where username = :user'),
                user=user,
                password=generate_password_hash(password)
                )

class Author():
    @staticmethod
    def get_all():
        result = db.execute("select a.email, a.nombre, a.apellido, a.registrador, a.fecharegistro::date as fecharegistro from autores a order by a.fecharegistro desc")
        return result

    @staticmethod
    def author_delete(email):
        db.execute(text('delete from autores where email = :email'), email=email)

    @staticmethod
    def get_reminders():
   #     result = db.execute("select email, nombre, apellido, registrador from autores where email in (select emailautor from envios_correo where recordatorio='true') order by fecharegistro desc")
#        result = db.execute("select * from recordatorios")
        result = db.execute("select distinct r.emailautor, r.nombre, r.apellido, r.enviando,c.asunto, e.correo_actual,e.fecha_envio::date as fecha_envio, c.editorial from recordatorios as r inner join envios_correo as e on r.emailautor = e.emailautor inner join cadenas_correo as c on e.idcorreo = c.id::text order by enviando asc")
        return result

    @staticmethod
    def add_reminder(autor):
        db.execute(text("insert into recordatorios (emailautor, nombre, apellido) select email, nombre, apellido from autores where email = :emailautor"), emailautor=autor)

    @staticmethod
    def insert_author(nombre, apellido, email, registrador):
        db.execute(text
                  ('insert into autores (nombre, apellido, email, registrador) values (:nombre, :apellido, :email, :registrador)'),
                  nombre=nombre,
                  apellido=apellido,
                  email=email,
                  registrador=registrador
                  )

    @staticmethod
    def get_name(autor):
        result = db.execute(text('select nombre, apellido from autores where email = :email'), email=autor)
        return result

    @staticmethod
    def get_detail(autor):
        result = db.execute(text("select email,nombre,apellido, fecharegistro::date as fecharegistro,registrador,numero  from autores left join telefonos on autores.email = emailautor where email= :autor"), autor=autor)
        return result

    @staticmethod
    def get_comments(autor):
        result = db.execute(text("select usernameeditor,comentario, fecha::date as fecha, fecha as fecha_orden from comentarios where emailautor = :autor order by fecha_orden desc"), autor=autor)
        return result

    @staticmethod
    def get_book(autor):
        result = db.execute(text("select nombre,nombreeditorial, registro::date as registro ,estado from libros where emailautor = :autor order by registro desc"), autor=autor)
        return result

    @staticmethod
    def get_tipo(autor):
        result = db.execute(text("select tipo from autores where email = :autor"), autor=autor)
        return result

    @staticmethod
    def update_autor(email, nombre, apellido, emailautor):
        db.execute(text('update autores set email = :email, nombre = :nombre, apellido = :apellido where email = :emailautor'), email=email, nombre=nombre, apellido=apellido, emailautor=emailautor)

    @staticmethod
    def update_phone(emailautor, numero):
        db.execute(text('update telefonos set numero = :numero where emailautor = :emailautor'), emailautor = emailautor, numero = numero)

    @staticmethod
    def update_tipo(emailautor, tipo):
        db.execute(text('update autores set tipo = :tipo where email = :emailautor'), emailautor = emailautor, tipo = tipo)

    @staticmethod
    def insert_phone(emailautor, numero):
        db.execute(text('insert into telefonos values(:numero, :emailautor)'), emailautor = emailautor, numero = numero)

class Book():
    @staticmethod
    def insert_book(nombre,editorial,email,estado):
        db.execute(text('insert into libros("nombre","nombreeditorial","emailautor","estado") values (:nombre, :editorial, :email, :estado)'),nombre=nombre,editorial=editorial,email=email,estado=estado)

    @staticmethod
    def update_status(nombre, nombreeditorial, autor, estado):
        db.execute(text('update libros set estado = :estado where emailautor = :autor and nombre = :nombre and nombreeditorial = :nombreeditorial'), nombre=nombre, nombreeditorial=nombreeditorial, autor=autor, estado=estado)

class Comments():
    @staticmethod
    def add_comment(email, usuario, comentario):
        db.execute(text('insert into comentarios ("emailautor", "comentario", "usernameeditor") values (:email, :comentario, :usuario)'), email=email, comentario=comentario, usuario=usuario)


class Editoriales():
    @staticmethod
    def get_editoriales():
        result = db.execute(text('select nombre from editoriales'))
        return result

    @staticmethod
    def add_editorial(nombre):
        db.execute(text("insert into editoriales values (:nombre)"), nombre=nombre)

class Mail():
    @staticmethod
    def delete_chain(id_chain, orden):
        db.execute(text("delete from cadenas_correo where id = :id_chain and orden = :orden"), orden = orden, id_chain = id_chain)

    @staticmethod
    def get_id(asunto, editorial):
        result = db.execute(text("select id from cadenas_correo where asunto=:asunto and editorial=:editorial group by id"), asunto=asunto, editorial=editorial)
        return result
        
    @staticmethod
    def get_order_max(asunto):
        result = db.execute(text("select max(orden::integer)+1 from cadenas_correo where asunto = :asunto"), asunto=asunto)
        return result

    @staticmethod
    def update_chains(texto, orden, id_chain):
        db.execute(text("update cadenas_correo set texto = :texto where orden = :orden and id = :id"), texto=texto, orden=orden, id=id_chain)

    @staticmethod
    def get_high_orden(id_chain):
        result = db.execute(text("select max(orden::integer) from cadenas_correo where id = :id_chain"), id_chain=id_chain)
        return result

    @staticmethod
    def create_chain(asunto,orden,editorial,texto,id_max):
        db.execute(text('insert into cadenas_correo values(:id, :orden, :texto, :asunto, :editorial)'), 
            id=id_max, orden=orden, texto=texto, asunto=asunto, editorial=editorial)

    @staticmethod
    def get_max_id():
        result = db.execute(text("select max(id)+1 from cadenas_correo"))
        return result

    @staticmethod
    def get_chain(cadena):
        result = db.execute(text("select texto from cadenas_correo where id::text = :cadena and orden = '1'"), cadena=cadena)
        return result

    @staticmethod
    def add_mail(email, fecha_envio, texto, periodicidad, cadena, fichero):
        db.execute(text('insert into envios_correo ("emailautor", "idcorreo", "fecha_envio", "frecuencia", "texto", "fichero") values (:emailautor, :idcorreo, :fecha_envio, :frecuencia, :texto, :fichero)'),
                   emailautor=email, fecha_envio=fecha_envio, idcorreo=cadena, frecuencia=periodicidad, texto=texto, fichero=fichero)

    @staticmethod
    def stop_mail(email):
        db.execute(text('delete from envios_correo where emailautor = :email'), email=email)

    @staticmethod
    def stop_reminder(email):
        db.execute(text('delete from recordatorios where emailautor = :email'), email=email)

    @staticmethod
    def get_chain_autor(email):
        result = db.execute(text('select distinct asunto from cadenas_correo where id::text = (select idcorreo from envios_correo where emailautor = :email)'), email=email)
        return result

    @staticmethod
    def get_periodicity(email):
        result = db.execute(text('select frecuencia from envios_correo where emailautor = :email'), email=email)
        return result

    @staticmethod
    def update_mail(email, fecha, cadena, periodicidad, texto):
        db.execute(text("update envios_correo set idcorreo = :cadena, fecha_envio = :fecha, correo_actual = '1', frecuencia = :periodicidad, texto = :texto where emailautor = :email"),
                   cadena=cadena, periodicidad=periodicidad, texto=texto, email=email, fecha=fecha)

    @staticmethod
    def change_reminder(autor):
        db.execute(text("update recordatorios set enviando = 'NO' where emailautor = :autor "), autor=autor)

    @staticmethod
    def restart_reminder(autor):
        db.execute(text("update recordatorios set enviando = 'SI' where emailautor = :autor "), autor=autor)

    @staticmethod
    def get_subjects():
        result = db.execute(text('select asunto, max(orden::integer) as orden,editorial,id from cadenas_correo group by editorial,asunto,id order by editorial'))
        return result

    @staticmethod
    def get_orden_texto(id_cadena):
        result = db.execute(text('select orden, texto from cadenas_correo where id = :id_cadena order by orden::integer'), id_cadena=id_cadena)
        return result

from wtforms.validators import (DataRequired, ValidationError, Email, Regexp, Length, EqualTo)
from wtforms import Form, StringField, PasswordField, TextAreaField, IntegerField

class RegisterForm(Form):
    username = StringField(
        'Username',
        validators=[
            DataRequired(),
            Regexp(
                r'[a-zA-Z0-9_]+$'
            ),
            Length(min=2),
            Length(max=20)

        ]
    )
    password = PasswordField(
        'Password',
        validators=[
            DataRequired(),
            Length(min=8),
            EqualTo('password2', message='Los passwords deben coincidir')
        ]
    )
    password2 = PasswordField(
        'Confirm password',
        validators=[
            DataRequired()
        ]
    )

class LoginForm(Form):
    username = StringField(
        'Username',
        validators=[
            DataRequired(),
        ]
    )
    password = PasswordField(
        'Password',
        validators= [
            DataRequired()
        ]
    )

class AuthorForm(Form):
    nombre = StringField(
        'Name',
        validators=[
            DataRequired()
            ]
        )
    apellido = StringField(
            'Surname',
            validators=[
                Length(min=1)
                ]
            )
    email = StringField(
        'Email',
        validators=[
            DataRequired(),
            Email()
            ]
        )

class ResetPassForm(Form):
    username = StringField(
        'Username',
        validators=[
            DataRequired()
        ]
    )
    oldPassword = PasswordField(
        'Old password',
        validators=[
            DataRequired()
        ]
    )
    newPassword = PasswordField(
        'New password',
        validators=[
            DataRequired(),
            Length(min=8),
            EqualTo('newPassword2', message='Los passwords deben coincidir')
        ]
    )
    newPassword2 = PasswordField(
        'Confirm new password',
        validators=[
            DataRequired()
        ]
    )

class UpdateAuthorForm(Form):
    nombre = StringField(
        'Nombre'
    )
    apellido = StringField(
        'Apellido'
    )
    email = StringField(
        'Email'
    )
    numero = StringField(
        'Telefono'
    )

class AddBookForm(Form):
    nombre_libro = StringField(
        'Nombre'
    )
    editorial = StringField(
        'Editorial'
    )

class AddCommentForm(Form):
    comentario = TextAreaField(
        'Texto'
    )

class AuthorSend(Form):
    cadena = StringField(
        'cadena', validators=[DataRequired()]
    )
    fecha_envio = StringField(
        'fecha envio', validators=[DataRequired()]
    )
    periodicidad = IntegerField(
        'periodicidad'
    )

class EditText(Form):
    texto = TextAreaField(
        'texto'
    )

class EditMail(Form):
    cadena = StringField(
        'cadena', validators=[DataRequired()]
    )
    periodicidad = IntegerField(
        'periodicidad'
    )

class EditMailText(Form):
    texto = TextAreaField(
        'texto'
    )

class AddEditorial(Form):
    nombre = StringField(
        'nombre'
    )

class NewChain(Form):
    asunto = TextAreaField(
            'asunto'
    )
    orden = TextAreaField(
            'orden'
    )
    editorial = TextAreaField(
            'editorial'
    )
    texto = TextAreaField(
            'texto'
    )

class DeleteChain(Form):
    orden = TextAreaField(
        'orden'
    )

class EditChain(Form):
    id_chain = TextAreaField(
            'orden'
    )
    orden = TextAreaField(
            'orden'
    )
    texto = TextAreaField(
            'texto'
    )